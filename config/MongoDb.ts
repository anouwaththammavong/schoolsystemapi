import mongoose, { Mongoose } from "mongoose";

const connectDatabase = async () => {
  try {
    const conn: Mongoose = await mongoose.connect(
      process.env.MONGO_URL as string
    );
    console.log("Mongo Connected");
  } catch (error: any) {
    console.log(`Error: ${error.message}`);
    process.exit(1);
  }
};

mongoose.connection.on("disconnected", () => {
  console.log("mongoDB disconnected!");
});

mongoose.connection.on("connected", () => {
  console.log("mongoDB connected!");
});

export default connectDatabase;