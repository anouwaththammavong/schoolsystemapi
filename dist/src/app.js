"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const MongoDb_1 = __importDefault(require("../config/MongoDb"));
const authRoutes_1 = __importDefault(require("./routes/authRoutes"));
const ClassLevelRoutes_1 = __importDefault(require("./routes/ClassLevelRoutes"));
const ClassroomRoutes_1 = __importDefault(require("./routes/ClassroomRoutes"));
const AcademicYearRoutes_1 = __importDefault(require("./routes/AcademicYearRoutes"));
const SchoolFeeRoutes_1 = __importDefault(require("./routes/SchoolFeeRoutes"));
const SubjectV2Routes_1 = __importDefault(require("./routes/SubjectV2Routes"));
const RegistrationRoutes_1 = __importDefault(require("./routes/RegistrationRoutes"));
const studentRoutes_1 = __importDefault(require("./routes/studentRoutes"));
const subjectRoutes_1 = __importDefault(require("./routes/subjectRoutes"));
const teacherRoutes_1 = __importDefault(require("./routes/teacherRoutes"));
const assignmentRoutes_1 = __importDefault(require("./routes/assignmentRoutes"));
const studentAssessmentRoutes_1 = __importDefault(require("./routes/studentAssessmentRoutes"));
const TeacherAssessmentRoutes_1 = __importDefault(require("./routes/TeacherAssessmentRoutes"));
const ScoreRoutes_1 = __importDefault(require("./routes/ScoreRoutes"));
const teacherAbsenceRoutes_1 = __importDefault(require("./routes/teacherAbsenceRoutes"));
const studentAbsenceRoutes_1 = __importDefault(require("./routes/studentAbsenceRoutes"));
const payStudentTuitionRoutes_1 = __importDefault(require("./routes/payStudentTuitionRoutes"));
const profileRoute_1 = __importDefault(require("./routes/profileRoute"));
const authMiddleware_1 = __importDefault(require("./middlewares/authMiddleware"));
const adminMiddleware_1 = __importDefault(require("./middlewares/adminMiddleware"));
dotenv_1.default.config();
(0, MongoDb_1.default)();
const port = process.env.PORT || 8081;
const app = (0, express_1.default)();
// Body parsing middleware
app.use(express_1.default.urlencoded({ extended: false }));
app.use(express_1.default.json());
app.use((0, cors_1.default)());
app.use("/api", authRoutes_1.default);
// Custom authentication middleware
app.use(authMiddleware_1.default);
// Routes
app.use("/api/v1/classLevel", ClassLevelRoutes_1.default);
app.use("/api/v1/classroom", ClassroomRoutes_1.default);
app.use("/api/v1/academic", AcademicYearRoutes_1.default);
app.use("/api/v1/school_fee", SchoolFeeRoutes_1.default);
app.use("/api/v1/subjectV2", SubjectV2Routes_1.default);
app.use("/api/v1/student", studentRoutes_1.default);
app.use("/api/v1/subject", subjectRoutes_1.default);
app.use("/api/v1/teacher", teacherRoutes_1.default);
app.use("/api/v1/registration", RegistrationRoutes_1.default);
app.use("/api/v1/assignment", assignmentRoutes_1.default);
app.use("/api/v1/studentAssessment", studentAssessmentRoutes_1.default);
app.use("/api/v1/teacherAssessment", TeacherAssessmentRoutes_1.default);
app.use("/api/v1/score", ScoreRoutes_1.default);
app.use("/api/v1/teacherAbsence", teacherAbsenceRoutes_1.default);
app.use("/api/v1/studentAbsence", studentAbsenceRoutes_1.default);
app.use("/api/v1/payStudentTuition", payStudentTuitionRoutes_1.default);
app.use("/api/v1/profileRoutes", profileRoute_1.default);
app.use(adminMiddleware_1.default);
// Error handling middleware
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).json({ message: "Internal Server Error" });
});
app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
