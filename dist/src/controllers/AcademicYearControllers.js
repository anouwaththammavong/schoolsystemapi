"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAcademicYear = exports.updateAcademicYear = exports.getAcademicYear = exports.createAcademicYear = void 0;
const AcademicYearRepository_1 = __importDefault(require("../data/repositories/AcademicYearRepository"));
const AcademicYearModels_1 = __importDefault(require("../domain/models/AcademicYearModels"));
const createAcademicYear = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const academicYearData = req.body;
        yield AcademicYearRepository_1.default.createAcademicYear(AcademicYearModels_1.default, academicYearData);
        res.status(201).json({ message: "Academic year created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createAcademicYear = createAcademicYear;
const getAcademicYear = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const academicYears = yield AcademicYearRepository_1.default.getAcademicYear(limit, offset, AcademicYearModels_1.default);
        res.status(200).json({ academicYears });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getAcademicYear = getAcademicYear;
const updateAcademicYear = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield AcademicYearRepository_1.default.updateAcademicYear(_id, reqBody, AcademicYearModels_1.default);
        res.status(201).json({ message: "Academic year updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateAcademicYear = updateAcademicYear;
const deleteAcademicYear = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield AcademicYearRepository_1.default.deleteAcademicYear(AcademicYearModels_1.default, _id);
        res.status(201).json({ message: "Academic year deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteAcademicYear = deleteAcademicYear;
