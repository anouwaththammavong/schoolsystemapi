"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteClassLevel = exports.updateClassLevel = exports.getClassLevel = exports.createClassLevel = void 0;
const classLevelModels_1 = __importDefault(require("../domain/models/classLevelModels"));
const classLevelRepository_1 = __importDefault(require("../data/repositories/classLevelRepository"));
const createClassLevel = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const classLevelData = req.body;
        yield classLevelRepository_1.default.createClassLevel(classLevelModels_1.default, classLevelData);
        res.status(201).json({ message: "Class Level created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createClassLevel = createClassLevel;
const getClassLevel = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const classLevels = yield classLevelRepository_1.default.getClassLevel(classLevelModels_1.default, limit, offset);
        res.status(200).json({ classLevels });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getClassLevel = getClassLevel;
const updateClassLevel = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield classLevelRepository_1.default.updateClassLevel(classLevelModels_1.default, _id, reqBody);
        res.status(201).json({ message: "ClassLevel updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateClassLevel = updateClassLevel;
const deleteClassLevel = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield classLevelRepository_1.default.deleteClassLevel(classLevelModels_1.default, _id);
        res.status(201).json({ message: "Class Level successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteClassLevel = deleteClassLevel;
