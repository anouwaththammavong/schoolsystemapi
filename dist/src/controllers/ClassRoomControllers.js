"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteClassroom = exports.updateClassroom = exports.getClassRooms = exports.createClassRoom = void 0;
const ClassRoomModels_1 = __importDefault(require("../domain/models/ClassRoomModels"));
const ClassRoomRepository_1 = __importDefault(require("../data/repositories/ClassRoomRepository"));
const createClassRoom = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const classRoomData = req.body;
        yield ClassRoomRepository_1.default.createClassRoom(ClassRoomModels_1.default, classRoomData);
        res.status(201).json({ message: "Class room created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createClassRoom = createClassRoom;
const getClassRooms = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const classrooms = yield ClassRoomRepository_1.default.getClassRoom(ClassRoomModels_1.default, limit, offset);
        res.status(200).json({ classrooms });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getClassRooms = getClassRooms;
const updateClassroom = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield ClassRoomRepository_1.default.updateClassroom(ClassRoomModels_1.default, _id, reqBody);
        res.status(201).json({ message: "Classroom updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateClassroom = updateClassroom;
const deleteClassroom = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield ClassRoomRepository_1.default.deleteClassroom(ClassRoomModels_1.default, _id);
        res.status(201).json({ message: "Classroom successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteClassroom = deleteClassroom;
