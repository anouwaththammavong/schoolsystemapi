"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteRegistration = exports.updateNewRegistration = exports.updateRegistration = exports.getRegistrations = exports.createNewStudentRegistration = exports.createOldStudentRegistration = void 0;
const RegistrationModels_1 = __importDefault(require("../domain/models/RegistrationModels"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const RegistrationRepository_1 = __importDefault(require("../data/repositories/RegistrationRepository"));
const createOldStudentRegistration = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const registrationData = req.body;
        yield RegistrationRepository_1.default.createOldStudentRegistration(RegistrationModels_1.default, studentModels_1.default, registrationData);
        res.status(201).json({ message: "Registration created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createOldStudentRegistration = createOldStudentRegistration;
const createNewStudentRegistration = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const registrationData = req.body;
        yield RegistrationRepository_1.default.createNewStudentRegistration(RegistrationModels_1.default, studentModels_1.default, registrationData);
        res.status(201).json({ message: "Registration created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createNewStudentRegistration = createNewStudentRegistration;
const getRegistrations = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const new_student = req.query.new_student;
    const academic_year_no = req.query.academic_year_no;
    const room_id = req.query.room_id;
    const student_id = req.query.student_id;
    const isPaid = req.query.isPaid;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const registrations = yield RegistrationRepository_1.default.getRegistrations(RegistrationModels_1.default, studentModels_1.default, new_student, academic_year_no, room_id, student_id, isPaid, limit, offset);
        res.status(200).json({ registrations });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getRegistrations = getRegistrations;
const updateRegistration = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield RegistrationRepository_1.default.updateRegistration(_id, reqBody, RegistrationModels_1.default, studentModels_1.default);
        res.status(201).json({ message: "Registration updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateRegistration = updateRegistration;
const updateNewRegistration = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield RegistrationRepository_1.default.updateNewRegistration(_id, reqBody, RegistrationModels_1.default, studentModels_1.default);
        res.status(201).json({ message: "Registration updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateNewRegistration = updateNewRegistration;
const deleteRegistration = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield RegistrationRepository_1.default.deleteRegistration(_id, RegistrationModels_1.default);
        res.status(201).json({ message: "Registration deleted successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteRegistration = deleteRegistration;
