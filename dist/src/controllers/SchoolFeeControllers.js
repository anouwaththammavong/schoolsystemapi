"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSchoolFee = exports.updateSchoolFee = exports.getSchoolFees = exports.createSchoolFee = void 0;
const SchoolFeeRepository_1 = __importDefault(require("../data/repositories/SchoolFeeRepository"));
const SchoolFeeModels_1 = __importDefault(require("../domain/models/SchoolFeeModels"));
const createSchoolFee = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const schoolFeeData = req.body;
        yield SchoolFeeRepository_1.default.createSchoolFee(SchoolFeeModels_1.default, schoolFeeData);
        res.status(201).json({ message: "School fee created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createSchoolFee = createSchoolFee;
const getSchoolFees = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const academic_year_no = req.query.academic_year_no;
    const level_id = req.query.level_id;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const school_fees = yield SchoolFeeRepository_1.default.getSchoolFees(SchoolFeeModels_1.default, academic_year_no, level_id, limit, offset);
        // Respond with the retrieved students
        res.status(200).json({ school_fees });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getSchoolFees = getSchoolFees;
const updateSchoolFee = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield SchoolFeeRepository_1.default.updateSchoolFee(_id, reqBody, SchoolFeeModels_1.default);
        res.status(201).json({ message: "School fee updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateSchoolFee = updateSchoolFee;
const deleteSchoolFee = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield SchoolFeeRepository_1.default.deleteSchoolFee(SchoolFeeModels_1.default, _id);
        res.status(201).json({ message: "School fee successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteSchoolFee = deleteSchoolFee;
