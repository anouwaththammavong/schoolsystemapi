"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteScore = exports.updateScore = exports.getScores = exports.createScore = void 0;
const ScoreModels_1 = __importDefault(require("../domain/models/ScoreModels"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const ScoreRepository_1 = __importDefault(require("../data/repositories/ScoreRepository"));
const createScore = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const scoreData = req.body;
        yield ScoreRepository_1.default.createScore(studentModels_1.default, ScoreModels_1.default, scoreData);
        res.status(201).json({ message: "Score created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createScore = createScore;
const getScores = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const room_id = req.query.room_id;
    const generation = req.query.generation;
    const month = req.query.month;
    const term = req.query.term;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const scores = yield ScoreRepository_1.default.getScores(room_id, generation, month, term, limit, offset, ScoreModels_1.default);
        res.status(200).json({ scores });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getScores = getScores;
const updateScore = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield ScoreRepository_1.default.updateScore(_id, reqBody, studentModels_1.default, ScoreModels_1.default);
        res.status(201).json({ message: "Score updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateScore = updateScore;
// scoreModel: Model<ScoreDocument>, _id: string
const deleteScore = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield ScoreRepository_1.default.deleteScore(ScoreModels_1.default, _id);
        res.status(201).json({ message: "Score deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteScore = deleteScore;
