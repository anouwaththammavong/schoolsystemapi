"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSubjectV2 = exports.updateSubjectV2 = exports.getSubjectV2 = exports.createSubjectV2 = void 0;
const subjectV2Models_1 = __importDefault(require("../domain/models/subjectV2Models"));
const SubjectV2Repository_1 = __importDefault(require("../data/repositories/SubjectV2Repository"));
const createSubjectV2 = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const subjectV2Data = req.body;
        yield SubjectV2Repository_1.default.createSubjectV2(subjectV2Models_1.default, subjectV2Data);
        res.status(201).json({ message: "Subject created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createSubjectV2 = createSubjectV2;
const getSubjectV2 = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const subjectV2s = yield SubjectV2Repository_1.default.getSubjectV2(subjectV2Models_1.default, limit, offset);
        res.status(200).json({ subjectV2s });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getSubjectV2 = getSubjectV2;
const updateSubjectV2 = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield SubjectV2Repository_1.default.updateSubjectV2(subjectV2Models_1.default, _id, reqBody);
        res.status(201).json({ message: "SubjectV2 updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateSubjectV2 = updateSubjectV2;
const deleteSubjectV2 = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield SubjectV2Repository_1.default.deleteSubjectV2(subjectV2Models_1.default, _id);
        res.status(201).json({ message: "Subject successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteSubjectV2 = deleteSubjectV2;
