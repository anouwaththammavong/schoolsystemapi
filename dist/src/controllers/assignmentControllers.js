"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAssignment = void 0;
const assignmentModels_1 = __importDefault(require("../domain/models/assignmentModels"));
const assignmentRepository_1 = __importDefault(require("../data/repositories/assignmentRepository"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const createAssignment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { class_room, class_level, title } = req.body;
    try {
        yield assignmentRepository_1.default.createAssignment(studentModels_1.default, assignmentModels_1.default, class_room, class_level, title);
        res.status(201).json({ message: "Assignment created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createAssignment = createAssignment;
