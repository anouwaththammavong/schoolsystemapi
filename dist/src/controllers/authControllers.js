"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logout = exports.refreshToken = exports.getUser = exports.updateUser = exports.login = exports.register = void 0;
const userModel_1 = __importDefault(require("../domain/models/userModel"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const authRepository_1 = __importDefault(require("../data/repositories/authRepository"));
const register = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { username, password, role } = req.body;
        // check if the user already exists
        const existingUser = yield userModel_1.default.findOne({ username: username });
        if (existingUser) {
            return res.status(409).json({ message: "User already exists" });
        }
        // Hash the password
        const hashedPassword = yield bcrypt_1.default.hash(password, 10);
        // Create a new user
        yield userModel_1.default.create({ username, password: hashedPassword, role });
        res.status(201).json({ message: "User registered successfully" });
    }
    catch (error) {
        res.status(500).json({ message: "Internal server error" });
    }
});
exports.register = register;
const login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { identified, password } = req.body;
        // Check if the user exists in the database
        const user = yield userModel_1.default.findOne({ username: identified });
        if (!user) {
            return res.status(401).json({ message: "Invalid credentials" });
        }
        // compare Password
        const isPasswordValid = yield bcrypt_1.default.compare(password, user.password);
        if (!isPasswordValid) {
            return res.status(401).json({ message: "Invalid credentials" });
        }
        //  Generate JWT token
        const accessToken = authRepository_1.default.generateAccessToken(user);
        const refreshToken = authRepository_1.default.generateRefreshToken(user);
        res.json({ accessToken, refreshToken });
    }
    catch (error) {
        res.status(500).json({ message: "Internal server error" });
    }
});
exports.login = login;
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    const { username, password, role } = reqBody;
    const user = yield userModel_1.default.findById(_id);
    if (user) {
        try {
            if (user.username !== username) {
                // check if the user already exists
                const existingUser = yield userModel_1.default.findOne({ username: username });
                if (existingUser) {
                    return res
                        .status(409)
                        .json({ message: "ຂໍ້ມູນຜູ້ໃຊ້ນີ້ມີໃນລະບົບແລ້ວ" });
                }
            }
            // Only hash the password if it is provided
            if (password) {
                user.password = yield bcrypt_1.default.hash(password, 10);
                console.log("hi");
            }
            user.username = username || user.username;
            user.role = role || user.role;
            yield user.save();
            res.status(201).json({ message: "User update successfully" });
        }
        catch (error) {
            res.status(500).json({ message: "Internal server error" });
        }
    }
    else {
        throw new Error("ບໍ່ພົບຂໍ້ມູນຜູ້ໃຊ້");
    }
});
exports.updateUser = updateUser;
const getUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const totalUser = yield userModel_1.default.countDocuments();
        const users = yield userModel_1.default
            .find()
            .limit(Number(limit))
            .skip(Number(offset));
        res.status(200).json({ users, totalUser });
    }
    catch (error) {
        throw new Error("Failed to get user: " + error.message);
    }
});
exports.getUser = getUser;
const refreshToken = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const refreshToken = req.body.refreshToken;
        // verify the refresh token
        const decoded = authRepository_1.default.verifyRefreshToken(refreshToken);
        if (!decoded) {
            return res.sendStatus(403);
        }
        else {
            // Generate JWT token
            const accessToken = authRepository_1.default.generateAccessToken(decoded);
            const refreshToken = authRepository_1.default.generateRefreshToken(decoded);
            res.json({ accessToken, refreshToken });
        }
    }
    catch (error) {
        res.status(500).json({ message: "Internal server error" });
    }
});
exports.refreshToken = refreshToken;
const logout = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.sendStatus(204);
});
exports.logout = logout;
