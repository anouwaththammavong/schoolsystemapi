"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updatePayStudentTuition = exports.getPayStudentTuition = void 0;
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const payStudentTuition_1 = __importDefault(require("../domain/models/payStudentTuition"));
const payStudentTuitionRepository_1 = __importDefault(require("../data/repositories/payStudentTuitionRepository"));
const getPayStudentTuition = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const class_room = req.query.class_room;
    const class_level = req.query.class_level;
    const generation = req.query.generation;
    const term_1 = req.query.term_1;
    const term_2 = req.query.term_2;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const pay_student_tuition = yield payStudentTuitionRepository_1.default.getPayStudentTuition(class_room, class_level, generation, term_1, term_2, limit, offset, studentModels_1.default, payStudentTuition_1.default);
        res.status(200).json({ pay_student_tuition });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getPayStudentTuition = getPayStudentTuition;
const updatePayStudentTuition = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield payStudentTuitionRepository_1.default.updatePayStudentTuition(payStudentTuition_1.default, _id, reqBody);
        res
            .status(201)
            .json({ message: "Pay student tuition updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updatePayStudentTuition = updatePayStudentTuition;
