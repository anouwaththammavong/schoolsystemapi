"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStudentAbsence = exports.updateStudentAbsence = exports.getStudentAbsence = exports.getAllStudentAbsence = exports.createStudentAbsence = void 0;
const studentAbsenceModels_1 = __importDefault(require("../domain/models/studentAbsenceModels"));
const studentAbsenceRepository_1 = __importDefault(require("../data/repositories/studentAbsenceRepository"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const createStudentAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const studentAbsenceData = req.body;
        yield studentAbsenceRepository_1.default.createStudentAbsence(studentAbsenceModels_1.default, studentModels_1.default, studentAbsenceData);
        res.status(201).json({ message: "Student absence created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createStudentAbsence = createStudentAbsence;
const getAllStudentAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const room_id = req.query.room_id;
    const generation = req.query.generation;
    const student_id = req.query.student_id;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const studentAbsence = yield studentAbsenceRepository_1.default.getAllStudentAbsence(room_id, generation, student_id, limit, offset, studentModels_1.default, studentAbsenceModels_1.default);
        res.status(200).json({ studentAbsence });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getAllStudentAbsence = getAllStudentAbsence;
const getStudentAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const room_id = req.query.room_id;
    const absence_date = req.query.absence_date;
    const student_id = req.query.student_id;
    const term = req.query.term;
    const be_reasonable = req.query.be_reasonable;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const studentAbsence = yield studentAbsenceRepository_1.default.getStudentAbsence(room_id, absence_date, student_id, term, be_reasonable, limit, offset, studentAbsenceModels_1.default);
        res.status(200).json({ studentAbsence });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getStudentAbsence = getStudentAbsence;
const updateStudentAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield studentAbsenceRepository_1.default.updateStudentAbsence(_id, reqBody, studentAbsenceModels_1.default);
        res.status(201).json({ message: "Student absence updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateStudentAbsence = updateStudentAbsence;
const deleteStudentAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield studentAbsenceRepository_1.default.deleteStudentAbsence(studentAbsenceModels_1.default, _id);
        res.status(201).json({ message: "Student absence successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteStudentAbsence = deleteStudentAbsence;
