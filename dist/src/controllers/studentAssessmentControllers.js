"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStudentAssessment = exports.updateStudentAssessment = exports.getStudentAssessment = exports.createStudentAssessment = void 0;
const studentAssessmentModels_1 = __importDefault(require("../domain/models/studentAssessmentModels"));
const studentAssessmentRepository_1 = __importDefault(require("../data/repositories/studentAssessmentRepository"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const createStudentAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const studentAssessmentData = req.body;
        yield studentAssessmentRepository_1.default.createStudentAssessment(studentAssessmentModels_1.default, studentAssessmentData);
        res
            .status(201)
            .json({ message: "Student Assessment created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createStudentAssessment = createStudentAssessment;
const getStudentAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const level_id = req.query.level_id;
    const room_id = req.query.room_id;
    const generation = req.query.generation;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const studentAssessments = yield studentAssessmentRepository_1.default.getStudentAssessment(level_id, room_id, generation, limit, offset, studentModels_1.default, studentAssessmentModels_1.default);
        res.status(200).json({ studentAssessments });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getStudentAssessment = getStudentAssessment;
const updateStudentAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield studentAssessmentRepository_1.default.updateStudentAssessment(studentAssessmentModels_1.default, _id, reqBody);
        res
            .status(201)
            .json({ message: "Student Assessment updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateStudentAssessment = updateStudentAssessment;
const deleteStudentAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield studentAssessmentRepository_1.default.deleteStudentAssessment(studentAssessmentModels_1.default, _id);
        res
            .status(201)
            .json({ message: "Student Assessment successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteStudentAssessment = deleteStudentAssessment;
