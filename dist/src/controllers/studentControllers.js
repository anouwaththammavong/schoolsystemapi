"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStudent = exports.updateStudent = exports.getStudent = exports.createStudent = void 0;
const studentRepository_1 = __importDefault(require("../data/repositories/studentRepository"));
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const createStudent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const studentData = req.body;
        yield studentRepository_1.default.createStudent(studentModels_1.default, studentData);
        res.status(201).json({ message: "Student created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createStudent = createStudent;
const getStudent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const student_id = req.query.student_id;
    const name = req.query.name;
    const generation = req.query.generation;
    const level_id = req.query.level_id;
    const new_student = req.query.new_student;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        // Call the getStudents function with the Mongoose model and any filters from the request
        const students = yield studentRepository_1.default.getStudents(studentModels_1.default, student_id, name, generation, level_id, new_student, limit, offset);
        // Respond with the retrieved students
        res.status(200).json({ students });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getStudent = getStudent;
const updateStudent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield studentRepository_1.default.updateStudent(studentModels_1.default, _id, reqBody);
        res.status(201).json({ message: "Student updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateStudent = updateStudent;
const deleteStudent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield studentRepository_1.default.deleteStudent(studentModels_1.default, _id);
        res.status(201).json({ message: "Student successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteStudent = deleteStudent;
