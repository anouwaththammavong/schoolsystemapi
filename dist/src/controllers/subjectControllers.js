"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSubject = exports.updateSubject = exports.getSubject = void 0;
const studentModels_1 = __importDefault(require("../domain/models/studentModels"));
const subjectRepository_1 = __importDefault(require("../data/repositories/subjectRepository"));
const subjectModels_1 = __importDefault(require("../domain/models/subjectModels"));
const getSubject = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const class_room = req.query.class_room;
    const class_level = req.query.class_level;
    const term = req.query.term;
    const generation = req.query.generation;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const subjects = yield subjectRepository_1.default.getSubject(class_room, class_level, term, generation, limit, offset, studentModels_1.default, subjectModels_1.default);
        res.status(200).json({ subjects });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getSubject = getSubject;
const updateSubject = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield subjectRepository_1.default.updateSubject(subjectModels_1.default, _id, reqBody);
        res.status(201).json({ message: "Subject updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateSubject = updateSubject;
const deleteSubject = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield subjectRepository_1.default.deleteSubject(subjectModels_1.default, _id);
        res.status(201).json({ message: "Student successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteSubject = deleteSubject;
