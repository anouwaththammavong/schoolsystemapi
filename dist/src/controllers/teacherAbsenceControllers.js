"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTeacherAbsence = exports.updateTeacherAbsence = exports.getTeacherAbsence = exports.createTeacherAbsence = void 0;
const teacherAbsenceModels_1 = __importDefault(require("../domain/models/teacherAbsenceModels"));
const teacherAbsenceRepository_1 = __importDefault(require("../data/repositories/teacherAbsenceRepository"));
const createTeacherAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const teacherAbsenceData = req.body;
        yield teacherAbsenceRepository_1.default.createTeacherAbsence(teacherAbsenceModels_1.default, teacherAbsenceData);
        res.status(201).json({ message: "Teacher absence created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createTeacherAbsence = createTeacherAbsence;
const getTeacherAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const absence_date = req.query.absence_date;
    const teacher_id = req.query.teacher_id;
    const be_reasonable = req.query.be_reasonable;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const teacherAbsence = yield teacherAbsenceRepository_1.default.getTeacherAbsenceRepository(absence_date, teacher_id, be_reasonable, limit, offset, teacherAbsenceModels_1.default);
        res.status(200).json({ teacherAbsence });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getTeacherAbsence = getTeacherAbsence;
const updateTeacherAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield teacherAbsenceRepository_1.default.updateTeacherAbsenceRepository(_id, reqBody, teacherAbsenceModels_1.default);
        res.status(201).json({ message: "Teacher absence updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateTeacherAbsence = updateTeacherAbsence;
const deleteTeacherAbsence = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield teacherAbsenceRepository_1.default.deleteTeacherAbsence(teacherAbsenceModels_1.default, _id);
        res.status(201).json({ message: "Teacher absence successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteTeacherAbsence = deleteTeacherAbsence;
