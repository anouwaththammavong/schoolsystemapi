"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTeacherAssessment = exports.updateTeacherAssessment = exports.getTeacherAssessment = exports.createTeacherAssessment = void 0;
const TeacherAssessmentModels_1 = __importDefault(require("../domain/models/TeacherAssessmentModels"));
const teacherModels_1 = __importDefault(require("../domain/models/teacherModels"));
const TeacherAssessmentRepository_1 = __importDefault(require("../data/repositories/TeacherAssessmentRepository"));
const createTeacherAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const teacherAssessmentData = req.body;
        yield TeacherAssessmentRepository_1.default.createTeacherAssessment(TeacherAssessmentModels_1.default, teacherModels_1.default, teacherAssessmentData);
        res
            .status(201)
            .json({ message: "Teacher Assessment created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createTeacherAssessment = createTeacherAssessment;
const getTeacherAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const teacher_id = req.query.teacher_id;
    const assessment_date = req.query.assessment_date;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        const teacherAssessments = yield TeacherAssessmentRepository_1.default.getTeacherAssessment(teacher_id, assessment_date, limit, offset, TeacherAssessmentModels_1.default, teacherModels_1.default);
        res.status(200).json({ teacherAssessments });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getTeacherAssessment = getTeacherAssessment;
const updateTeacherAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield TeacherAssessmentRepository_1.default.updateTeacherAssessment(_id, reqBody, TeacherAssessmentModels_1.default, teacherModels_1.default);
        res
            .status(201)
            .json({ message: "Teacher Assessment updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateTeacherAssessment = updateTeacherAssessment;
const deleteTeacherAssessment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield TeacherAssessmentRepository_1.default.deleteTeacherAssessment(TeacherAssessmentModels_1.default, _id);
        res
            .status(201)
            .json({ message: "Teacher Assessment successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteTeacherAssessment = deleteTeacherAssessment;
