"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTeacher = exports.updateTeacher = exports.getTeachers = exports.createTeacher = void 0;
const teacherModels_1 = __importDefault(require("../domain/models/teacherModels"));
const teacherRepository_1 = __importDefault(require("../data/repositories/teacherRepository"));
const createTeacher = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const teacherData = req.body;
        yield teacherRepository_1.default.createTeacher(teacherModels_1.default, teacherData);
        res.status(201).json({ message: "Teacher created successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.createTeacher = createTeacher;
const getTeachers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const teacher_id = req.query.teacher_id;
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
        // Call the getStudents function with the Mongoose model and any filters from the request
        const teachers = yield teacherRepository_1.default.getTeachers(teacherModels_1.default, teacher_id, limit, offset);
        // Respond with the retrieved students
        res.status(200).json({ teachers });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.getTeachers = getTeachers;
const updateTeacher = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        yield teacherRepository_1.default.updateTeacher(teacherModels_1.default, _id, reqBody);
        res.status(201).json({ message: "Teacher updated successfully" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.updateTeacher = updateTeacher;
const deleteTeacher = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _id = req.params.id;
    try {
        yield teacherRepository_1.default.deleteTeacher(teacherModels_1.default, _id);
        res.status(201).json({ message: "Teacher successfully deleted" });
    }
    catch (error) {
        res
            .status(500)
            .json({ message: "Internal server error", error: error.message });
    }
});
exports.deleteTeacher = deleteTeacher;
