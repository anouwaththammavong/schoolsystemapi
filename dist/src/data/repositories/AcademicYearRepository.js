"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const academicYearRepository = {
    createAcademicYear: (academicYearModel, academicYearData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return academicYearModel.create(academicYearData);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create academic year: " + error.message);
        }
    }),
    getAcademicYear: (limit, offset, academicYearModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const totalAcademicYears = yield academicYearModel.countDocuments();
            const academicYear = yield academicYearModel
                .find()
                .sort({ academic_year_no: 1 })
                .limit(limit)
                .skip(offset);
            return {
                academicYear,
                totalAcademicYears,
            };
        }
        catch (error) {
            throw new Error("Failed to get academic year: " + error.message);
        }
    }),
    updateAcademicYear: (_id, reqBody, academicYearModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { academic_year, academic_year_no } = reqBody;
        const academicYear = yield academicYearModel.findById(_id);
        if (academicYear) {
            try {
                academicYear.academic_year =
                    academic_year || academicYear.academic_year;
                academicYear.academic_year_no =
                    academic_year_no || academicYear.academic_year_no;
                yield academicYear.save();
            }
            catch (error) {
                throw new Error("Failed to update academic year: " + error.message);
            }
        }
        else {
            throw new Error("no academic year information found");
        }
    }),
    deleteAcademicYear: (academicYearModel, id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const academicYear = yield academicYearModel.deleteOne({ _id: id });
            if (academicYear.deletedCount === 0) {
                throw new Error("Academic year not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete academic year: " + error.message);
        }
    }),
};
exports.default = academicYearRepository;
