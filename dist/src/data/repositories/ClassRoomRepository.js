"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const classRoomRepository = {
    createClassRoom: (classRoomModel, classRoomData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const existingRoomId = yield classRoomModel.findOne({
                room_id: classRoomData.room_id,
            });
            if (existingRoomId) {
                throw new Error("ໄອດີຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
            }
            // Check if class room name if is unique
            // const existingClassRoomName = await classRoomModel.findOne({
            //   room_name: classRoomData.room_name,
            // });
            // if (existingClassRoomName) {
            //   throw new Error("ຊື່ຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
            // }
            const existingLevelId = yield classRoomModel.findOne({
                level_id: classRoomData.level_id,
            });
            if (existingLevelId) {
                throw new Error("ໄອດີຂອງຊັ້ນຮຽນໃນຫ້ອງນີ້ແມ່ນມີແລ້ວ");
            }
            return classRoomModel.create(classRoomData);
        }
        catch (error) {
            throw new Error("Failed to create class room: " + error.message);
        }
    }),
    getClassRoom: (classRoomModel, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const totalCount = yield classRoomModel.countDocuments();
            const classRooms = yield classRoomModel
                .find()
                .populate("level_id")
                .sort({ room_name: 1 })
                .limit(limit)
                .skip(offset)
                .exec();
            return { classRooms, totalCount };
        }
        catch (error) {
            throw new Error("Failed to get classroom: " + error.message);
        }
    }),
    updateClassroom: (classRoomModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { level_id, room_id, room_name } = reqBody;
        const classroom = yield classRoomModel.findOne({ _id: _id });
        if (classroom) {
            try {
                if (String(classroom.level_id) !== String(level_id)) {
                    const existingLevelId = yield classRoomModel.findOne({
                        level_id: level_id,
                    });
                    if (existingLevelId) {
                        throw new Error("ໄອດີຂອງຊັ້ນຮຽນໃນຫ້ອງນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                if (classroom.room_id !== room_id) {
                    const existingRoomId = yield classRoomModel.findOne({
                        room_id: room_id,
                    });
                    if (existingRoomId) {
                        throw new Error("ໄອດີຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                // if (classroom.room_name !== room_name) {
                //   // Check if class room name if is unique
                //   const existingClassRoomName = await classRoomModel.findOne({
                //     room_name: room_name,
                //   });
                //   if (existingClassRoomName) {
                //     throw new Error("ຊື່ຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
                //   }
                // }
                classroom.level_id = level_id || classroom.level_id;
                classroom.room_id = room_id || classroom.room_id;
                classroom.room_name = room_name || classroom.room_name;
                yield classroom.save();
            }
            catch (error) {
                throw new Error("Failed to update classroom: " + error.message);
            }
        }
        else {
            throw new Error("no class room information found");
        }
    }),
    deleteClassroom: (classRoomModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const classroom = yield classRoomModel.deleteOne({ _id: _id });
            if (classroom.deletedCount === 0) {
                throw new Error("Classroom not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete Classroom: " + error.message);
        }
    }),
};
exports.default = classRoomRepository;
