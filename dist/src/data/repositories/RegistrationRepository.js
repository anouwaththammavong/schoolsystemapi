"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const registrationRepository = {
    createOldStudentRegistration: (registrationModel, studentModel, registrationData) => __awaiter(void 0, void 0, void 0, function* () {
        // const student = await studentModel.findOne({
        //   student_id: registrationData.student_id,
        // });
        // if (student) {
        //   try {
        //     // Create the registration with the student's ObjectId
        //     const registration = await registrationModel.create({
        //       registration_id: registrationData.registration_id,
        //       student_id: student._id, // Reference to Student
        //       room_id: registrationData.room_id, // Reference to ClassRoom
        //       level_id: registrationData.level_id, // Reference to Class Level
        //       academic_year_no: registrationData.academic_year_no, // Reference to Academic Year
        //       school_fee_id: registrationData.school_fee_id,
        //       registration_date: registrationData.registration_date,
        //       isPaid: registrationData.isPaid,
        //     });
        //     student.room_id = registrationData.room_id;
        //     student.level_id = registrationData.level_id;
        //     await student.save();
        //     return registration
        //   } catch (error: any) {
        //     throw new Error("Internal server error" + (error as Error).message);
        //   }
        // } else {
        //   throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
        // }
        const session = yield mongoose_1.default.startSession();
        session.startTransaction();
        try {
            // Find the student using the provided student_id
            const student = yield studentModel
                .findOne({ student_id: registrationData.student_id })
                .session(session);
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            const existingRegistration_id = yield registrationModel
                .findOne({
                registration_id: registrationData.registration_id,
            })
                .session(session);
            if (existingRegistration_id) {
                throw new Error("ໄອດີຂອງການລົງທະບຽນນີ້ແມ່ນມີແລ້ວ");
            }
            const existingSchool_fee_id = yield registrationModel
                .findOne({
                student_id: student._id,
                room_id: registrationData.room_id,
                level_id: registrationData.level_id,
                academic_year_no: registrationData.academic_year_no,
                school_fee_id: registrationData.school_fee_id,
            })
                .session(session);
            if (existingSchool_fee_id) {
                throw new Error("ຂໍ້ມູນຂອງການລົງທະບຽນຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
            // Create the registration with the student's ObjectId
            const registration = new registrationModel({
                registration_id: registrationData.registration_id,
                student_id: student._id, // Reference the ObjectId of the student
                room_id: registrationData.room_id, // Example ObjectId
                level_id: registrationData.level_id, // Example ObjectId
                academic_year_no: registrationData.academic_year_no, // Example ObjectId
                school_fee_id: registrationData.school_fee_id, // Example ObjectId
                registration_date: registrationData.registration_date,
            });
            yield registration.save({ session });
            // Update the student's room_id and level_id
            student.room_id = registration.room_id;
            student.level_id = registration.level_id;
            student.new_student = false;
            yield student.save({ session });
            yield session.commitTransaction();
            session.endSession();
            return registration;
        }
        catch (error) {
            yield session.abortTransaction();
            session.endSession();
            throw new Error("ເກີດຂໍ້ຜິດພາດໃນການເພີ່ມຂໍ້ມູນການລົງທະບຽນ: " + error.message);
        }
    }),
    createNewStudentRegistration: (registrationModel, studentModel, registrationData) => __awaiter(void 0, void 0, void 0, function* () {
        const session = yield mongoose_1.default.startSession();
        session.startTransaction();
        try {
            // Find the student using the provided student_id
            const student = yield studentModel
                .findOne({ student_id: registrationData.student_id })
                .session(session);
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            const existingRegistration_id = yield registrationModel
                .findOne({
                registration_id: registrationData.registration_id,
            })
                .session(session);
            if (existingRegistration_id) {
                throw new Error("ໄອດີຂອງການລົງທະບຽນນີ້ແມ່ນມີແລ້ວ");
            }
            const existingSchool_fee_id = yield registrationModel
                .findOne({
                student_id: student._id,
                room_id: registrationData.room_id,
                level_id: registrationData.level_id,
                academic_year_no: registrationData.academic_year_no,
                school_fee_id: registrationData.school_fee_id,
            })
                .session(session);
            if (existingSchool_fee_id) {
                throw new Error("ຂໍ້ມູນຂອງການລົງທະບຽນຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
            // Create the registration with the student's ObjectId
            const registration = new registrationModel({
                registration_id: registrationData.registration_id,
                student_id: student._id, // Reference the ObjectId of the student
                room_id: student.room_id, // Example ObjectId
                level_id: student.level_id, // Example ObjectId
                academic_year_no: registrationData.academic_year_no, // Example ObjectId
                school_fee_id: registrationData.school_fee_id, // Example ObjectId
                registration_date: registrationData.registration_date,
            });
            yield registration.save({ session });
            // Update the student's room_id and level_id
            student.room_id = registration.room_id;
            student.level_id = registration.level_id;
            yield student.save({ session });
            yield session.commitTransaction();
            session.endSession();
            return registration;
        }
        catch (error) {
            yield session.abortTransaction();
            session.endSession();
            throw new Error("ເກີດຂໍ້ຜິດພາດໃນການເພີ່ມຂໍ້ມູນການລົງທະບຽນ: " + error.message);
        }
    }),
    getRegistrations: (registrationModel, studentModel, new_student, academic_year_no, room_id, student_id, isPaid, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            // Initialize the conditions as an empty object
            let conditions = {};
            // If student_id is provided, find the corresponding _id from the students collection
            if (student_id) {
                const student = yield studentModel.findOne({ student_id }).exec();
                if (student) {
                    conditions.student_id = student._id;
                }
                else {
                    throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
                }
            }
            // Set the isPaid condition
            //   if (isPaid !== undefined) {
            //     conditions.isPaid =
            //       typeof isPaid === "string" ? isPaid === "true" : isPaid;
            //   }
            if (isPaid !== undefined) {
                conditions.isPaid =
                    isPaid;
            }
            if (
            // new_student !== undefined ||
            academic_year_no ||
                room_id
            // student_id
            // isPaid
            ) {
                conditions = Object.assign(Object.assign(Object.assign({}, conditions), (academic_year_no && {
                    academic_year_no: new mongoose_1.default.Types.ObjectId(academic_year_no),
                })), (room_id && { room_id: new mongoose_1.default.Types.ObjectId(room_id) }));
                // if (new_student !== undefined) {
                //     if (typeof new_student === 'string') {
                //       conditions["student_id.new_student"] = new_student === 'true';
                //     } else {
                //       conditions["student_id.new_student"] = new_student;
                //     }
                //   }
            }
            // Construct the countConditions array for aggregation pipeline
            const countConditions = [{ $match: conditions }];
            // If new_student is provided, add the lookup and match stages
            if (new_student !== undefined) {
                countConditions.push({
                    $lookup: {
                        from: "students",
                        localField: "student_id",
                        foreignField: "_id",
                        as: "student",
                    },
                });
                countConditions.push({ $unwind: "$student" });
                countConditions.push({
                    $match: {
                        "student.new_student": typeof new_student === "string"
                            ? new_student === "true"
                            : new_student,
                    },
                });
            }
            const countResult = yield registrationModel.aggregate([
                ...countConditions,
                { $count: "totalCount" },
            ]);
            const totalCount = countResult.length > 0 ? countResult[0].totalCount : 0;
            // Retrieve results based on the conditions
            const registrationsQuery = yield registrationModel
                .find(conditions)
                .populate({
                path: "student_id",
                model: studentModel,
                match: new_student !== undefined
                    ? typeof new_student === "string"
                        ? { new_student: new_student === "true" }
                        : { new_student: new_student }
                    : {},
                populate: [
                    { path: "room_id", model: "ClassRoom" },
                    { path: "level_id", model: "ClassLevel" },
                ],
            })
                .populate("room_id")
                .populate("level_id")
                .populate("academic_year_no")
                .populate("school_fee_id")
                .sort({ registration_date: 1 })
                .limit(Number(limit))
                .skip(Number(offset))
                .exec();
            // Filter out the results where student_id is null due to the match condition
            const filteredRegistrations = registrationsQuery.filter((reg) => reg.student_id !== null);
            return { registrations: filteredRegistrations, totalCount };
        }
        catch (error) {
            throw new Error("Failed to get registrations: " + error.message);
        }
    }),
    updateRegistration: (_id, reqBody, registrationModel, studentModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { registration_id, student_id, room_id, level_id, academic_year_no, school_fee_id, registration_date, isPaid, } = reqBody;
        // Start a session
        const session = yield mongoose_1.default.startSession();
        session.startTransaction();
        try {
            // Find the existing registration within the session
            const registration = yield registrationModel
                .findById(_id)
                .session(session);
            if (!registration) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນການລົງທະບຽນນີ້");
            }
            // Find the student using the provided student_id
            const student = yield studentModel
                .findOne({ student_id: student_id })
                .session(session);
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            if (String(registration.registration_id) !== String(registration_id)) {
                const existingRegistration_id = yield registrationModel
                    .findOne({
                    registration_id: registration_id,
                })
                    .session(session);
                if (existingRegistration_id) {
                    throw new Error("ໄອດີຂອງການລົງທະບຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            if (String(registration.school_fee_id) !== String(school_fee_id)) {
                const existingSchool_fee_id = yield registrationModel
                    .findOne({
                    student_id: student._id,
                    room_id: room_id,
                    level_id: level_id,
                    academic_year_no: academic_year_no,
                    school_fee_id: school_fee_id,
                })
                    .session(session);
                if (existingSchool_fee_id) {
                    throw new Error("ຂໍ້ມູນຂອງການລົງທະບຽນຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            // Update the registration fields
            registration.registration_id = registration_id || registration.registration_id;
            registration.student_id = student._id || registration.student_id;
            registration.room_id = room_id || registration.room_id;
            registration.level_id = level_id || registration.level_id;
            registration.academic_year_no = academic_year_no || registration.academic_year_no;
            registration.school_fee_id = school_fee_id || registration.school_fee_id;
            registration.registration_date = registration_date || registration.registration_date;
            registration.isPaid = isPaid || registration.isPaid;
            // Save the updated registration within the session
            yield registration.save({ session });
            // Update the student's room_id and level_id
            student.room_id = registration.room_id || student.room_id;
            student.level_id = registration.level_id || student.level_id;
            yield student.save({ session });
            yield session.commitTransaction();
            session.endSession();
            return registration;
        }
        catch (error) {
            yield session.abortTransaction();
            session.endSession();
            throw new Error("ເກີດຂໍ້ຜິດພາກໃນການແກ້ໄຂຂໍ້ມູນການລົງທະບຽນ: " + error.message);
        }
    }),
    updateNewRegistration: (_id, reqBody, registrationModel, studentModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { registration_id, student_id, academic_year_no, school_fee_id, registration_date, isPaid, } = reqBody;
        // Start a session
        const session = yield mongoose_1.default.startSession();
        session.startTransaction();
        try {
            // Find the existing registration within the session
            const registration = yield registrationModel
                .findById(_id)
                .session(session);
            if (!registration) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນການລົງທະບຽນນີ້");
            }
            // Find the student using the provided student_id
            const student = yield studentModel
                .findOne({ student_id: student_id })
                .session(session);
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            if (String(registration.registration_id) !== String(registration_id)) {
                const existingRegistration_id = yield registrationModel
                    .findOne({
                    registration_id: registration_id,
                })
                    .session(session);
                if (existingRegistration_id) {
                    throw new Error("ໄອດີຂອງການລົງທະບຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            if (String(registration.school_fee_id) !== String(school_fee_id)) {
                const existingSchool_fee_id = yield registrationModel
                    .findOne({
                    student_id: student._id,
                    room_id: student.room_id,
                    level_id: student.level_id,
                    academic_year_no: academic_year_no,
                    school_fee_id: school_fee_id,
                })
                    .session(session);
                if (existingSchool_fee_id) {
                    throw new Error("ຂໍ້ມູນຂອງການລົງທະບຽນຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            // Update the registration fields
            registration.registration_id = registration_id || registration.registration_id;
            registration.student_id = student._id || registration.student_id;
            registration.room_id = student.room_id || registration.room_id;
            registration.level_id = student.level_id || registration.level_id;
            registration.academic_year_no = academic_year_no || registration.academic_year_no;
            registration.school_fee_id = school_fee_id || registration.school_fee_id;
            registration.registration_date = registration_date || registration.registration_date;
            registration.isPaid = isPaid || registration.isPaid;
            // Save the updated registration within the session
            yield registration.save({ session });
            // Update the student's room_id and level_id
            student.room_id = registration.room_id || student.room_id;
            student.level_id = registration.level_id || student.level_id;
            yield student.save({ session });
            yield session.commitTransaction();
            session.endSession();
            return registration;
        }
        catch (error) {
            yield session.abortTransaction();
            session.endSession();
            throw new Error("ເກີດຂໍ້ຜິດພາກໃນການແກ້ໄຂຂໍ້ມູນການລົງທະບຽນ: " + error.message);
        }
    }),
    deleteRegistration: (_id, registrationModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const registration = yield registrationModel.deleteOne({ _id: _id });
            if (registration.deletedCount === 0) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນການລົງທະບຽນນີ້");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("ເກີດຂໍ້ຜິດພາດໃນການລົບຂໍ້ມູນການລົງທະບຽນ: " + error.message);
        }
    }),
};
exports.default = registrationRepository;
// getRegistrations: async (
//     registrationModel: Model<RegistrationDocument>,
//     studentModel: Model<StudentDocument>,
//     new_student: any,
//     academic_year_no: any,
//     room_id: any,
//     student_id: any,
//     limit: any,
//     offset: any
//   ) => {
//     try {
//       // Initialize the conditions as an empty object
//       let conditions: any = {};
//       // Construct the query based on the provided parameters
//       if (new_student !== undefined || academic_year_no || room_id || student_id) {
//         conditions = {
//           ...(academic_year_no && { academic_year_no }),
//           ...(room_id && { room_id }),
//           ...(student_id && { student_id }),
//         };
//       }
//       // Use aggregate pipeline to handle the nested new_student field
//       const pipeline: any[] = [
//         { $match: conditions },
//         {
//           $lookup: {
//             from: "students", // Ensure this matches your actual collection name for students
//             localField: "student_id",
//             foreignField: "_id",
//             as: "student_info",
//           },
//         },
//         { $unwind: "$student_info" },
//         ...(new_student !== undefined
//           ? [{ $match: { "student_info.new_student": new_student } }]
//           : []),
//         {
//           $lookup: {
//             from: "classrooms",
//             localField: "room_id",
//             foreignField: "_id",
//             as: "room_id",
//           },
//         },
//         { $unwind: "$room_id" },
//         {
//           $lookup: {
//             from: "classlevels",
//             localField: "level_id",
//             foreignField: "_id",
//             as: "level_id",
//           },
//         },
//         { $unwind: "$level_id" },
//         {
//           $lookup: {
//             from: "academicyears",
//             localField: "academic_year_no",
//             foreignField: "_id",
//             as: "academic_year_no",
//           },
//         },
//         { $unwind: "$academic_year_no" },
//         {
//           $lookup: {
//             from: "schoolfees",
//             localField: "school_fee_id",
//             foreignField: "_id",
//             as: "school_fee_id",
//           },
//         },
//         { $unwind: "$school_fee_id" },
//         { $sort: { registration_date: 1 } },
//         { $skip: Number(offset) },
//         { $limit: Number(limit) },
//       ];
//       // Execute the aggregate query
//       const registrations = await registrationModel.aggregate(pipeline).exec();
//       // Get total count of documents that match the criteria
//       const totalCount = await registrationModel.countDocuments(conditions);
//       return { registrations, totalCount };
//     } catch (error: any) {
//       throw new Error("Failed to fetch registrations: " + error.message);
//     }
//   },
