"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const schoolFeeRepository = {
    createSchoolFee: (schoolFeeModel, schoolFeeData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const existingSchool_fee_id = yield schoolFeeModel.findOne({
                school_fee_id: schoolFeeData.school_fee_id,
            });
            if (existingSchool_fee_id) {
                throw new Error("ໄອດີຂອງຄ່າທຳນຽມນີ້ແມ່ນມີແລ້ວ");
            }
            const existingAcademic_year_no = yield schoolFeeModel.findOne({
                academic_year_no: schoolFeeData.academic_year_no,
                level_id: schoolFeeData.level_id,
            });
            if (existingAcademic_year_no) {
                throw new Error("ຂໍ້ມູນຂອງສົກຮຽນທີ້ນີ້ ຫຼື ຊັ້ນຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
            return schoolFeeModel.create(schoolFeeData);
        }
        catch (error) {
            throw new Error("Failed to create school fee: " + error.message);
        }
    }),
    getSchoolFees: (schoolFeeModel, academic_year_no, level_id, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filter = {};
            if (academic_year_no) {
                filter = Object.assign(Object.assign({}, filter), { academic_year_no: academic_year_no });
            }
            if (level_id) {
                filter = Object.assign(Object.assign({}, filter), { level_id: level_id });
            }
            const totalCount = yield schoolFeeModel.countDocuments(filter);
            const schoolFees = yield schoolFeeModel
                .find(filter)
                .populate("academic_year_no")
                .populate("level_id")
                .sort({ academic_year_no: 1 })
                .limit(limit)
                .skip(offset)
                .exec();
            return { schoolFees, totalCount };
        }
        catch (error) {
            throw new Error("Failed to get School fee: " + error.message);
        }
    }),
    updateSchoolFee: (_id, reqBody, schoolFeeModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { school_fee_id, academic_year_no, level_id, book_fee, uniform_fee, term_fee, total_fee, } = reqBody;
        const school_fee = yield schoolFeeModel.findById(_id);
        if (school_fee) {
            try {
                if (String(school_fee.school_fee_id) !== String(school_fee_id)) {
                    const existingSchool_fee_id = yield schoolFeeModel.findOne({
                        school_fee_id: school_fee_id,
                    });
                    if (existingSchool_fee_id) {
                        throw new Error("ໄອດີຂອງຄ່າທຳນຽມນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                if (String(school_fee.academic_year_no) !== String(academic_year_no)) {
                    const existingAcademic_year_no = yield schoolFeeModel.findOne({
                        academic_year_no: academic_year_no,
                        level_id: level_id,
                    });
                    if (existingAcademic_year_no) {
                        throw new Error("ຂໍ້ມູນຂອງສົກຮຽນທີ້ນີ້ ຫຼື ຊັ້ນຮຽນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                school_fee.school_fee_id = school_fee_id || school_fee.school_fee_id;
                school_fee.academic_year_no =
                    academic_year_no || school_fee.academic_year_no;
                school_fee.level_id = level_id || school_fee.level_id;
                school_fee.book_fee = book_fee || school_fee.book_fee;
                school_fee.uniform_fee = uniform_fee || school_fee.uniform_fee;
                school_fee.term_fee = term_fee || school_fee.term_fee;
                school_fee.total_fee = total_fee || school_fee.total_fee;
                yield school_fee.save();
            }
            catch (error) {
                throw new Error("Failed to update student absence: " + error.message);
            }
        }
        else {
            throw new Error("no school fee information found");
        }
    }),
    deleteSchoolFee: (schoolFeeModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const schoolFee = yield schoolFeeModel.deleteOne({ _id: _id });
            if (schoolFee.deletedCount === 0) {
                throw new Error("School fee not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete School fee: " + error.message);
        }
    }),
};
exports.default = schoolFeeRepository;
