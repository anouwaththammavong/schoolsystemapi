"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const scoreRepository = {
    createScore: (studentModel, scoreModel, scoreData) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            const student = yield studentModel.findOne({
                student_id: scoreData.student,
            });
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            const score_month = new Date(scoreData.month);
            const startOfMonth = new Date(score_month.getFullYear(), score_month.getMonth(), 1);
            const endOfMonth = new Date(score_month.getFullYear(), score_month.getMonth() + 1, 0, 23, 59, 59, 999);
            const existingStudentScoreMonth = yield scoreModel.findOne({
                student: student._id,
                room_id: scoreData.room_id,
                level_id: scoreData.level_id,
                generation: scoreData.generation,
                month: {
                    $gte: startOfMonth, // Greater than or equal to the start of the month
                    $lt: endOfMonth, // Less than the start of the next month
                },
            });
            if (existingStudentScoreMonth) {
                throw new Error("ຂໍ້ມູນຄະແນນເສັງເດືອນນີ້ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
            if (scoreData.term === "ພາກຮຽນ1") {
                const existingTerm1 = yield scoreModel.findOne({
                    student: student._id,
                    room_id: scoreData.room_id,
                    level_id: scoreData.level_id,
                    generation: scoreData.generation,
                    term: "ພາກຮຽນ1",
                });
                if (existingTerm1) {
                    throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 1 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            if (scoreData.term === "ພາກຮຽນ2") {
                const existingTerm1 = yield scoreModel.findOne({
                    student: student._id,
                    room_id: scoreData.room_id,
                    level_id: scoreData.level_id,
                    generation: scoreData.generation,
                    term: "ພາກຮຽນ2",
                });
                if (existingTerm1) {
                    throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 2 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                }
            }
            const newScore = yield scoreModel.create({
                student: student._id,
                room_id: scoreData.room_id,
                level_id: scoreData.level_id,
                generation: scoreData.generation,
                month: scoreData.month,
                term: scoreData.term,
                subjects: scoreData.subjects,
                total_point: scoreData.subjects.reduce((total, subject) => Number(total) + Number(subject.point), 0),
                rank: scoreData.rank,
            });
            if (!scoreData.rank) {
                // Rank students in the same room_id, level_id, generation, month, term
                const scoresInSameCriteria = yield scoreModel.find({
                    room_id: scoreData.room_id,
                    level_id: scoreData.level_id,
                    generation: scoreData.generation,
                    month: {
                        $gte: startOfMonth, // Greater than or equal to the start of the month
                        $lt: endOfMonth, // Less than the start of the next month
                    },
                    term: scoreData.term,
                });
                // Sort the scores based on their total_point in descending order
                const sortedScores = scoresInSameCriteria.sort((a, b) => b.total_point - a.total_point);
                // update rank for each score
                let rank = 1;
                let prevTotalPoint = ((_a = sortedScores[0]) === null || _a === void 0 ? void 0 : _a.total_point) || 0;
                for (const score of sortedScores) {
                    if (score.total_point < prevTotalPoint) {
                        rank++;
                    }
                    score.rank = rank;
                    yield score.save();
                    prevTotalPoint = score.total_point;
                }
            }
            return newScore;
            //   return scoreModel.create({
            //     student: student._id,
            //     room_id: scoreData.room_id,
            //     level_id: scoreData.level_id,
            //     generation: scoreData.generation,
            //     month: scoreData.month,
            //     term: scoreData.term,
            //     subjects: scoreData.subjects,
            //     total_point: scoreData.total_point,
            //     rank: scoreData.rank,
            //   });
        }
        catch (error) {
            throw new Error("Failed to create score: " + error.message);
        }
    }),
    getScores: (room_id, generation, month, term, limit, offset, scoreModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const convertScoreMonth = new Date(month);
            const scoreYear = convertScoreMonth.getFullYear();
            const scoreMonth = convertScoreMonth.getMonth() + 1;
            let filter = {
                room_id: room_id,
                generation: generation,
                // $expr: {
                //   $and: [
                //     { $eq: [{ $year: "$month" }, scoreYear] }, // Match year
                //     { $eq: [{ $month: "$month" }, scoreMonth] },
                //   ],
                // },
            };
            if (term) {
                // If term is provided, filter by term
                filter.term = term;
            }
            else {
                // If term is not provided, filter by year and month
                filter.term = "ບໍ່ແມ່ນ";
                filter.$expr = {
                    $and: [
                        { $eq: [{ $year: "$month" }, scoreYear] }, // Match year
                        { $eq: [{ $month: "$month" }, scoreMonth] }, // Match month
                    ],
                };
            }
            // Count total score matching the condition before applying limit and skip
            const totalScore = yield scoreModel.countDocuments(filter);
            const scores = yield scoreModel
                .find(filter)
                .populate("student")
                .populate("room_id")
                .populate("level_id")
                .populate({
                path: "subjects",
                populate: [
                    { path: "title", model: "SubjectV2" },
                    { path: "teacher", model: "Teachers" },
                ],
            })
                .sort({ rank: 1 })
                .limit(limit)
                .skip(offset)
                .exec();
            return {
                scores,
                totalScore,
            };
        }
        catch (error) {
            throw new Error("Failed to get score: " + error.message);
        }
    }),
    updateScore: (_id, reqBody, studentModel, scoreModel) => __awaiter(void 0, void 0, void 0, function* () {
        var _b;
        const { student, room_id, level_id, generation, month, term, subjects, rank, } = reqBody;
        const score = yield scoreModel.findById(_id);
        if (score) {
            try {
                // Find the student using the provided student
                const studentOne = yield studentModel.findOne({ student_id: student });
                if (!studentOne) {
                    throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
                }
                const score_month = new Date(month);
                const startOfMonth = new Date(score_month.getFullYear(), score_month.getMonth(), 1);
                const endOfMonth = new Date(score_month.getFullYear(), score_month.getMonth() + 1, 0, 23, 59, 59, 999);
                if (score.month.toISOString() !== new Date(month).toISOString()) {
                    const existingStudentScoreMonth = yield scoreModel.findOne({
                        student: studentOne._id,
                        room_id: room_id || score.room_id,
                        level_id: level_id || score.level_id,
                        generation: generation || score.generation,
                        month: {
                            $gte: startOfMonth, // Greater than or equal to the start of the month
                            $lt: endOfMonth, // Less than the start of the next month
                        },
                    });
                    if (existingStudentScoreMonth) {
                        throw new Error("ຂໍ້ມູນຄະແນນເສັງເດືອນນີ້ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                if (score.term !== term) {
                    if (term === "ພາກຮຽນ1") {
                        const existingTerm1 = yield scoreModel.findOne({
                            student: studentOne._id,
                            room_id: room_id,
                            level_id: level_id,
                            generation: generation,
                            term: "ພາກຮຽນ1",
                        });
                        if (existingTerm1) {
                            throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 1 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                        }
                    }
                    if (term === "ພາກຮຽນ2") {
                        const existingTerm1 = yield scoreModel.findOne({
                            student: studentOne._id,
                            room_id: room_id,
                            level_id: level_id,
                            generation: generation,
                            term: "ພາກຮຽນ2",
                        });
                        if (existingTerm1) {
                            throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 2 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
                        }
                    }
                }
                score.student = studentOne._id || score.student;
                score.room_id = room_id || score.room_id;
                score.level_id = level_id || score.level_id;
                score.generation = generation || score.generation;
                score.month = month || score.month;
                score.term = term || score.term;
                score.subjects = subjects || score.subjects;
                score.total_point = subjects
                    ? subjects.reduce((total, subject) => total + subject.point, 0)
                    : score.total_point;
                score.rank = rank || score.rank;
                yield score.save();
                // Recalculate the rank if needed
                if (!rank) {
                    // Build the common filter criteria
                    const filter = {
                        room_id: room_id || score.room_id,
                        level_id: level_id || score.level_id,
                        generation: generation || score.generation,
                        term: term || score.term,
                    };
                    // If term is "ບໍ່ແມ່ນ", add the month filter
                    if (term === "ບໍ່ແມ່ນ") {
                        filter.month = {
                            $gte: startOfMonth, // Greater than or equal to the start of the month
                            $lt: endOfMonth, // Less than the start of the next month
                        };
                    }
                    // Find scores in the same criteria
                    const scoresInSameCriteria = yield scoreModel.find(filter);
                    // Sort the scores based on their total_point in descending order
                    const sortedScores = scoresInSameCriteria.sort((a, b) => b.total_point - a.total_point);
                    // Update rank for each score
                    let rank = 1;
                    let prevTotalPoint = ((_b = sortedScores[0]) === null || _b === void 0 ? void 0 : _b.total_point) || 0;
                    for (const scores of sortedScores) {
                        if (scores.total_point < prevTotalPoint) {
                            rank++;
                        }
                        scores.rank = rank;
                        yield scores.save();
                        prevTotalPoint = scores.total_point;
                    }
                }
                return score;
            }
            catch (error) {
                throw new Error("Failed to update score: " + error.message);
            }
        }
        else {
            throw new Error("ບໍ່ພົບຂໍ້ມູນຄະແນນເສັງນີ້");
        }
    }),
    deleteScore: (scoreModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const score = yield scoreModel.deleteOne({ _id: _id });
            if (score.deletedCount === 0) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນຄະແນນເສັງນີ້");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete score: " + error.message);
        }
    }),
};
exports.default = scoreRepository;
