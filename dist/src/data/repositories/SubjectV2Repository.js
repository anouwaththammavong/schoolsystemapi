"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const subjectV2Repository = {
    createSubjectV2: (subjectV2Model, subjectV2Data) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            // Check if subject id if is unique
            const existingSubjectV2Id = yield subjectV2Model.findOne({
                subject_id: subjectV2Data.subject_id,
            });
            if (existingSubjectV2Id) {
                throw new Error("ໄອດີຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
            }
            // Check if subject name if is unique
            const existingSubjectV2Name = yield subjectV2Model.findOne({
                subject_name: subjectV2Data.subject_name,
            });
            if (existingSubjectV2Name) {
                throw new Error("ຊື່ຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
            }
            return subjectV2Model.create(subjectV2Data);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create subject: " + error.message);
        }
    }),
    getSubjectV2: (subjectV2Model, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const totalCount = yield subjectV2Model.countDocuments();
            const subjectV2s = yield subjectV2Model
                .find()
                .sort({ subject_name: 1 })
                .limit(Number(limit))
                .skip(Number(offset))
                .exec();
            return { subjectV2s, totalCount };
        }
        catch (error) {
            throw new Error("Failed to get subject: " + error.message);
        }
    }),
    updateSubjectV2: (subjectV2Model, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { subject_id, subject_name } = reqBody;
        const subjectV2 = yield subjectV2Model.findOne({ _id: _id });
        if (subjectV2) {
            try {
                if (subjectV2.subject_id !== subject_id) {
                    const existingSubjectV2Id = yield subjectV2Model.findOne({
                        subject_id: subject_id,
                    });
                    if (existingSubjectV2Id) {
                        throw new Error("ໄອດີຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
                    }
                }
                if (subjectV2.subject_name !== subject_name) {
                    // Check if subject name if is unique
                    const existingSubjectV2Name = yield subjectV2Model.findOne({
                        subject_name: subject_name,
                    });
                    if (existingSubjectV2Name) {
                        throw new Error("ຊື່ຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
                    }
                }
                subjectV2.subject_id = subject_id || subjectV2.subject_id;
                subjectV2.subject_name = subject_name || subjectV2.subject_name;
                yield subjectV2.save();
            }
            catch (error) {
                throw new Error("Failed to update subject: " + error.message);
            }
        }
        else {
            throw new Error("no subject information found");
        }
    }),
    deleteSubjectV2: (subjectV2Model, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const subjectV2 = yield subjectV2Model.deleteOne({ _id: _id });
            if (subjectV2.deletedCount === 0) {
                throw new Error("Subject not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete subject: " + error.message);
        }
    }),
};
exports.default = subjectV2Repository;
