"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const teacherAssessmentRepository = {
    createTeacherAssessment: (teacherAssessmentModel, teacherModel, teacherAssessmentData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const teacher = yield teacherModel.findOne({
                teacher_id: teacherAssessmentData.teacher,
            });
            if (!teacher) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນອາຈານ");
            }
            const assessment_date = new Date(teacherAssessmentData.assessment_date);
            const startOfMonth = new Date(assessment_date.getFullYear(), assessment_date.getMonth(), 1);
            const endOfMonth = new Date(assessment_date.getFullYear(), assessment_date.getMonth() + 1, 0, 23, 59, 59, 999);
            const existingTeacherAssessment = yield teacherAssessmentModel.findOne({
                teacher: teacher._id,
                assessment_date: {
                    $gte: startOfMonth, // Greater than or equal to the start of the month
                    $lt: endOfMonth, // Less than the start of the next month
                },
            });
            if (existingTeacherAssessment) {
                throw new Error("ການປະເມີນອາຈານຜູ້ນີ້ໃນເດືອນນີ້ແມ່ນມີແລ້ວ");
            }
            return teacherAssessmentModel.create({
                teacher: teacher._id,
                attendance_class: teacherAssessmentData.attendance_class,
                behavior: teacherAssessmentData.behavior,
                assess_teaching: teacherAssessmentData.assess_teaching,
                assessment_date: teacherAssessmentData.assessment_date,
                note: teacherAssessmentData.note,
            });
        }
        catch (error) {
            throw new Error("ເກີດຂໍ້ຜິດພາດໃນການປະເມີນອາຈານ: " + error.message);
        }
    }),
    getTeacherAssessment: (teacher_id, assessment_date, limit, offset, teacherAssessmentModel, teacherModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let condition = {};
            if (teacher_id) {
                const teacher = yield teacherModel.findOne({
                    teacher_id: teacher_id,
                });
                condition["teacher"] = teacher === null || teacher === void 0 ? void 0 : teacher._id;
            }
            if (assessment_date) {
                const convertAssessmentDate = new Date(assessment_date);
                const year = convertAssessmentDate.getFullYear();
                const month = convertAssessmentDate.getMonth() + 1;
                condition["$expr"] = {
                    $and: [
                        { $eq: [{ $year: "$assessment_date" }, year] }, // Match year
                        { $eq: [{ $month: "$assessment_date" }, month] }, // Match month
                    ],
                };
            }
            // Count total assessment matching the condition before applying limit and skip
            const totalAssessment = yield teacherAssessmentModel.countDocuments(condition);
            const teacher_assessments = yield teacherAssessmentModel
                .find(condition)
                .populate("teacher")
                .sort({ assessment_date: -1 }) // Sorting by assessment_date instead of name
                .limit(limit)
                .skip(offset)
                .exec();
            return {
                teacher_assessments,
                totalAssessment,
            };
        }
        catch (error) {
            throw new Error("Failed to get teacher assessment: " + error.message);
        }
    }),
    updateTeacherAssessment: (_id, reqBody, teacherAssessmentModel, teacherModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { teacher, attendance_class, behavior, assess_teaching, assessment_date, note, } = reqBody;
        const teacher_assessment = yield teacherAssessmentModel.findById(_id);
        if (teacher_assessment) {
            try {
                // Find the teacher using the provided teacher
                const findTeacher = yield teacherModel.findOne({ teacher_id: teacher });
                if (!findTeacher) {
                    throw new Error("ບໍ່ພົບຂໍ້ມູນອາຈານ");
                }
                if (teacher_assessment.assessment_date.toISOString() !==
                    new Date(assessment_date).toISOString()) {
                    const convertAssessment_data = new Date(assessment_date);
                    const startOfMonth = new Date(convertAssessment_data.getFullYear(), convertAssessment_data.getMonth(), 1);
                    const endOfMonth = new Date(convertAssessment_data.getFullYear(), convertAssessment_data.getMonth() + 1, 0, 23, 59, 59, 999);
                    const existingTeacherAssessment = yield teacherAssessmentModel.findOne({
                        teacher: findTeacher._id,
                        assessment_date: {
                            $gte: startOfMonth, // Greater than or equal to the start of the month
                            $lt: endOfMonth, // Less than the start of the next month
                        },
                    });
                    if (existingTeacherAssessment) {
                        throw new Error("ການປະເມີນອາຈານຜູ້ນີ້ໃນເດືອນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                teacher_assessment.teacher =
                    findTeacher._id || teacher_assessment.teacher;
                teacher_assessment.attendance_class =
                    attendance_class || teacher_assessment.attendance_class;
                teacher_assessment.behavior = behavior || teacher_assessment.behavior;
                teacher_assessment.assess_teaching =
                    assess_teaching || teacher_assessment.assess_teaching;
                teacher_assessment.assessment_date =
                    assessment_date || teacher_assessment.assessment_date;
                teacher_assessment.note = note || "";
                yield teacher_assessment.save();
            }
            catch (error) {
                throw new Error("ເກີດຂໍ້ຜິດພາກໃນການແກ້ໄຂຂໍ້ມູນການປະເມີນຜົນ: " +
                    error.message);
            }
        }
        else {
            throw new Error("ບໍ່ພົບຂໍ້ມູນການປະເມີນນີ້");
        }
    }),
    deleteTeacherAssessment: (teacherAssessmentModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const teacher_assessment = yield teacherAssessmentModel.deleteOne({
                _id: _id,
            });
            if (teacher_assessment.deletedCount === 0) {
                throw new Error("Teacher Assessment not found");
            }
        }
        catch (error) {
            throw new Error("Failed to delete Teacher Assessment: " + error.message);
        }
    }),
};
exports.default = teacherAssessmentRepository;
