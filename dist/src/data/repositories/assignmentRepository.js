"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assignmentRepository = {
    createAssignment: (studentModel, assignmentModel, class_room, class_level, title) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const students = yield studentModel.find({ class_level: class_level });
            if (students.length === 0) {
                throw new Error("There are no students in this class yet");
            }
            // Extract student IDs that already have subjects
            const studentIds = students.map((student) => student._id);
            // Create assignments for each student
            const assignments = studentIds.map((studentId) => __awaiter(void 0, void 0, void 0, function* () {
                return yield assignmentModel.create({
                    student: studentId,
                    class_room: class_room,
                    class_level: class_level,
                    title: title,
                });
            }));
            return assignments;
        }
        catch (error) {
            throw new Error("Failed to create assignment: " + error.message);
        }
    }),
};
exports.default = assignmentRepository;
