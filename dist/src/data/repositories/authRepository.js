"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { Jwt } from "jsonwebtoken";
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const authRepository = {
    generateAccessToken: (user) => {
        const token = jsonwebtoken_1.default.sign({ id: user._id, username: user.username }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: "1d",
        });
        return token;
    },
    generateRefreshToken: (user) => {
        const token = jsonwebtoken_1.default.sign({ id: user._id, username: user.username }, process.env.REFRESH_TOKEN_SECRET, {
            expiresIn: "7d",
        });
        return token;
    },
    verifyAccessToken: (token) => {
        try {
            const decoded = jsonwebtoken_1.default.verify(token, process.env.ACCESS_TOKEN_SECRET);
            return decoded;
        }
        catch (error) {
            return;
        }
    },
    verifyRefreshToken: (token) => {
        try {
            const decoded = jsonwebtoken_1.default.verify(token, process.env.REFRESH_TOKEN_SECRET);
            return decoded;
        }
        catch (error) {
            return;
        }
    },
};
exports.default = authRepository;
