"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const classLevelRepository = {
    createClassLevel: (classLevelModel, classLevelData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            // Check if class level id if is unique
            const existingClassLevelId = yield classLevelModel.findOne({
                level_id: classLevelData.level_id,
            });
            if (existingClassLevelId) {
                throw new Error("ໄອດີຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
            }
            // Check if class level name if is unique
            const existingClassLevelName = yield classLevelModel.findOne({
                level_name: classLevelData.level_name,
            });
            if (existingClassLevelName) {
                throw new Error("ຊື່ຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
            }
            return classLevelModel.create(classLevelData);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create Class Level: " + error.message);
        }
    }),
    getClassLevel: (classLevelModel, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const totalCount = yield classLevelModel.countDocuments();
            const classLevels = yield classLevelModel
                .find()
                .sort({ level_name: 1 })
                .limit(Number(limit))
                .skip(Number(offset))
                .exec();
            return { classLevels, totalCount };
        }
        catch (error) {
            throw new Error("Failed to get class level: " + error.message);
        }
    }),
    updateClassLevel: (classLevelModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { level_id, level_name } = reqBody;
        const classLevel = yield classLevelModel.findOne({ _id: _id });
        if (classLevel) {
            try {
                if (classLevel.level_id !== level_id) {
                    const existingClassLevelId = yield classLevelModel.findOne({
                        level_id: level_id,
                    });
                    if (existingClassLevelId) {
                        throw new Error("ໄອດີຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                if (classLevel.level_name !== level_name) {
                    // Check if class level name if is unique
                    const existingClassLevelName = yield classLevelModel.findOne({
                        level_name: level_name,
                    });
                    if (existingClassLevelName) {
                        throw new Error("ຊື່ຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
                    }
                }
                classLevel.level_id = level_id || classLevel.level_id;
                classLevel.level_name = level_name || classLevel.level_name;
                yield classLevel.save();
            }
            catch (error) {
                throw new Error("Failed to update class level: " + error.message);
            }
        }
        else {
            throw new Error("no class level information found");
        }
    }),
    deleteClassLevel: (classLevelModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const classLevel = yield classLevelModel.deleteOne({ _id: _id });
            if (classLevel.deletedCount === 0) {
                throw new Error("Class level not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete class level: " + error.message);
        }
    }),
};
exports.default = classLevelRepository;
