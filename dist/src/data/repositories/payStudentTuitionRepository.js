"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const payStudentTuitionRepository = {
    // getPayStudentTuition: async (
    //   class_room: any,
    //   class_level: any,
    //   generation: any,
    //   limit: any,
    //   offset: any,
    //   studentModel: Model<StudentDocument>,
    //   payStudentTuitionModel: Model<PayStudentTuitionDocument>
    // ) => {
    //   try {
    //     const students = await studentModel.find({ class_level: class_level });
    //     // extract student IDs from the result
    //     const studentIds = students.map((student) => student._id);
    //     // Find pay student tuition for students matching the extracted IDs
    //     const existingPayStudentTuition = await payStudentTuitionModel
    //       .find({ student: { $in: studentIds } })
    //       .lean();
    //     // Extract student IDs that already have pay student tuition
    //     const studentIdsWithPayStudentTuition = existingPayStudentTuition.map(
    //       (pay_student_tuition) => pay_student_tuition.student.toString()
    //     );
    //     // Find student Ids without any pay student tuition
    //     const studentIdsWithoutPayStudentTuition = studentIds.filter(
    //       (id) => !studentIdsWithPayStudentTuition.includes(id.toString())
    //     );
    //     // Create new pay student tuition for students without existing entries
    //     if (studentIdsWithoutPayStudentTuition.length > 0) {
    //       const newPayStudentTuitionPromises =
    //         studentIdsWithoutPayStudentTuition.map((studentId) =>
    //           payStudentTuitionModel.create({
    //             student: studentId,
    //             class_room: class_room,
    //             class_level: class_level,
    //           })
    //         );
    //       await Promise.all(newPayStudentTuitionPromises);
    //     }
    //     // Count total matching students before pagination
    //     const totalStudents = await studentModel.countDocuments({
    //       class_level: class_level,
    //       generation: generation,
    //     });
    //     const payStudentTuition = await studentModel
    //       .aggregate([
    //         { $match: { class_level: class_level, generation: generation } },
    //         {
    //           $lookup: {
    //             from: "paystudenttuitions",
    //             let: { studentId: "$_id" },
    //             pipeline: [
    //               {
    //                 $match: {
    //                   $expr: { $eq: ["$student", "$$studentId"] },
    //                 },
    //               },
    //             ],
    //             as: "paystudenttuitions",
    //           },
    //         },
    //       ])
    //       .sort({ student_number: 1 })
    //       .limit(Number(limit))
    //       .skip(Number(offset));
    //     return {payStudentTuition,totalStudents};
    //   } catch (error: any) {
    //     throw new Error(
    //       "Failed to get pay student tuition: " + (error as Error).message
    //     );
    //   }
    // },
    getPayStudentTuition: (class_room, class_level, generation, term_1, term_2, limit, offset, studentModel, payStudentTuitionModel) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        try {
            // Convert string query parameters to booleans
            const term1Bool = term_1 === "true";
            const term2Bool = term_2 === "true";
            const students = yield studentModel.find({ class_level: class_level });
            // Extract student IDs from the result
            const studentIds = students.map((student) => student._id);
            // Find pay student tuition for students matching the extracted IDs
            const existingPayStudentTuition = yield payStudentTuitionModel
                .find({ student: { $in: studentIds } })
                .lean();
            // Extract student IDs that already have pay student tuition
            const studentIdsWithPayStudentTuition = existingPayStudentTuition.map((pay_student_tuition) => pay_student_tuition.student.toString());
            // Find student Ids without any pay student tuition
            const studentIdsWithoutPayStudentTuition = studentIds.filter((id) => !studentIdsWithPayStudentTuition.includes(id.toString()));
            // Create new pay student tuition for students without existing entries
            if (studentIdsWithoutPayStudentTuition.length > 0) {
                const newPayStudentTuitionPromises = studentIdsWithoutPayStudentTuition.map((studentId) => payStudentTuitionModel.create({
                    student: studentId,
                    class_room: class_room,
                    class_level: class_level,
                }));
                yield Promise.all(newPayStudentTuitionPromises);
            }
            // Build the aggregate pipeline
            const aggregatePipeline = [
                { $match: { class_level: class_level, generation: generation } },
                {
                    $lookup: {
                        from: "paystudenttuitions",
                        let: { studentId: "$_id" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: ["$student", "$$studentId"] },
                                },
                            },
                        ],
                        as: "paystudenttuitions",
                    },
                },
                {
                    $unwind: {
                        path: "$paystudenttuitions",
                        preserveNullAndEmptyArrays: true,
                    },
                },
            ];
            // Add filtering for term_1 and term_2 in the aggregation pipeline if provided
            const matchConditions = {};
            if (term_1 !== undefined) {
                matchConditions["paystudenttuitions.term_1"] = term1Bool;
            }
            if (term_2 !== undefined) {
                matchConditions["paystudenttuitions.term_2"] = term2Bool;
            }
            if (Object.keys(matchConditions).length > 0) {
                aggregatePipeline.push({ $match: matchConditions });
            }
            // Apply the same match conditions to the total count query
            const totalCountPipeline = [
                { $match: { class_level: class_level, generation: generation } },
                {
                    $lookup: {
                        from: "paystudenttuitions",
                        let: { studentId: "$_id" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: ["$student", "$$studentId"] },
                                },
                            },
                        ],
                        as: "paystudenttuitions",
                    },
                },
                {
                    $unwind: {
                        path: "$paystudenttuitions",
                        preserveNullAndEmptyArrays: true,
                    },
                },
            ];
            if (Object.keys(matchConditions).length > 0) {
                totalCountPipeline.push({ $match: matchConditions });
            }
            const totalStudentsResult = yield studentModel.aggregate([
                ...totalCountPipeline,
                { $count: "totalStudents" },
            ]);
            const totalStudents = ((_a = totalStudentsResult[0]) === null || _a === void 0 ? void 0 : _a.totalStudents) || 0;
            const payStudentTuition = yield studentModel
                .aggregate(aggregatePipeline)
                .sort({ student_number: 1 })
                .limit(Number(limit))
                .skip(Number(offset));
            return { payStudentTuition, totalStudents };
        }
        catch (error) {
            throw new Error("Failed to get pay student tuition: " + error.message);
        }
    }),
    updatePayStudentTuition: (payStudentTuitionModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { term_1, term_2 } = reqBody;
        const payStudentTuition = yield payStudentTuitionModel.findById(_id);
        if (payStudentTuition) {
            try {
                payStudentTuition.term_1 = term_1;
                payStudentTuition.term_2 = term_2;
                yield payStudentTuition.save();
            }
            catch (error) {
                throw new Error("Internal server error" + error.message);
            }
        }
        else {
            throw new Error("no pay student tuition information found");
        }
    }),
};
exports.default = payStudentTuitionRepository;
