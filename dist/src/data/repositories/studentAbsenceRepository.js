"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ClassRoomModels_1 = __importDefault(require("../../domain/models/ClassRoomModels"));
const studentAbsenceRepository = {
    createStudentAbsence: (studentAbsenceModel, studentModel, studentAbsenceData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const student = yield studentModel.findById(studentAbsenceData.student);
            if (!student) {
                throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
            }
            const absenceDate = new Date(studentAbsenceData.absence_date);
            const startOfDay = new Date(absenceDate);
            startOfDay.setHours(0, 0, 0, 0);
            const endOfDay = new Date(absenceDate);
            endOfDay.setHours(23, 59, 59, 999);
            const existingStudentAbsence = yield studentAbsenceModel.findOne({
                student: studentAbsenceData.student,
                absence_date: {
                    $gte: startOfDay, // Greater than or equal to the start of the day
                    $lt: endOfDay, // Less than the end of the day
                },
            });
            if (existingStudentAbsence) {
                // throw new Error("This student Absence already exist");
                throw new Error("ການຂາດຮຽນໃນວັນທີ່ນີ້ຂອງນັກຮຽນຜູ້ນີ້ມີໃນລະບົບແລ້ວ");
            }
            return studentAbsenceModel.create(Object.assign(Object.assign({}, studentAbsenceData), { generation: student.generation }));
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create student absence: " + error.message);
        }
    }),
    // getStudentAbsence: async (
    //   class_level: any,
    //   absence_date: any,
    //   student_id: any,
    //   limit: any,
    //   offset: any,
    //   studentAbsenceModel: Model<StudentAbsenceDocument>
    // ) => {
    //   try {
    //     let query = studentAbsenceModel
    //       .find({ student: student_id, class_level: class_level })
    //       .sort({ name: -1 })
    //       .limit(limit)
    //       .skip(offset);
    //     if (absence_date) {
    //       const conditions = [];
    //       const absenceDate = new Date(absence_date);
    //       const year = absenceDate.getFullYear();
    //       const month = absenceDate.getMonth() + 1;
    //       conditions.push({
    //         $expr: {
    //           $and: [
    //             { $eq: [{ $year: "$absence_date" }, year] }, // Match year
    //             { $eq: [{ $month: "$absence_date" }, month] }, // Match month
    //           ],
    //         },
    //       });
    //       query = query.where({ $and: conditions });
    //     }
    //     const results = await query.exec();
    //     return results;
    //   } catch (error: any) {
    //     throw new Error(
    //       "Failed to get student absence: " + (error as Error).message
    //     );
    //   }
    // },
    getAllStudentAbsence: (room_id, generation, student_id, limit, offset, studentModel, studentAbsenceModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const totalStudents = yield studentModel.countDocuments({
                room_id: room_id,
                generation: generation,
            });
            // Construct the $match stage dynamically
            const matchStage = {
                room_id: new mongoose_1.default.Types.ObjectId(room_id),
                generation: generation,
            };
            if (student_id) {
                matchStage.student_id = student_id;
            }
            const studentAbsence = yield studentModel
                .aggregate([
                { $match: matchStage },
                {
                    $lookup: {
                        from: "studentabsences",
                        let: { studentId: "$_id" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: ["$student", "$$studentId"] },
                                },
                            },
                            {
                                $match: {
                                    $expr: {
                                        $eq: ["$room_id", new mongoose_1.default.Types.ObjectId(room_id)],
                                    },
                                },
                            },
                            {
                                $match: {
                                    $expr: { $eq: ["$generation", generation] },
                                },
                            },
                            {
                                $group: {
                                    _id: "$student",
                                    student_absence_term1: {
                                        $sum: {
                                            $cond: [{ $eq: ["$term", "1"] }, 1, 0],
                                        },
                                    },
                                    student_absence_term2: {
                                        $sum: {
                                            $cond: [{ $eq: ["$term", "2"] }, 1, 0],
                                        },
                                    },
                                    student_sum_all_term1_and_term2: {
                                        $sum: 1,
                                    },
                                },
                            },
                        ],
                        as: "studentAbsence",
                    },
                },
                {
                    $lookup: {
                        from: "classrooms",
                        localField: "room_id",
                        foreignField: "_id",
                        as: "roomDetails",
                    },
                },
                { $unwind: "$roomDetails" },
                {
                    $lookup: {
                        from: "classlevels",
                        localField: "roomDetails.level_id",
                        foreignField: "_id",
                        as: "level_id",
                    },
                },
                { $unwind: "$level_id" },
            ])
                .sort({ student_number: 1 })
                .limit(Number(limit))
                .skip(Number(offset));
            // Format the response to match the required structure
            const formattedResponse = {
                studentAbsence: {
                    studentAbsence: studentAbsence.map((student) => ({
                        _id: student._id,
                        student_id: student.student_id,
                        name: student.name,
                        last_name: student.last_name,
                        age: student.age,
                        student_number: student.student_number,
                        date_of_birth: student.date_of_birth,
                        room_id: student.roomDetails, // Include room details
                        level_id: student.level_id,
                        status: student.status,
                        school_entry_date: student.school_entry_date,
                        gender: student.gender,
                        generation: student.generation,
                        note: student.note,
                        studentAbsence: student.studentAbsence.map((absence) => ({
                            _id: absence._id,
                            student: absence.student,
                            room_id: absence.room_id,
                            student_absence_term1: absence.student_absence_term1,
                            student_absence_term2: absence.student_absence_term2,
                            student_sum_all_term1_and_term2: absence.student_sum_all_term1_and_term2,
                        })),
                    })),
                    totalStudentsAbsent: totalStudents,
                },
            };
            return formattedResponse;
        }
        catch (error) {
            throw new Error("Failed to get student absence: " + error.message);
        }
    }),
    getStudentAbsence: (room_id, absence_date, student_id, term, be_reasonable, limit, offset, studentAbsenceModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filter = {
                student: student_id,
                room_id: room_id,
            };
            if (absence_date) {
                const absenceDate = new Date(absence_date);
                const year = absenceDate.getFullYear();
                const month = absenceDate.getMonth() + 1;
                filter["$expr"] = {
                    $and: [
                        { $eq: [{ $year: "$absence_date" }, year] }, // Match year
                        { $eq: [{ $month: "$absence_date" }, month] }, // Match month
                    ],
                };
            }
            if (term) {
                filter["term"] = term;
            }
            if (be_reasonable) {
                filter["be_reasonable"] = be_reasonable;
            }
            // Count total absences matching the filter before applying limit and skip
            const totalAbsences = yield studentAbsenceModel.countDocuments(filter);
            // Create base filters for be_reasonable counts
            let beReasonableFilter = Object.assign(Object.assign({}, filter), { be_reasonable: "ມີເຫດຜົນ" });
            let notBeReasonableFilter = Object.assign(Object.assign({}, filter), { be_reasonable: "ບໍ່ມີເຫດຜົນ" });
            // Remove term from filters if not provided
            if (!term) {
                delete beReasonableFilter.term;
                delete notBeReasonableFilter.term;
            }
            // Count total absences where be_reasonable is true
            const totalBe_reasonable = yield studentAbsenceModel.countDocuments(beReasonableFilter);
            // Count total absences where be_reasonable is false
            const totalNotBe_reasonable = yield studentAbsenceModel.countDocuments(notBeReasonableFilter);
            let query = studentAbsenceModel
                .find({ student: student_id, room_id: room_id })
                .sort({ name: -1 })
                .limit(limit)
                .skip(offset);
            const absences = yield studentAbsenceModel
                .find(filter)
                .populate("student")
                .populate({
                path: "room_id",
                model: ClassRoomModels_1.default,
                populate: [{ path: "level_id", model: "ClassLevel" }],
            })
                .sort({ absence_date: 1 }) // Sorting by absence_date instead of name
                .limit(limit)
                .skip(offset)
                .exec();
            // Return both absences and the total count
            return {
                absences,
                totalAbsences,
                totalBe_reasonable,
                totalNotBe_reasonable,
            };
        }
        catch (error) {
            throw new Error("Failed to get student absence: " + error.message);
        }
    }),
    updateStudentAbsence: (_id, reqBody, studentAbsenceModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { be_reasonable, absence_allday, absence_date, term, note } = reqBody;
        const student_absence_date = yield studentAbsenceModel.findById(_id);
        if (student_absence_date) {
            try {
                if (student_absence_date.absence_date.toISOString() !==
                    new Date(absence_date).toISOString()) {
                    const absenceDate = new Date(absence_date);
                    const startOfDay = new Date(absenceDate);
                    startOfDay.setHours(0, 0, 0, 0);
                    const endOfDay = new Date(absenceDate);
                    endOfDay.setHours(23, 59, 59, 999);
                    const existingTeacherAbsence = yield studentAbsenceModel.findOne({
                        student: student_absence_date.student, // Match teacher
                        absence_date: {
                            $gte: startOfDay, // Greater than or equal to the start of the day
                            $lt: endOfDay, // Less than the end of the day
                        },
                    });
                    if (existingTeacherAbsence) {
                        // throw new Error("This student Absence already exist");
                        throw new Error("ການຂາດຮຽນໃນວັນທີ່ນີ້ຂອງນັກຮຽນຜູ້ນີ້ມີໃນລະບົບແລ້ວ");
                    }
                }
                student_absence_date.be_reasonable =
                    be_reasonable || student_absence_date.be_reasonable;
                student_absence_date.absence_allday =
                    absence_allday || student_absence_date.absence_allday;
                student_absence_date.absence_date =
                    absence_date || student_absence_date.absence_date;
                student_absence_date.term = term || student_absence_date.term;
                student_absence_date.note = note || "";
                yield student_absence_date.save();
            }
            catch (error) {
                throw new Error("Failed to update student absence: " + error.message);
            }
        }
        else {
            throw new Error("no student absence information found");
        }
    }),
    deleteStudentAbsence: (studentAbsenceModel, id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const student_absence_date = yield studentAbsenceModel.deleteOne({
                _id: id,
            });
            if (student_absence_date.deletedCount === 0) {
                throw new Error("Student absence not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete student absence: " + error.message);
        }
    }),
};
exports.default = studentAbsenceRepository;
