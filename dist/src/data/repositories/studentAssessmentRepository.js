"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const studentAssessmentRepository = {
    createStudentAssessment: (studentAssessmentModel, studentAssessmentData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            // Check if student_id and classroom already exist
            const existingStudentAssessment = yield studentAssessmentModel.find({
                student: studentAssessmentData.student,
                room_id: studentAssessmentData.room_id,
                level_id: studentAssessmentData.level_id,
            });
            if (existingStudentAssessment.length > 0) {
                throw new Error("ຂໍ້ມູນນັກຮຽນນີ້ໃນຫ້ອງນີ້ມີການປະເມີນແລ້ວ");
            }
            return studentAssessmentModel.create(studentAssessmentData);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create studentAssessment: " + error.message);
        }
    }),
    getStudentAssessment: (level_id, room_id, generation, limit, offset, studentModel, studentAssessmentModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const students = yield studentModel.find({
                level_id: level_id,
                room_id: room_id,
                generation: generation,
            });
            // extract student IDs from the result
            const studentIds = students.map((student) => student._id);
            // find student assessments for students matching the extracted Ids
            const existingStudentAssessment = yield studentAssessmentModel
                .find({
                student: { $in: studentIds },
                level_id: level_id,
                room_id: room_id,
                generation: generation,
            })
                .lean();
            // Extract student Ids that already have student assessments
            const studentIdsWithStudentAssessments = existingStudentAssessment.map((studentAssessment) => studentAssessment.student.toString());
            // Find Student Ids without any Student Assessments
            const studentIdsWithoutStudentAssessments = studentIds.filter((id) => !studentIdsWithStudentAssessments.includes(id.toString()));
            // Create new student Assessment for students without existing entries
            if (studentIdsWithoutStudentAssessments.length > 0) {
                const newStudentsPromises = studentIdsWithoutStudentAssessments.map((studentId) => studentAssessmentModel.create({
                    student: studentId,
                    level_id: level_id,
                    room_id: room_id,
                    generation: generation,
                }));
                yield Promise.all(newStudentsPromises);
            }
            const totalStudents = yield studentAssessmentModel.countDocuments({
                room_id: room_id,
                level_id: level_id,
                generation: generation,
            });
            // const studentAssessments = await studentModel
            //   .aggregate([
            //     {
            //       $match: {
            //         level_id: new mongoose.Types.ObjectId(level_id),
            //         room_id: new mongoose.Types.ObjectId(room_id),
            //         generation: generation,
            //       },
            //     },
            //     {
            //       $lookup: {
            //         from: "studentassessments",
            //         let: { studentId: "$_id" },
            //         pipeline: [
            //           {
            //             $match: {
            //               $expr: { $eq: ["$student", "$$studentId"] },
            //             },
            //           },
            //         ],
            //         localField: "student",
            //         foreignField: "_id",
            //         as: "studentassessments",
            //       },
            //     },
            //     {
            //       $lookup: {
            //         from: "classrooms",
            //         localField: "room_id",
            //         foreignField: "_id",
            //         as: "roomDetails",
            //       },
            //     },
            //     {
            //       $lookup: {
            //         from: "classlevels",
            //         localField: "roomDetails.level_id",
            //         foreignField: "_id",
            //         as: "level_id",
            //       },
            //     },
            //   ])
            //   .sort({ student_number: 1 })
            //   .limit(Number(limit))
            //   .skip(Number(offset));
            const studentAssessments = yield studentAssessmentModel
                .aggregate([
                {
                    $match: {
                        level_id: new mongoose_1.default.Types.ObjectId(level_id),
                        room_id: new mongoose_1.default.Types.ObjectId(room_id),
                        generation: generation,
                    },
                },
                {
                    $lookup: {
                        from: "students",
                        localField: "student",
                        foreignField: "_id",
                        as: "student",
                    },
                },
                {
                    $unwind: "$student",
                },
                {
                    $lookup: {
                        from: "classrooms",
                        localField: "room_id",
                        foreignField: "_id",
                        as: "roomDetails",
                    },
                },
                {
                    $unwind: "$roomDetails",
                },
                {
                    $lookup: {
                        from: "classlevels",
                        localField: "level_id",
                        foreignField: "_id",
                        as: "levelDetails",
                    },
                },
                {
                    $unwind: "$levelDetails",
                },
                {
                    $project: {
                        _id: 1,
                        student: 1,
                        roomDetails: 1,
                        levelDetails: 1,
                        attendance_class: 1,
                        behavior: 1,
                        assess_learning: 1,
                        generation: 1,
                        note: 1,
                    },
                },
            ])
                .sort({ "student.student_number": 1 })
                .limit(Number(limit))
                .skip(Number(offset));
            return {
                studentAssessments,
                totalStudents,
            };
        }
        catch (error) {
            throw new Error("Failed to get subject: " + error.message);
        }
    }),
    updateStudentAssessment: (studentAssessmentModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { attendance_class, behavior, assess_learning, note } = reqBody;
        const studentAssessment = yield studentAssessmentModel.findById(_id);
        if (studentAssessment) {
            try {
                studentAssessment.attendance_class =
                    attendance_class || studentAssessment.attendance_class;
                studentAssessment.behavior = behavior || studentAssessment.behavior;
                studentAssessment.assess_learning =
                    assess_learning || studentAssessment.assess_learning;
                studentAssessment.note = note || studentAssessment.note;
                // Save the updated studentAssessment
                yield studentAssessment.save();
            }
            catch (error) {
                throw new Error("Internal server error" + error.message);
            }
        }
        else {
            throw new Error("no studentAssessment information found");
        }
    }),
    deleteStudentAssessment: (studentAssessmentModel, _id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const student_assessment = yield studentAssessmentModel.deleteOne({
                _id: _id,
            });
            if (student_assessment.deletedCount === 0) {
                throw new Error("Student Assessment not found");
            }
        }
        catch (error) {
            throw new Error("Failed to delete Student Assessment: " + error.message);
        }
    }),
};
exports.default = studentAssessmentRepository;
