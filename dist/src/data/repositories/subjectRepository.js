"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// db.employees.aggregate([{$lookup:{from:"departments",localField:"department_id",foreignField:"id",as:"ข้อมูลแผนก"}}])
const subjectRepository = {
    getSubject: (class_room, class_level, term, generation, limit, offset, studentModel, subjectModels) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (term > 2) {
                throw new Error("term should't more than 2");
            }
            const students = yield studentModel.find({ class_level: class_level });
            // extract student IDs from the result
            const studentIds = students.map((student) => student._id);
            // Find subjects for students matching the extracted IDs
            const existingSubjects = yield subjectModels
                .find({ student: { $in: studentIds }, term: term })
                .lean();
            // Extract student IDs that already have subjects
            const studentIdsWithSubjects = existingSubjects.map((subject) => subject.student.toString());
            // Find student Ids without any subjects
            const studentIdsWithoutSubjects = studentIds.filter((id) => !studentIdsWithSubjects.includes(id.toString()));
            // Create new subjects for students without existing entries
            if (studentIdsWithoutSubjects.length > 0) {
                const newSubjectsPromises = studentIdsWithoutSubjects.map((studentId) => subjectModels.create({
                    student: studentId,
                    class_room: class_room,
                    class_level: class_level,
                    term: term,
                }));
                yield Promise.all(newSubjectsPromises);
            }
            // Count total matching students before pagination
            const totalStudents = yield studentModel.countDocuments({
                class_level: class_level,
                generation: generation,
            });
            const subjects = yield studentModel
                .aggregate([
                // Match students with the specified class level
                { $match: { class_level: class_level, generation: generation } },
                // lookup subjects for the matched students
                //   {
                //     $lookup: {
                //       from: "subjects",
                //       localField: "_id",
                //       foreignField: "student",
                //       as: "subjects",
                //     },
                //   }
                {
                    $lookup: {
                        from: "subjects",
                        let: { studentId: "$_id" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: ["$student", "$$studentId"] },
                                    term: term, // Filter subjects by the term from the request query
                                },
                            },
                        ],
                        as: "subjects",
                    },
                },
            ])
                .sort({ student_number: 1 })
                .limit(Number(limit))
                .skip(Number(offset));
            // Calculate total score for each subject
            // subjects.forEach((student) => {
            //     student.subjects.forEach((subject:SubjectDocument) => {
            //       subject.total_score =
            //       Number(subject.laos) +
            //       Number(subject.english) +
            //       Number(subject.geography) +
            //       Number(subject.social) +
            //       Number(subject.physical_education) +
            //       Number(subject.history) +
            //       Number(subject.math) +
            //       Number(subject.chemical) +
            //       Number(subject.physics) +
            //       Number(subject.biology);
            //     });
            //   });
            // Return all subjects
            return { subjects, totalStudents };
        }
        catch (error) {
            throw new Error("Failed to get subject: " + error.message);
        }
    }),
    // updateSubject: async (
    //   subjectModel: Model<SubjectDocument>,
    //   _id: string,
    //   reqBody: Subject
    // ) => {
    //   const {
    //     laos,
    //     english,
    //     geography,
    //     social,
    //     physical_education,
    //     history,
    //     math,
    //     chemical,
    //     physics,
    //     biology,
    //   } = reqBody;
    //   const subject = await subjectModel.findById(_id);
    //   if (subject) {
    //     try {
    //       if (
    //         laos > 10 ||
    //         english > 10 ||
    //         geography > 10 ||
    //         social > 10 ||
    //         physical_education > 10 ||
    //         history > 10 ||
    //         math > 10 ||
    //         chemical > 10 ||
    //         physics > 10 ||
    //         biology > 10
    //       ) {
    //         throw new Error("Grade must not exceed 10");
    //       }
    //       subject.laos = laos || subject.laos;
    //       subject.english = english || subject.english;
    //       subject.geography = geography || subject.geography;
    //       subject.social = social || subject.social;
    //       subject.physical_education =
    //         physical_education || subject.physical_education;
    //       subject.history = history || subject.history;
    //       subject.math = math || subject.math;
    //       subject.chemical = chemical || subject.chemical;
    //       subject.physics = physics || subject.physics;
    //       subject.biology = biology || subject.biology;
    //       // Calculate total score for each subject
    //       const sum_all_score =
    //         Number(laos || subject.laos) +
    //         Number(english || subject.english) +
    //         Number(geography || subject.geography) +
    //         Number(social || subject.social) +
    //         Number(physical_education || subject.physical_education) +
    //         Number(history || subject.history) +
    //         Number(math || subject.math) +
    //         Number(chemical || subject.chemical) +
    //         Number(physics || subject.physics) +
    //         Number(biology || subject.biology);
    //       subject.total_score = sum_all_score;
    //       subject.academic_results =
    //         sum_all_score >= 0 && sum_all_score < 40
    //           ? "Fail"
    //           : sum_all_score >= 40 && sum_all_score < 50
    //           ? "Below Average"
    //           : sum_all_score >= 50 && sum_all_score < 59
    //           ? "Average"
    //           : sum_all_score >= 60 && sum_all_score < 70
    //           ? "Good"
    //           : sum_all_score >= 70 && sum_all_score < 80
    //           ? "Very Good"
    //           : "Excellent";
    //       // Save the updated subject
    //       await subject.save();
    //     } catch (error: any) {
    //       throw new Error("Internal server error" + (error as Error).message);
    //     }
    //   } else {
    //     throw new Error("no subject information found");
    //   }
    // },
    updateSubject: (subjectModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { rank, laos, english, geography, social, physical_education, history, math, chemical, physics, biology, } = reqBody;
        const subject = yield subjectModel.findById(_id);
        if (subject) {
            try {
                // Update subject grades
                subject.rank = typeof rank !== "undefined" ? rank : subject.rank;
                subject.laos = typeof laos !== "undefined" ? laos : subject.laos;
                subject.english =
                    typeof english !== "undefined" ? english : subject.english;
                subject.geography =
                    typeof geography !== "undefined" ? geography : subject.geography;
                subject.social =
                    typeof social !== "undefined" ? social : subject.social;
                subject.physical_education =
                    typeof physical_education !== "undefined"
                        ? physical_education
                        : subject.physical_education;
                subject.history =
                    typeof history !== "undefined" ? history : subject.history;
                subject.math = typeof math !== "undefined" ? math : subject.math;
                subject.chemical =
                    typeof chemical !== "undefined" ? chemical : subject.chemical;
                subject.physics =
                    typeof physics !== "undefined" ? physics : subject.physics;
                subject.biology =
                    typeof biology !== "undefined" ? biology : subject.biology;
                // Calculate total score for the subject
                const totalScore = [
                    typeof laos !== "undefined" ? laos : subject.laos,
                    typeof english !== "undefined" ? english : subject.english,
                    typeof geography !== "undefined" ? geography : subject.geography,
                    typeof social !== "undefined" ? social : subject.social,
                    typeof physical_education !== "undefined"
                        ? physical_education
                        : subject.physical_education,
                    typeof history !== "undefined" ? history : subject.history,
                    typeof math !== "undefined" ? math : subject.math,
                    typeof chemical !== "undefined" ? chemical : subject.chemical,
                    typeof physics !== "undefined" ? physics : subject.physics,
                    typeof biology !== "undefined" ? biology : subject.biology,
                ].reduce((acc, curr) => acc + Number(curr), 0);
                subject.total_score = totalScore;
                subject.academic_results =
                    totalScore >= 0 && totalScore < 40
                        ? "ອ່ອນຫຼາຍ"
                        : totalScore >= 40 && totalScore < 50
                            ? "ອ່ອນ"
                            : totalScore >= 50 && totalScore < 59
                                ? "ພໍໃຊ້"
                                : totalScore >= 60 && totalScore < 70
                                    ? "ດີ"
                                    : totalScore >= 70 && totalScore < 80
                                        ? "ດີຫຼາຍ"
                                        : "ດີເດັ່ນ";
                // Save the updated subject
                yield subject.save();
                if (!rank) {
                    const findGenerationStudent = yield subjectModel
                        .findOne({ student: subject.student })
                        .populate("student");
                    // Find all subjects in the same class_level and generation
                    const matchingSubjects = yield subjectModel
                        .find({
                        class_level: subject.class_level,
                        // generation: findGenerationStudent?.student.generation
                    })
                        .populate("student");
                    // Extract the _id values from matchingSubjects
                    const studentIdsWithNull = matchingSubjects
                        .filter((subject) => !subject.student) // Filter subjects with null student
                        .map((subject) => subject._id); // Map to array of _id values
                    if (studentIdsWithNull.length > 0) {
                        // Delete documents where student is null based on extracted _id values
                        yield subjectModel.deleteMany({
                            class_level: subject.class_level,
                            _id: { $in: studentIdsWithNull }, // Delete documents with _id in the extracted array
                        });
                    }
                    const filter_same_generation_classLevel = matchingSubjects.filter((student) => student.student &&
                        student.student.generation ===
                            (findGenerationStudent === null || findGenerationStudent === void 0 ? void 0 : findGenerationStudent.student.generation) &&
                        student.student.level_id ===
                            (findGenerationStudent === null || findGenerationStudent === void 0 ? void 0 : findGenerationStudent.student.level_id));
                    // Sort the subjects based on their total_score in descending order
                    const sortedSubjects = filter_same_generation_classLevel.sort((a, b) => b.total_score - a.total_score);
                    // Update rank for each subject
                    let rank = 1;
                    let prevTotalScore = sortedSubjects[0].total_score; // Initialize with the highest total_score
                    for (const subj of sortedSubjects) {
                        if (subj.total_score < prevTotalScore) {
                            rank++;
                        }
                        subj.rank = rank;
                        yield subj.save();
                        prevTotalScore = subj.total_score;
                    }
                }
            }
            catch (error) {
                throw new Error("Internal server error" + error.message);
            }
        }
        else {
            throw new Error("No subject information found");
        }
    }),
    deleteSubject: (subjectModel, id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const subject = yield subjectModel.deleteOne({ _id: id });
            if (subject.deletedCount === 0) {
                throw new Error("Subject not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete subject: " + error.message);
        }
    }),
};
exports.default = subjectRepository;
