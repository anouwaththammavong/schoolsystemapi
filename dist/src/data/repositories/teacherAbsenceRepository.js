"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const teacherAbsenceRepository = {
    createTeacherAbsence: (teacherAbsenceModel, teacherAbsenceData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const absenceDate = new Date(teacherAbsenceData.absence_date);
            const startOfDay = new Date(absenceDate);
            startOfDay.setHours(0, 0, 0, 0);
            const endOfDay = new Date(absenceDate);
            endOfDay.setHours(23, 59, 59, 999);
            const existingTeacherAbsence = yield teacherAbsenceModel.findOne({
                teacher: teacherAbsenceData.teacher, // Match teacher
                absence_date: {
                    $gte: startOfDay, // Greater than or equal to the start of the day
                    $lt: endOfDay, // Less than the end of the day
                },
            });
            if (existingTeacherAbsence) {
                // throw new Error("This teacher Absence already exist");
                throw new Error("ການຂາດສອນໃນວັນທີ່ນີ້ຂອງອາຈານຜູ້ນີ້ມີໃນລະບົບແລ້ວ");
            }
            return teacherAbsenceModel.create(teacherAbsenceData);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create teacher absence: " + error.message);
        }
    }),
    // getTeacherAbsenceRepository: async (
    //   absence_date: any,
    //   teacher_id: any,
    //   limit: any,
    //   offset: any,
    //   teacherAbsenceModel: Model<TeacherAbsenceDocument>
    // ) => {
    //   try {
    //     let query = teacherAbsenceModel
    //       .find({ teacher: teacher_id })
    //       .sort({ name: -1 })
    //       .limit(limit)
    //       .skip(offset);
    //     if (absence_date) {
    //       const conditions = [];
    //       const absenceDate = new Date(absence_date);
    //       const year = absenceDate.getFullYear();
    //       const month = absenceDate.getMonth() + 1;
    //       conditions.push({
    //         $expr: {
    //           $and: [
    //             { $eq: [{ $year: "$absence_date" }, year] }, // Match year
    //             { $eq: [{ $month: "$absence_date" }, month] }, // Match month
    //           ],
    //         },
    //       });
    //       query = query.where({ $and: conditions });
    //     }
    //     const results = await query.exec();
    //     return results;
    //   } catch (error: any) {
    //     throw new Error(
    //       "Failed to get teacher absence: " + (error as Error).message
    //     );
    //   }
    // },
    getTeacherAbsenceRepository: (absence_date, teacher_id, be_reasonable, limit, offset, teacherAbsenceModel) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filter = {
                teacher: teacher_id,
            };
            if (absence_date) {
                const absenceDate = new Date(absence_date);
                const year = absenceDate.getFullYear();
                const month = absenceDate.getMonth() + 1;
                // Apply filter for the year and month
                filter["$expr"] = {
                    $and: [
                        { $eq: [{ $year: "$absence_date" }, year] }, // Match year
                        { $eq: [{ $month: "$absence_date" }, month] }, // Match month
                    ],
                };
            }
            if (be_reasonable) {
                filter["be_reasonable"] = be_reasonable;
            }
            // Count total absences matching the filter before applying limit and skip
            const totalAbsences = yield teacherAbsenceModel.countDocuments(filter);
            // Create base filters for be_reasonable counts
            let beReasonableFilter = Object.assign(Object.assign({}, filter), { be_reasonable: "ມີເຫດຜົນ" });
            let notBeReasonableFilter = Object.assign(Object.assign({}, filter), { be_reasonable: "ບໍ່ມີເຫດຜົນ" });
            // Count total absences where be_reasonable is true
            const totalBe_reasonable = yield teacherAbsenceModel.countDocuments(beReasonableFilter);
            // Count total absences where be_reasonable is false
            const totalNotBe_reasonable = yield teacherAbsenceModel.countDocuments(notBeReasonableFilter);
            // Apply limit and skip for pagination
            const absences = yield teacherAbsenceModel
                .find(filter)
                .populate("teacher")
                .sort({ absence_date: -1 }) // Sorting by absence_date instead of name
                .limit(limit)
                .skip(offset)
                .exec();
            // Return both absences and the total count
            return {
                absences,
                totalAbsences,
                totalBe_reasonable,
                totalNotBe_reasonable,
            };
        }
        catch (error) {
            throw new Error("Failed to get teacher absence: " + error.message);
        }
    }),
    updateTeacherAbsenceRepository: (_id, reqBody, teacherAbsenceModel) => __awaiter(void 0, void 0, void 0, function* () {
        const { be_reasonable, absence_allday, absence_date, note } = reqBody;
        const teacher_absence_date = yield teacherAbsenceModel.findById(_id);
        if (teacher_absence_date) {
            try {
                if (teacher_absence_date.absence_date.toISOString() !==
                    new Date(absence_date).toISOString()) {
                    const absenceDate = new Date(absence_date);
                    const startOfDay = new Date(absenceDate);
                    startOfDay.setHours(0, 0, 0, 0);
                    const endOfDay = new Date(absenceDate);
                    endOfDay.setHours(23, 59, 59, 999);
                    const existingTeacherAbsence = yield teacherAbsenceModel.findOne({
                        teacher: teacher_absence_date.teacher, // Match teacher
                        absence_date: {
                            $gte: startOfDay, // Greater than or equal to the start of the day
                            $lt: endOfDay, // Less than the end of the day
                        },
                    });
                    if (existingTeacherAbsence) {
                        // throw new Error("This teacher Absence already exist");
                        throw new Error("ການຂາດສອນໃນວັນທີ່ນີ້ຂອງອາຈານຜູ້ນີ້ມີໃນລະບົບແລ້ວ");
                    }
                }
                teacher_absence_date.be_reasonable =
                    be_reasonable || teacher_absence_date.be_reasonable;
                teacher_absence_date.absence_allday =
                    absence_allday || teacher_absence_date.absence_allday;
                teacher_absence_date.absence_date =
                    absence_date || teacher_absence_date.absence_date;
                teacher_absence_date.note = note || "";
                yield teacher_absence_date.save();
            }
            catch (error) {
                throw new Error("Failed to update teacher absence: " + error.message);
            }
        }
        else {
            throw new Error("no teacher absence information found");
        }
    }),
    deleteTeacherAbsence: (teacherAbsenceModel, id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const teacher_absence = yield teacherAbsenceModel.deleteOne({ _id: id });
            if (teacher_absence.deletedCount === 0) {
                throw new Error("Teacher absence not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete teacher absence: " + error.message);
        }
    }),
};
exports.default = teacherAbsenceRepository;
