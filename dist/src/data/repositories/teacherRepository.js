"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const teacherRepository = {
    createTeacher: (teacherModel, teacherData) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            // Check if teacher_id is unique
            const existingTeacherId = yield teacherModel.findOne({
                teacher_id: teacherData.teacher_id,
            });
            if (existingTeacherId) {
                // throw new Error("Teacher ID must be unique");
                throw new Error("ລະຫັດອາຈານນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ");
            }
            // Check if the class already exists.
            const existingClass = yield teacherModel.findOne({
                class_teacher: teacherData.class_teacher,
            });
            if (existingClass && Boolean(existingClass === null || existingClass === void 0 ? void 0 : existingClass.class_teacher)) {
                // throw new Error("Teacher in this class already exists");
                throw new Error("ຄູປະຈຳຫ້ອງນີ້ມີຢູ່ແລ້ວ");
            }
            return teacherModel.create(teacherData);
        }
        catch (error) {
            // Handle the error
            throw new Error("Failed to create teacher: " + error.message);
        }
    }),
    getTeachers: (teacherModel, teacher_id, limit, offset) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            let filter = {};
            if (teacher_id) {
                filter = { teacher_id: teacher_id };
            }
            // Count total teachers matching the filter before applying limit and skip
            const totalTeachers = yield teacherModel.countDocuments(filter);
            // Apply limit and skip for pagination
            // const teachers = await teacherModel
            //   .find(filter)
            //   .populate("subjects.title")
            //   .populate("class_teacher")
            //   .sort({ name: -1 })
            //   .limit(limit)
            //   .skip(offset)
            //   .exec();
            // Apply limit and skip for pagination
            const teachers = yield teacherModel
                .find(filter)
                .populate({
                path: "subjects.title"
            })
                .populate({
                path: "class_teacher",
                populate: {
                    path: "level_id",
                    model: "ClassLevel"
                }
            })
                .sort({ name: -1 })
                .limit(Number(limit))
                .skip(Number(offset))
                .exec();
            // Return both teachers and the total count
            return {
                teachers,
                totalTeachers,
            };
        }
        catch (error) {
            throw new Error("Failed to get teachers: " + error.message);
        }
    }),
    updateTeacher: (teacherModel, _id, reqBody) => __awaiter(void 0, void 0, void 0, function* () {
        const { teacher_id, name, last_name, age, teacher_tel, date_of_birth, teacher_village, teacher_district, teacher_province, nationality, graduated_from_institute, gender, degree, subjects, class_teacher, } = reqBody;
        const teacher = yield teacherModel.findById(_id);
        if (teacher) {
            try {
                if (teacher.teacher_id != teacher_id) {
                    const existingTeacherId = yield teacherModel.findOne({
                        teacher_id: teacher_id,
                    });
                    if (existingTeacherId) {
                        // throw new Error("Teacher ID must be unique");
                        throw new Error("ລະຫັດອາຈານນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ");
                    }
                }
                if (class_teacher) {
                    if (teacher.class_teacher != class_teacher) {
                        const existingClass = yield teacherModel.findOne({
                            class_teacher: class_teacher,
                        });
                        if (existingClass) {
                            // throw new Error("Teacher in this class already exists");
                            throw new Error("ຄູປະຈຳຫ້ອງນີ້ມີຢູ່ແລ້ວ");
                        }
                    }
                }
                teacher.teacher_id = teacher_id || teacher.teacher_id;
                teacher.name = name || teacher.name;
                teacher.last_name = last_name || teacher.last_name;
                teacher.age = age || teacher.age;
                teacher.teacher_tel = teacher_tel || teacher.teacher_tel;
                teacher.date_of_birth = date_of_birth || teacher.date_of_birth;
                teacher.teacher_village = teacher_village || teacher.teacher_village;
                teacher.teacher_district = teacher_district || teacher.teacher_district;
                teacher.teacher_province = teacher_province || teacher.teacher_province;
                teacher.nationality = nationality || teacher.nationality;
                teacher.graduated_from_institute =
                    graduated_from_institute || teacher.graduated_from_institute;
                teacher.gender = gender || teacher.gender;
                teacher.degree = degree || teacher.degree;
                teacher.subjects = subjects || teacher.subjects;
                if (typeof (class_teacher) !== undefined) {
                    teacher.class_teacher = class_teacher;
                }
                // Save the update teacher
                yield teacher.save();
            }
            catch (error) {
                throw new Error("Failed to update teacher: " + error.message);
            }
        }
        else {
            throw new Error("no teacher information found");
        }
    }),
    deleteTeacher: (teacherModels, id) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const teacher = yield teacherModels.deleteOne({ _id: id });
            if (teacher.deletedCount === 0) {
                throw new Error("Teacher not found");
            }
        }
        catch (error) {
            // Handle any errors that occur during the find or delete operations
            throw new Error("Failed to delete teacher: " + error.message);
        }
    }),
};
exports.default = teacherRepository;
