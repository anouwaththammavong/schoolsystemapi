"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const scoreSchema = new mongoose_1.Schema({
    student: { type: mongoose_1.Schema.Types.ObjectId, ref: "Students", required: true },
    room_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "ClassRoom",
        required: true,
    },
    level_id: { type: mongoose_1.Schema.Types.ObjectId, ref: "ClassLevel", required: true },
    generation: { type: String, required: true },
    month: { type: Date, required: true },
    term: { type: String },
    subjects: [
        {
            title: { type: mongoose_1.Schema.Types.ObjectId, ref: "SubjectV2", required: true },
            point: { type: Number, default: 0 },
            teacher: { type: mongoose_1.Schema.Types.ObjectId, ref: "Teachers" },
        },
    ],
    total_point: { type: Number },
    rank: { type: Number },
});
exports.default = mongoose_1.default.model("Scores", scoreSchema);
