"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const studentSchema = new mongoose_1.Schema({
    student_id: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    last_name: { type: String, required: true },
    age: { type: String, required: true },
    student_tel: { type: String, required: true },
    date_of_birth: { type: Date, required: true },
    student_village: { type: String, required: true },
    student_district: { type: String, required: true },
    student_province: { type: String, required: true },
    nationality: { type: String, required: true },
    father_name: { type: String, required: true },
    father_job: { type: String, required: true },
    mother_name: { type: String, required: true },
    mother_job: { type: String, required: true },
    father_tel: { type: String, required: true },
    mother_tel: { type: String, required: true },
    student_past_school: { type: String },
    student_number: { type: String, required: true },
    room_id: { type: mongoose_1.Schema.Types.ObjectId, ref: "ClassRoom", required: true },
    level_id: { type: mongoose_1.Schema.Types.ObjectId, ref: "ClassLevel", required: true },
    status: { type: String, required: true },
    note: { type: String },
    school_entry_date: { type: Date, required: true },
    gender: { type: String, required: true },
    generation: { type: String, required: true },
    new_student: { type: Boolean, required: true, default: true },
});
exports.default = mongoose_1.default.model("Students", studentSchema);
