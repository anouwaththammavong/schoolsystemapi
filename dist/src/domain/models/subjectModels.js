"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const subjectSchema = new mongoose_1.Schema({
    student: { type: mongoose_1.Schema.Types.ObjectId, ref: "Students", required: true },
    class_room: { type: String, required: true },
    class_level: { type: String, required: true },
    term: { type: String, required: true },
    laos: { type: Number, default: 0 },
    english: { type: Number, default: 0 },
    geography: { type: Number, default: 0 },
    social: { type: Number, default: 0 },
    physical_education: { type: Number, default: 0 },
    history: { type: Number, default: 0 },
    math: { type: Number, default: 0 },
    chemical: { type: Number, default: 0 },
    physics: { type: Number, default: 0 },
    biology: { type: Number, default: 0 },
    total_score: { type: Number, default: 0 },
    academic_results: { type: String, default: "ອ່ອນຫຼາຍ" },
    rank: { type: Number }
});
exports.default = mongoose_1.default.model("Subject", subjectSchema);
