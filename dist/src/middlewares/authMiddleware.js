"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const authRepository_1 = __importDefault(require("../data/repositories/authRepository"));
const authMiddleware = (req, res, next) => {
    // decode token
    const authHeader = req.headers.authorization;
    if (authHeader && authHeader.startsWith("Bearer ")) {
        const accessToken = authHeader.split(" ")[1];
        // verifies secret and check exp
        const decoded = authRepository_1.default.verifyAccessToken(accessToken);
        if (!decoded) {
            res.status(401).json({ message: "Unauthorized" });
        }
        else {
            // if everything is good, save to request for use in other routes
            req.decoded = decoded;
            next();
        }
    }
    else {
        // User is not authenticated, send unauthorized response
        res.status(401).json({ message: "Unauthorized" });
    }
};
exports.default = authMiddleware;
