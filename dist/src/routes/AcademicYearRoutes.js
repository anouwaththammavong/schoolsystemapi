"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const AcademicYearControllers_1 = require("../controllers/AcademicYearControllers");
const router = express_1.default.Router();
router.post("/createAcademicYear", AcademicYearControllers_1.createAcademicYear);
router.get("/getAcademicYear", AcademicYearControllers_1.getAcademicYear);
router.put("/updateAcademicYear/:id", AcademicYearControllers_1.updateAcademicYear);
router.delete("/deleteAcademicYear/:id", AcademicYearControllers_1.deleteAcademicYear);
exports.default = router;
