"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ClassLevelControllers_1 = require("../controllers/ClassLevelControllers");
const router = express_1.default.Router();
router.post("/createClassLevel", ClassLevelControllers_1.createClassLevel);
router.get("/getClassLevel", ClassLevelControllers_1.getClassLevel);
router.put("/updateClassLevel/:id", ClassLevelControllers_1.updateClassLevel);
router.delete("/deleteClassLevel/:id", ClassLevelControllers_1.deleteClassLevel);
exports.default = router;
