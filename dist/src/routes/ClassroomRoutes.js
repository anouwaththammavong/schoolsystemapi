"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ClassRoomControllers_1 = require("../controllers/ClassRoomControllers");
const router = express_1.default.Router();
router.post("/createClassroom", ClassRoomControllers_1.createClassRoom);
router.get("/getClassrooms", ClassRoomControllers_1.getClassRooms);
router.put("/updateClassroom/:id", ClassRoomControllers_1.updateClassroom);
router.delete("/deleteClassroom/:id", ClassRoomControllers_1.deleteClassroom);
exports.default = router;
