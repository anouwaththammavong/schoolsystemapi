"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const RegistrationControllers_1 = require("../controllers/RegistrationControllers");
const router = express_1.default.Router();
router.post("/createOldStudentRegistration", RegistrationControllers_1.createOldStudentRegistration);
router.post("/createNewStudentRegistration", RegistrationControllers_1.createNewStudentRegistration);
router.get("/getRegistrations", RegistrationControllers_1.getRegistrations);
router.put("/updateRegistration/:id", RegistrationControllers_1.updateRegistration);
router.put("/updateNewRegistration/:id", RegistrationControllers_1.updateNewRegistration);
router.delete("/deleteRegistration/:id", RegistrationControllers_1.deleteRegistration);
exports.default = router;
