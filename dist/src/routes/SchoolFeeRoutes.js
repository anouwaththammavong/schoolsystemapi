"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const SchoolFeeControllers_1 = require("../controllers/SchoolFeeControllers");
const router = express_1.default.Router();
router.post("/createSchoolFee", SchoolFeeControllers_1.createSchoolFee);
router.get("/getSchoolFees", SchoolFeeControllers_1.getSchoolFees);
router.put("/updateSchoolFee/:id", SchoolFeeControllers_1.updateSchoolFee);
router.delete("/deleteSchoolFee/:id", SchoolFeeControllers_1.deleteSchoolFee);
exports.default = router;
