"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ScoreControllers_1 = require("../controllers/ScoreControllers");
const router = express_1.default.Router();
router.post("/createScore", ScoreControllers_1.createScore);
router.get("/getScores", ScoreControllers_1.getScores);
router.put("/updateScore/:id", ScoreControllers_1.updateScore);
router.delete("/deleteScore/:id", ScoreControllers_1.deleteScore);
exports.default = router;
