"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const SubjectV2Controller_1 = require("../controllers/SubjectV2Controller");
const router = express_1.default.Router();
router.post("/createSubjectV2", SubjectV2Controller_1.createSubjectV2);
router.get("/getSubjectV2", SubjectV2Controller_1.getSubjectV2);
router.put("/updateSubjectV2/:id", SubjectV2Controller_1.updateSubjectV2);
router.delete("/deleteSubjectV2/:id", SubjectV2Controller_1.deleteSubjectV2);
exports.default = router;
