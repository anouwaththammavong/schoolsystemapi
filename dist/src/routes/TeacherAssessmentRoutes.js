"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const teacherAssessmentControllers_1 = require("../controllers/teacherAssessmentControllers");
const router = express_1.default.Router();
router.post("/createTeacherAssessment", teacherAssessmentControllers_1.createTeacherAssessment);
router.get("/getTeacherAssessment", teacherAssessmentControllers_1.getTeacherAssessment);
router.put("/updateTeacherAssessment/:id", teacherAssessmentControllers_1.updateTeacherAssessment);
router.delete("/deleteTeacherAssessment/:id", teacherAssessmentControllers_1.deleteTeacherAssessment);
exports.default = router;
