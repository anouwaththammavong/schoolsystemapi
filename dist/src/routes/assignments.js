"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const assignmentControllers_1 = require("../controllers/assignmentControllers");
const router = express_1.default.Router();
router.post("/createAssignment", assignmentControllers_1.createAssignment);
