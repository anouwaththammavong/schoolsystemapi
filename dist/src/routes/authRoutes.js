"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const authControllers_1 = require("../controllers/authControllers");
const router = express_1.default.Router();
router.post("/auth/register", authControllers_1.register);
router.post("/auth/login", authControllers_1.login);
router.put("/auth/updateUser/:id", authControllers_1.updateUser);
router.get("/auth/getUser", authControllers_1.getUser);
// Route for refreshing access token
router.post('/auth/refresh-token', authControllers_1.refreshToken);
router.post('/auth/logout', authControllers_1.logout);
exports.default = router;
