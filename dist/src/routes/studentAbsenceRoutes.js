"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const studentAbsenceControllers_1 = require("../controllers/studentAbsenceControllers");
const router = express_1.default.Router();
router.post("/createStudentAbsence", studentAbsenceControllers_1.createStudentAbsence);
router.get("/getStudentAbsence", studentAbsenceControllers_1.getStudentAbsence);
router.get("/getAllStudentAbsence", studentAbsenceControllers_1.getAllStudentAbsence);
router.put("/updateStudentAbsence/:id", studentAbsenceControllers_1.updateStudentAbsence);
router.delete("/deleteStudentAbsence/:id", studentAbsenceControllers_1.deleteStudentAbsence);
exports.default = router;
