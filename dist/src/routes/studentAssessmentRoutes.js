"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const studentAssessmentControllers_1 = require("../controllers/studentAssessmentControllers");
const router = express_1.default.Router();
router.post("/createStudentAssessment", studentAssessmentControllers_1.createStudentAssessment);
router.get("/getStudentAssessment", studentAssessmentControllers_1.getStudentAssessment);
router.put("/updateStudentAssessment/:id", studentAssessmentControllers_1.updateStudentAssessment);
router.delete("/deleteStudentAssessment/:id", studentAssessmentControllers_1.deleteStudentAssessment);
exports.default = router;
