"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const studentControllers_1 = require("../controllers/studentControllers");
const router = express_1.default.Router();
router.post("/createStudent", studentControllers_1.createStudent);
router.get("/getStudent", studentControllers_1.getStudent);
router.put("/updateStudent/:id", studentControllers_1.updateStudent);
router.delete("/deleteStudent/:id", studentControllers_1.deleteStudent);
exports.default = router;
