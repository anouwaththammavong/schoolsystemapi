"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const subjectControllers_1 = require("../controllers/subjectControllers");
const router = express_1.default.Router();
router.get("/getSubject", subjectControllers_1.getSubject);
router.put("/updateSubject/:id", subjectControllers_1.updateSubject);
router.delete("/deleteSubject/:id", subjectControllers_1.deleteSubject);
exports.default = router;
