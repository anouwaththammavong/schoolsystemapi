"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const teacherAbsenceControllers_1 = require("../controllers/teacherAbsenceControllers");
const router = express_1.default.Router();
router.post("/createTeacherAbsence", teacherAbsenceControllers_1.createTeacherAbsence);
router.get("/getTeacherAbsence", teacherAbsenceControllers_1.getTeacherAbsence);
router.put("/updateTeacherAbsence/:id", teacherAbsenceControllers_1.updateTeacherAbsence);
router.delete("/deleteTeacherAbsence/:id", teacherAbsenceControllers_1.deleteTeacherAbsence);
exports.default = router;
