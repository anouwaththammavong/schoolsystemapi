"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const teacherControllers_1 = require("../controllers/teacherControllers");
const router = express_1.default.Router();
router.post("/createTeacher", teacherControllers_1.createTeacher);
router.get("/getTeacher", teacherControllers_1.getTeachers);
router.put("/updateTeacher/:id", teacherControllers_1.updateTeacher);
router.delete("/deleteTeacher/:id", teacherControllers_1.deleteTeacher);
exports.default = router;
