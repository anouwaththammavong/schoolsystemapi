import express, { Express, Request, Response, NextFunction } from "express";
import dotenv from "dotenv";
import cors from "cors";
import connectDatabase from "../config/MongoDb";
import authRoutes from "./routes/authRoutes";
import ClassLevelRoutes from "./routes/ClassLevelRoutes";
import ClassroomRoutes from "./routes/ClassroomRoutes";
import AcademicYearRoutes from "./routes/AcademicYearRoutes";
import SchoolFeeRoutes from "./routes/SchoolFeeRoutes";
import subjectV2Routes from "./routes/SubjectV2Routes";
import registrationRoutes from "./routes/RegistrationRoutes";
import studentRoutes from "./routes/studentRoutes";
import subjectRoutes from "./routes/subjectRoutes";
import teacherRoutes from "./routes/teacherRoutes";
import assignmentRoutes from "./routes/assignmentRoutes";
import studentAssessmentRoutes from "./routes/studentAssessmentRoutes";
import TeacherAssessmentRoutes from "./routes/TeacherAssessmentRoutes";
import ScoreRoutes from "./routes/ScoreRoutes";
import teacherAbsenceRoutes from "./routes/teacherAbsenceRoutes";
import studentAbsenceRoutes from "./routes/studentAbsenceRoutes";
import payStudentTuitionRoutes from "./routes/payStudentTuitionRoutes";
import profileRoutes from "./routes/profileRoute"
import authMiddleware from "./middlewares/authMiddleware";
import adminMiddleware from "./middlewares/adminMiddleware";

dotenv.config();
connectDatabase();

const port = process.env.PORT || 8081;
const app: Express = express();

// Body parsing middleware
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

app.use("/api", authRoutes);

// Custom authentication middleware
app.use(authMiddleware);

// Routes
app.use("/api/v1/classLevel", ClassLevelRoutes);
app.use("/api/v1/classroom", ClassroomRoutes);
app.use("/api/v1/academic", AcademicYearRoutes);
app.use("/api/v1/school_fee", SchoolFeeRoutes);
app.use("/api/v1/subjectV2", subjectV2Routes);
app.use("/api/v1/student", studentRoutes);
app.use("/api/v1/subject", subjectRoutes);
app.use("/api/v1/teacher", teacherRoutes);

app.use("/api/v1/registration", registrationRoutes);

app.use("/api/v1/assignment", assignmentRoutes);
app.use("/api/v1/studentAssessment", studentAssessmentRoutes);
app.use("/api/v1/teacherAssessment", TeacherAssessmentRoutes);
app.use("/api/v1/score", ScoreRoutes);
app.use("/api/v1/teacherAbsence", teacherAbsenceRoutes);
app.use("/api/v1/studentAbsence", studentAbsenceRoutes);
app.use("/api/v1/payStudentTuition", payStudentTuitionRoutes);
app.use("/api/v1/profileRoutes", profileRoutes);

app.use(adminMiddleware);

// Error handling middleware
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status(500).json({ message: "Internal Server Error" });
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
