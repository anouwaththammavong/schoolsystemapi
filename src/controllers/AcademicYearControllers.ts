import { Request, Response } from "express";
import academicYearRepository from "../data/repositories/AcademicYearRepository";
import AcademicYearModels, {
  AcademicYear,
} from "../domain/models/AcademicYearModels";

export const createAcademicYear = async (req: Request, res: Response) => {
  try {
    const academicYearData: AcademicYear = req.body;
    await academicYearRepository.createAcademicYear(
      AcademicYearModels,
      academicYearData
    );
    res.status(201).json({ message: "Academic year created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getAcademicYear = async (req: Request, res: Response) => {
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const academicYears = await academicYearRepository.getAcademicYear(
      limit,
      offset,
      AcademicYearModels
    );

    res.status(200).json({ academicYears });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateAcademicYear = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await academicYearRepository.updateAcademicYear(
      _id,
      reqBody,
      AcademicYearModels
    );
    res.status(201).json({ message: "Academic year updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteAcademicYear = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await academicYearRepository.deleteAcademicYear(AcademicYearModels, _id);
    res.status(201).json({ message: "Academic year deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
