import { Request, Response } from "express";
import classLevelModels, {
  ClassLevel,
} from "../domain/models/classLevelModels";
import classLevelRepository from "../data/repositories/classLevelRepository";

export const createClassLevel = async (req: Request, res: Response) => {
  try {
    const classLevelData: ClassLevel = req.body;
    await classLevelRepository.createClassLevel(
      classLevelModels,
      classLevelData
    );
    res.status(201).json({ message: "Class Level created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getClassLevel = async (req: Request, res: Response) => {
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const classLevels = await classLevelRepository.getClassLevel(
      classLevelModels,
      limit,
      offset
    );
    res.status(200).json({ classLevels });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateClassLevel = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await classLevelRepository.updateClassLevel(classLevelModels, _id, reqBody);
    res.status(201).json({ message: "ClassLevel updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteClassLevel = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await classLevelRepository.deleteClassLevel(classLevelModels, _id);
    res.status(201).json({ message: "Class Level successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
