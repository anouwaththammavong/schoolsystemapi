import { Request, Response } from "express";
import ClassRoomModels, { ClassRoom } from "../domain/models/ClassRoomModels";
import classRoomRepository from "../data/repositories/ClassRoomRepository";

export const createClassRoom = async (req: Request, res: Response) => {
  try {
    const classRoomData: ClassRoom = req.body;
    await classRoomRepository.createClassRoom(ClassRoomModels, classRoomData);
    res.status(201).json({ message: "Class room created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getClassRooms = async (req: Request, res: Response) => {
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const classrooms = await classRoomRepository.getClassRoom(
      ClassRoomModels,
      limit,
      offset
    );

    res.status(200).json({ classrooms });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateClassroom = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await classRoomRepository.updateClassroom(ClassRoomModels, _id, reqBody);
    res.status(201).json({ message: "Classroom updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteClassroom = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await classRoomRepository.deleteClassroom(ClassRoomModels, _id);
    res.status(201).json({ message: "Classroom successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
