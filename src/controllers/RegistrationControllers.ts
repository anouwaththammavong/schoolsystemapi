import { Request, Response } from "express";
import RegistrationModels, {
  Registration,
} from "../domain/models/RegistrationModels";
import studentModels from "../domain/models/studentModels";
import registrationRepository from "../data/repositories/RegistrationRepository";

export const createOldStudentRegistration = async (
  req: Request,
  res: Response
) => {
  try {
    const registrationData: Registration = req.body;
    await registrationRepository.createOldStudentRegistration(
      RegistrationModels,
      studentModels,
      registrationData
    );
    res.status(201).json({ message: "Registration created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const createNewStudentRegistration = async (
  req: Request,
  res: Response
) => {
  try {
    const registrationData: Registration = req.body;
    await registrationRepository.createNewStudentRegistration(
      RegistrationModels,
      studentModels,
      registrationData
    );
    res.status(201).json({ message: "Registration created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getRegistrations = async (req: Request, res: Response) => {
  const new_student = req.query.new_student;
  const academic_year_no = req.query.academic_year_no;
  const room_id = req.query.room_id;
  const student_id = req.query.student_id;
  const isPaid = req.query.isPaid;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const registrations = await registrationRepository.getRegistrations(
      RegistrationModels,
      studentModels,
      new_student,
      academic_year_no,
      room_id,
      student_id,
      isPaid,
      limit,
      offset
    );

    res.status(200).json({ registrations });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateRegistration = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await registrationRepository.updateRegistration(
      _id,
      reqBody,
      RegistrationModels,
      studentModels
    );

    res.status(201).json({ message: "Registration updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateNewRegistration = async (req: Request, res: Response) => {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
      await registrationRepository.updateNewRegistration(
        _id,
        reqBody,
        RegistrationModels,
        studentModels
      );
  
      res.status(201).json({ message: "Registration updated successfully" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };

export const deleteRegistration = async (req: Request, res: Response) => {
    const _id = req.params.id;
    try {
      await registrationRepository.deleteRegistration(
        _id,
        RegistrationModels
      );
  
      res.status(201).json({ message: "Registration deleted successfully" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };
  