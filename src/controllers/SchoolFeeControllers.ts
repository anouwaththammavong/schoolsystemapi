import { Request, Response } from "express";
import schoolFeeRepository from "../data/repositories/SchoolFeeRepository";
import SchoolFeeModels, { SchoolFee } from "../domain/models/SchoolFeeModels";

export const createSchoolFee = async (req: Request, res: Response) => {
  try {
    const schoolFeeData: SchoolFee = req.body;
    await schoolFeeRepository.createSchoolFee(SchoolFeeModels, schoolFeeData);
    res.status(201).json({ message: "School fee created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getSchoolFees = async (req: Request, res: Response) => {
  const academic_year_no = req.query.academic_year_no;
  const level_id = req.query.level_id;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const school_fees = await schoolFeeRepository.getSchoolFees(
      SchoolFeeModels,
      academic_year_no,
      level_id,
      limit,
      offset
    );

    // Respond with the retrieved students
    res.status(200).json({ school_fees });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateSchoolFee = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await schoolFeeRepository.updateSchoolFee(_id, reqBody, SchoolFeeModels);
    res.status(201).json({ message: "School fee updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteSchoolFee = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await schoolFeeRepository.deleteSchoolFee(SchoolFeeModels, _id);
    res.status(201).json({ message: "School fee successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
}