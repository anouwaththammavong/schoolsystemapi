import { Request, Response } from "express";
import ScoreModels, { Score } from "../domain/models/ScoreModels";
import studentModels from "../domain/models/studentModels";
import scoreRepository from "../data/repositories/ScoreRepository";

export const createScore = async (req: Request, res: Response) => {
  try {
    const scoreData: Score = req.body;
    await scoreRepository.createScore(studentModels, ScoreModels, scoreData);
    res.status(201).json({ message: "Score created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getScores = async (req: Request, res: Response) => {
  const room_id = req.query.room_id;
  const generation = req.query.generation;
  const month = req.query.month;
  const term = req.query.term;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const scores = await scoreRepository.getScores(
      room_id,
      generation,
      month,
      term,
      limit,
      offset,
      ScoreModels
    );

    res.status(200).json({ scores });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateScore = async (req: Request, res: Response) => {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
        await scoreRepository.updateScore(_id,reqBody,studentModels,ScoreModels);
        res.status(201).json({ message: "Score updated successfully" })
    } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
}

// scoreModel: Model<ScoreDocument>, _id: string

export const deleteScore = async (req: Request, res: Response) => {
    const _id = req.params.id;
    try {
        await scoreRepository.deleteScore(ScoreModels,_id);
        res.status(201).json({ message: "Score deleted" });
    } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
}