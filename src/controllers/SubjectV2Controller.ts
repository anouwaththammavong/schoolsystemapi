import { Request, Response } from "express";
import subjectV2Models, {SubjectV2} from "../domain/models/subjectV2Models";
import subjectV2Repository from "../data/repositories/SubjectV2Repository";

export const createSubjectV2 = async (req: Request, res: Response) => {
    try {
      const subjectV2Data: SubjectV2 = req.body;
      await subjectV2Repository.createSubjectV2(
        subjectV2Models,
        subjectV2Data
      );
      res.status(201).json({ message: "Subject created successfully" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };
  
  export const getSubjectV2 = async (req: Request, res: Response) => {
    const limit = req.query.limit;
    const offset = req.query.offset;
    try {
      const subjectV2s = await subjectV2Repository.getSubjectV2(
        subjectV2Models,
        limit,
        offset
      );
      res.status(200).json({ subjectV2s });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };
  
  export const updateSubjectV2 = async (req: Request, res: Response) => {
    const reqBody = req.body;
    const _id = req.params.id;
    try {
      await subjectV2Repository.updateSubjectV2(subjectV2Models, _id, reqBody);
      res.status(201).json({ message: "SubjectV2 updated successfully" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };
  
  export const deleteSubjectV2 = async (req: Request, res: Response) => {
    const _id = req.params.id;
    try {
      await subjectV2Repository.deleteSubjectV2(subjectV2Models, _id);
      res.status(201).json({ message: "Subject successfully deleted" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };
  