import { Request, Response } from "express";
import assignmentModels, {
  Assignment,
} from "../domain/models/assignmentModels";
import assignmentRepository from "../data/repositories/assignmentRepository";
import studentModels from "../domain/models/studentModels";

export const createAssignment = async (req: Request, res: Response) => {
  const { class_room, class_level, title } = req.body;
  try {
    await assignmentRepository.createAssignment(
      studentModels,
      assignmentModels,
      class_room,
      class_level,
      title
    );
    res.status(201).json({ message: "Assignment created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
