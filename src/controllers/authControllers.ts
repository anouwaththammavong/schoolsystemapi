import { Request, Response } from "express";
import userModel, { User } from "../domain/models/userModel";
import bcrypt from "bcrypt";
import authRepository from "../data/repositories/authRepository";

export const register = async (req: Request, res: Response) => {
  try {
    const { username, password, role } = req.body;

    // check if the user already exists
    const existingUser = await userModel.findOne({ username: username });
    if (existingUser) {
      return res.status(409).json({ message: "User already exists" });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user
    await userModel.create({ username, password: hashedPassword, role });
    res.status(201).json({ message: "User registered successfully" });
  } catch (error: any) {
    res.status(500).json({ message: "Internal server error" });
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { identified, password } = req.body;

    // Check if the user exists in the database
    const user = await userModel.findOne({ username: identified });
    if (!user) {
      return res.status(401).json({ message: "Invalid credentials" });
    }

    // compare Password
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ message: "Invalid credentials" });
    }

    //  Generate JWT token
    const accessToken = authRepository.generateAccessToken(user);
    const refreshToken = authRepository.generateRefreshToken(user);

    res.json({ accessToken, refreshToken });
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
};

export const updateUser = async (req: Request, res: Response) => {
  const reqBody: User = req.body;
  const _id = req.params.id;
  const { username, password, role } = reqBody;
  const user = await userModel.findById(_id);

  if (user) {
    try {
      if (user.username !== username) {
        // check if the user already exists
        const existingUser = await userModel.findOne({ username: username });
        if (existingUser) {
          return res
            .status(409)
            .json({ message: "ຂໍ້ມູນຜູ້ໃຊ້ນີ້ມີໃນລະບົບແລ້ວ" });
        }
      }

      // Only hash the password if it is provided
      if (password) {
        user.password = await bcrypt.hash(password, 10);
        console.log("hi")
      }
      user.username = username || user.username;
      user.role = role || user.role;
      await user.save();
      res.status(201).json({ message: "User update successfully" });
    } catch (error: any) {
      res.status(500).json({ message: "Internal server error" });
    }
  } else {
    throw new Error("ບໍ່ພົບຂໍ້ມູນຜູ້ໃຊ້");
  }
};

export const getUser = async (req: Request, res: Response) => {
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const totalUser = await userModel.countDocuments();

    const users = await userModel
      .find()
      .limit(Number(limit))
      .skip(Number(offset));

    res.status(200).json({ users, totalUser });
  } catch (error: any) {
    throw new Error("Failed to get user: " + (error as Error).message);
  }
};

export const refreshToken = async (req: Request, res: Response) => {
  try {
    const refreshToken = req.body.refreshToken;

    // verify the refresh token
    const decoded = authRepository.verifyRefreshToken(refreshToken);
    if (!decoded) {
      return res.sendStatus(403);
    } else {
      // Generate JWT token
      const accessToken = authRepository.generateAccessToken(decoded);
      const refreshToken = authRepository.generateRefreshToken(decoded);

      res.json({ accessToken, refreshToken });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
};

export const logout = async (req: Request, res: Response) => {
  res.sendStatus(204);
};
