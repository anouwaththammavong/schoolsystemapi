import { Request, Response } from "express";
import studentModels from "../domain/models/studentModels";
import payStudentTuition from "../domain/models/payStudentTuition";
import payStudentTuitionRepository from "../data/repositories/payStudentTuitionRepository";

export const getPayStudentTuition = async (req: Request, res: Response) => {
  const class_room = req.query.class_room;
  const class_level = req.query.class_level;
  const generation = req.query.generation;
  const term_1 = req.query.term_1;
  const term_2 = req.query.term_2;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const pay_student_tuition =
      await payStudentTuitionRepository.getPayStudentTuition(
        class_room,
        class_level,
        generation,
        term_1,
        term_2,
        limit,
        offset,
        studentModels,
        payStudentTuition
      );

    res.status(200).json({ pay_student_tuition });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updatePayStudentTuition = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await payStudentTuitionRepository.updatePayStudentTuition(
      payStudentTuition,
      _id,
      reqBody
    );
    res
      .status(201)
      .json({ message: "Pay student tuition updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
