import { Request,Response } from "express"
import userRepository from "../data/repositories/userRepository";

export const getProfile = async (req:Request,res:Response) => {
    try {
        const users = await userRepository.getUserByUsername((req as any).decoded.username);
        if (!users) {
            res.status(500).json({ message: 'Internal server error' });
        } else {
            res.status(200).json({
                username: users.username,
                userRole: users.role
            })
        }
    }catch (error:any) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}