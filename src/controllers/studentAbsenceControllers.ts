import { Request, Response } from "express";
import studentAbsenceModels, {
  StudentAbsence,
} from "../domain/models/studentAbsenceModels";
import studentAbsenceRepository from "../data/repositories/studentAbsenceRepository";
import studentModels from "../domain/models/studentModels";

export const createStudentAbsence = async (req: Request, res: Response) => {
  try {
    const studentAbsenceData: StudentAbsence = req.body;
    await studentAbsenceRepository.createStudentAbsence(
      studentAbsenceModels,
      studentModels,
      studentAbsenceData
    );
    res.status(201).json({ message: "Student absence created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getAllStudentAbsence = async (req: Request, res: Response) => {
  const room_id = req.query.room_id;
  const generation = req.query.generation
  const student_id = req.query.student_id;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const studentAbsence = await studentAbsenceRepository.getAllStudentAbsence(
      room_id,
      generation,
      student_id,
      limit,
      offset,
      studentModels,
      studentAbsenceModels
    );
    res.status(200).json({ studentAbsence });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
}

export const getStudentAbsence = async (req: Request, res: Response) => {
  const room_id = req.query.room_id;
  const absence_date = req.query.absence_date;
  const student_id = req.query.student_id;
  const term = req.query.term;
  const be_reasonable = req.query.be_reasonable;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const studentAbsence = await studentAbsenceRepository.getStudentAbsence(
      room_id,
      absence_date,
      student_id,
      term,
      be_reasonable,
      limit,
      offset,
      studentAbsenceModels
    );
    res.status(200).json({ studentAbsence });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateStudentAbsence = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await studentAbsenceRepository.updateStudentAbsence(
      _id,
      reqBody,
      studentAbsenceModels
    );
    res.status(201).json({ message: "Student absence updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteStudentAbsence = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await studentAbsenceRepository.deleteStudentAbsence(
      studentAbsenceModels,
      _id
    );
    res.status(201).json({ message: "Student absence successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
