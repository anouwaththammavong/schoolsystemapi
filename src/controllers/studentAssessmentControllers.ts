import { Request, Response } from "express";
import studentAssessmentModels, {
  StudentAssessment,
} from "../domain/models/studentAssessmentModels";
import studentAssessmentRepository from "../data/repositories/studentAssessmentRepository";
import studentModels from "../domain/models/studentModels";

export const createStudentAssessment = async (req: Request, res: Response) => {
  try {
    const studentAssessmentData: StudentAssessment = req.body;
    await studentAssessmentRepository.createStudentAssessment(
      studentAssessmentModels,
      studentAssessmentData
    );
    res
      .status(201)
      .json({ message: "Student Assessment created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getStudentAssessment = async (req: Request, res: Response) => {
  const level_id = req.query.level_id;
  const room_id = req.query.room_id;
  const generation = req.query.generation;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const studentAssessments =
      await studentAssessmentRepository.getStudentAssessment(
        level_id,
        room_id,
        generation,
        limit,
        offset,
        studentModels,
        studentAssessmentModels
      );

    res.status(200).json({ studentAssessments });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateStudentAssessment = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await studentAssessmentRepository.updateStudentAssessment(
      studentAssessmentModels,
      _id,
      reqBody
    );
    res
      .status(201)
      .json({ message: "Student Assessment updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteStudentAssessment = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await studentAssessmentRepository.deleteStudentAssessment(
      studentAssessmentModels,
      _id
    );
    res
      .status(201)
      .json({ message: "Student Assessment successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
