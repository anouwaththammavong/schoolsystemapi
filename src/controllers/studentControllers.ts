import { Request, Response } from "express";
import studentRepository from "../data/repositories/studentRepository";
import studentModels, { Student } from "../domain/models/studentModels";

export const createStudent = async (req: Request, res: Response) => {
  try {
    const studentData: Student = req.body;
    await studentRepository.createStudent(studentModels, studentData);
    res.status(201).json({ message: "Student created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getStudent = async (req: Request, res: Response) => {
  const student_id = req.query.student_id;
  const name = req.query.name;
  const generation = req.query.generation;
  const level_id = req.query.level_id;
  const new_student = req.query.new_student;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    // Call the getStudents function with the Mongoose model and any filters from the request
    const students = await studentRepository.getStudents(
      studentModels,
      student_id,
      name,
      generation,
      level_id,
      new_student,
      limit,
      offset
    );

    // Respond with the retrieved students
    res.status(200).json({ students });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateStudent = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await studentRepository.updateStudent(studentModels, _id, reqBody);
    res.status(201).json({ message: "Student updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteStudent = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await studentRepository.deleteStudent(studentModels, _id);
    res.status(201).json({ message: "Student successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
