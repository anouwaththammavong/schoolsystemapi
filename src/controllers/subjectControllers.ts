import { Request, Response } from "express";
import studentModels from "../domain/models/studentModels";
import subjectRepository from "../data/repositories/subjectRepository";
import subjectModels from "../domain/models/subjectModels";

export const getSubject = async (req: Request, res: Response) => {
  const class_room = req.query.class_room;
  const class_level = req.query.class_level;
  const term = req.query.term;
  const generation = req.query.generation;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const subjects = await subjectRepository.getSubject(
      class_room,
      class_level,
      term,
      generation,
      limit,
      offset,
      studentModels,
      subjectModels
    );

    res.status(200).json({ subjects });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateSubject = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await subjectRepository.updateSubject(subjectModels, _id, reqBody);
    res.status(201).json({ message: "Subject updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteSubject = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await subjectRepository.deleteSubject(subjectModels, _id);
    res.status(201).json({ message: "Student successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
