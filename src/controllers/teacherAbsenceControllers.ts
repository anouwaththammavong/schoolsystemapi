import { Request, Response } from "express";
import teacherAbsenceModels, {
  TeacherAbsence,
} from "../domain/models/teacherAbsenceModels";
import teacherModels from "../domain/models/teacherModels";
import teacherAbsenceRepository from "../data/repositories/teacherAbsenceRepository";

export const createTeacherAbsence = async (req: Request, res: Response) => {
  try {
    const teacherAbsenceData: TeacherAbsence = req.body;
    await teacherAbsenceRepository.createTeacherAbsence(
      teacherAbsenceModels,
      teacherAbsenceData
    );
    res.status(201).json({ message: "Teacher absence created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getTeacherAbsence = async (req: Request, res: Response) => {
  const absence_date = req.query.absence_date;
  const teacher_id = req.query.teacher_id;
  const be_reasonable = req.query.be_reasonable;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const teacherAbsence =
      await teacherAbsenceRepository.getTeacherAbsenceRepository(
        absence_date,
        teacher_id,
        be_reasonable,
        limit,
        offset,
        teacherAbsenceModels
      );
    res.status(200).json({ teacherAbsence });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateTeacherAbsence = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await teacherAbsenceRepository.updateTeacherAbsenceRepository(
      _id,
      reqBody,
      teacherAbsenceModels
    );
    res.status(201).json({ message: "Teacher absence updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteTeacherAbsence = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await teacherAbsenceRepository.deleteTeacherAbsence(
      teacherAbsenceModels,
      _id
    );
    res.status(201).json({ message: "Teacher absence successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
