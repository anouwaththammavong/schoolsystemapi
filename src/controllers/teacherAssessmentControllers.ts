import { Request, Response } from "express";
import TeacherAssessmentModels, {
  TeacherAssessment,
} from "../domain/models/TeacherAssessmentModels";
import teacherModels from "../domain/models/teacherModels";
import teacherAssessmentRepository from "../data/repositories/TeacherAssessmentRepository";

export const createTeacherAssessment = async (req: Request, res: Response) => {
  try {
    const teacherAssessmentData: TeacherAssessment = req.body;
    await teacherAssessmentRepository.createTeacherAssessment(
      TeacherAssessmentModels,
      teacherModels,
      teacherAssessmentData
    );
    res
      .status(201)
      .json({ message: "Teacher Assessment created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getTeacherAssessment = async (req: Request, res: Response) => {
  const teacher_id = req.query.teacher_id;
  const assessment_date = req.query.assessment_date;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    const teacherAssessments =
      await teacherAssessmentRepository.getTeacherAssessment(
        teacher_id,
        assessment_date,
        limit,
        offset,
        TeacherAssessmentModels,
        teacherModels
      );
    res.status(200).json({ teacherAssessments });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateTeacherAssessment = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await teacherAssessmentRepository.updateTeacherAssessment(
      _id,
      reqBody,
      TeacherAssessmentModels,
      teacherModels
    );
    res
      .status(201)
      .json({ message: "Teacher Assessment updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteTeacherAssessment = async (req: Request, res: Response) => {
    const _id = req.params.id;
    try {
      await teacherAssessmentRepository.deleteTeacherAssessment(
        TeacherAssessmentModels,
        _id
      );
      res
        .status(201)
        .json({ message: "Teacher Assessment successfully deleted" });
    } catch (error: any) {
      res
        .status(500)
        .json({ message: "Internal server error", error: error.message });
    }
  };