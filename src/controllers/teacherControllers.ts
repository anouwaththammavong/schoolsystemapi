import { Request, Response } from "express";
import teacherModels, { Teacher } from "../domain/models/teacherModels";
import teacherRepository from "../data/repositories/teacherRepository";

export const createTeacher = async (req: Request, res: Response) => {
  try {
    const teacherData: Teacher = req.body;
    await teacherRepository.createTeacher(teacherModels, teacherData);
    res.status(201).json({ message: "Teacher created successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const getTeachers = async (req: Request, res: Response) => {
  const teacher_id = req.query.teacher_id;
  const limit = req.query.limit;
  const offset = req.query.offset;
  try {
    // Call the getStudents function with the Mongoose model and any filters from the request
    const teachers = await teacherRepository.getTeachers(
      teacherModels,
      teacher_id,
      limit,
      offset
    );

    // Respond with the retrieved students
    res.status(200).json({ teachers });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const updateTeacher = async (req: Request, res: Response) => {
  const reqBody = req.body;
  const _id = req.params.id;
  try {
    await teacherRepository.updateTeacher(teacherModels, _id, reqBody);
    res.status(201).json({ message: "Teacher updated successfully" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};

export const deleteTeacher = async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    await teacherRepository.deleteTeacher(teacherModels, _id);
    res.status(201).json({ message: "Teacher successfully deleted" });
  } catch (error: any) {
    res
      .status(500)
      .json({ message: "Internal server error", error: error.message });
  }
};
