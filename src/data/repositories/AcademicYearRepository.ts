import { Model, Document } from "mongoose";
import { AcademicYear } from "../../domain/models/AcademicYearModels";

type AcademicYearDocument = Document & AcademicYear;

const academicYearRepository = {
  createAcademicYear: async (
    academicYearModel: Model<AcademicYearDocument>,
    academicYearData: AcademicYear
  ) => {
    try {
      return academicYearModel.create(academicYearData);
    } catch (error: any) {
      // Handle the error
      throw new Error(
        "Failed to create academic year: " + (error as Error).message
      );
    }
  },

  getAcademicYear: async (
    limit: any,
    offset: any,
    academicYearModel: Model<AcademicYearDocument>
  ) => {
    try {
      const totalAcademicYears = await academicYearModel.countDocuments();

      const academicYear = await academicYearModel
        .find()
        .sort({ academic_year_no: 1 })
        .limit(limit)
        .skip(offset);

      return {
        academicYear,
        totalAcademicYears,
      };
    } catch (error: any) {
      throw new Error(
        "Failed to get academic year: " + (error as Error).message
      );
    }
  },

  updateAcademicYear: async (
    _id: string,
    reqBody: AcademicYearDocument,
    academicYearModel: Model<AcademicYearDocument>
  ) => {
    const { academic_year, academic_year_no } = reqBody;
    const academicYear = await academicYearModel.findById(_id);
    if (academicYear) {
      try {
        academicYear.academic_year =
          academic_year || academicYear.academic_year;
        academicYear.academic_year_no =
          academic_year_no || academicYear.academic_year_no;

        await academicYear.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update academic year: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no academic year information found");
    }
  },

  deleteAcademicYear: async (
    academicYearModel: Model<AcademicYearDocument>,
    id: string
  ) => {
    try {
      const academicYear = await academicYearModel.deleteOne({ _id: id });
      if (academicYear.deletedCount === 0) {
        throw new Error("Academic year not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete academic year: " + error.message);
    }
  },
};

export default academicYearRepository