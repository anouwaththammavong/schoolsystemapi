import { Model, Document } from "mongoose";
import { ClassRoom } from "../../domain/models/ClassRoomModels";

type ClassRoomDocument = Document & ClassRoom;

const classRoomRepository = {
  createClassRoom: async (
    classRoomModel: Model<ClassRoomDocument>,
    classRoomData: ClassRoom
  ) => {
    try {
      const existingRoomId = await classRoomModel.findOne({
        room_id: classRoomData.room_id,
      });
      if (existingRoomId) {
        throw new Error("ໄອດີຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
      }

      // Check if class room name if is unique
      // const existingClassRoomName = await classRoomModel.findOne({
      //   room_name: classRoomData.room_name,
      // });
      // if (existingClassRoomName) {
      //   throw new Error("ຊື່ຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
      // }

      const existingLevelId = await classRoomModel.findOne({
        level_id: classRoomData.level_id,
      });
      if (existingLevelId) {
        throw new Error("ໄອດີຂອງຊັ້ນຮຽນໃນຫ້ອງນີ້ແມ່ນມີແລ້ວ");
      }

      return classRoomModel.create(classRoomData);
    } catch (error: any) {
      throw new Error(
        "Failed to create class room: " + (error as Error).message
      );
    }
  },

  getClassRoom: async (
    classRoomModel: Model<ClassRoomDocument>,
    limit: any,
    offset: any
  ) => {
    try {
      const totalCount = await classRoomModel.countDocuments();

      const classRooms = await classRoomModel
        .find()
        .populate("level_id")
        .sort({ room_name: 1 })
        .limit(limit)
        .skip(offset)
        .exec();

      return { classRooms, totalCount };
    } catch (error: any) {
      throw new Error("Failed to get classroom: " + error.message);
    }
  },

  updateClassroom: async (
    classRoomModel: Model<ClassRoomDocument>,
    _id: string,
    reqBody: ClassRoom
  ) => {
    const { level_id, room_id, room_name } = reqBody;
    const classroom = await classRoomModel.findOne({ _id: _id });
    if (classroom) {
      try {
        if (String(classroom.level_id) !== String(level_id)) {
          const existingLevelId = await classRoomModel.findOne({
            level_id: level_id,
          });
          if (existingLevelId) {
            throw new Error("ໄອດີຂອງຊັ້ນຮຽນໃນຫ້ອງນີ້ແມ່ນມີແລ້ວ");
          }
        }

        if (classroom.room_id !== room_id) {
          const existingRoomId = await classRoomModel.findOne({
            room_id: room_id,
          });
          if (existingRoomId) {
            throw new Error("ໄອດີຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        // if (classroom.room_name !== room_name) {
        //   // Check if class room name if is unique
        //   const existingClassRoomName = await classRoomModel.findOne({
        //     room_name: room_name,
        //   });
        //   if (existingClassRoomName) {
        //     throw new Error("ຊື່ຂອງຫ້ອງຣຽນນີ້ແມ່ນມີແລ້ວ");
        //   }
        // }

        classroom.level_id = level_id || classroom.level_id;
        classroom.room_id = room_id || classroom.room_id;
        classroom.room_name = room_name || classroom.room_name;

        await classroom.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update classroom: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no class room information found");
    }
  },
  
  deleteClassroom: async (
    classRoomModel: Model<ClassRoomDocument>,
    _id: string
  ) => {
    try {
      const classroom = await classRoomModel.deleteOne({ _id: _id });
      if (classroom.deletedCount === 0) {
        throw new Error("Classroom not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete Classroom: " + error.message);
    }
  },
};

export default classRoomRepository;
