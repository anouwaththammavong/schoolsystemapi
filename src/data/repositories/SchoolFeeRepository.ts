import { Model, Document } from "mongoose";
import { SchoolFee } from "../../domain/models/SchoolFeeModels";

type SchoolFeeDocument = Document & SchoolFee;

const schoolFeeRepository = {
  createSchoolFee: async (
    schoolFeeModel: Model<SchoolFeeDocument>,
    schoolFeeData: SchoolFee
  ) => {
    try {
      const existingSchool_fee_id = await schoolFeeModel.findOne({
        school_fee_id: schoolFeeData.school_fee_id,
      });
      if (existingSchool_fee_id) {
        throw new Error("ໄອດີຂອງຄ່າທຳນຽມນີ້ແມ່ນມີແລ້ວ");
      }

      const existingAcademic_year_no = await schoolFeeModel.findOne({
        academic_year_no: schoolFeeData.academic_year_no,
        level_id: schoolFeeData.level_id,
      });
      if (existingAcademic_year_no) {
        throw new Error("ຂໍ້ມູນຂອງສົກຮຽນທີ້ນີ້ ຫຼື ຊັ້ນຮຽນນີ້ແມ່ນມີແລ້ວ");
      }

      return schoolFeeModel.create(schoolFeeData);
    } catch (error: any) {
      throw new Error(
        "Failed to create school fee: " + (error as Error).message
      );
    }
  },

  getSchoolFees: async (
    schoolFeeModel: Model<SchoolFeeDocument>,
    academic_year_no: any,
    level_id: any,
    limit: any,
    offset: any
  ) => {
    try {
      let filter: { academic_year_no?: any; level_id?: any } = {};

      if (academic_year_no) {
        filter = { ...filter, academic_year_no: academic_year_no };
      }

      if (level_id) {
        filter = { ...filter, level_id: level_id };
      }

      const totalCount = await schoolFeeModel.countDocuments(filter);

      const schoolFees = await schoolFeeModel
        .find(filter)
        .populate("academic_year_no")
        .populate("level_id")
        .sort({ academic_year_no: 1 })
        .limit(limit)
        .skip(offset)
        .exec();

      return { schoolFees, totalCount };
    } catch (error: any) {
      throw new Error("Failed to get School fee: " + (error as Error).message);
    }
  },

  updateSchoolFee: async (
    _id: string,
    reqBody: SchoolFeeDocument,
    schoolFeeModel: Model<SchoolFeeDocument>
  ) => {
    const {
      school_fee_id,
      academic_year_no,
      level_id,
      book_fee,
      uniform_fee,
      term_fee,
      total_fee,
    } = reqBody;
    const school_fee = await schoolFeeModel.findById(_id);
    if (school_fee) {
      try {
        if (String(school_fee.school_fee_id) !== String(school_fee_id)) {
          const existingSchool_fee_id = await schoolFeeModel.findOne({
            school_fee_id: school_fee_id,
          });
          if (existingSchool_fee_id) {
            throw new Error("ໄອດີຂອງຄ່າທຳນຽມນີ້ແມ່ນມີແລ້ວ");
          }
        }

        if (String(school_fee.academic_year_no) !== String(academic_year_no)) {
          const existingAcademic_year_no = await schoolFeeModel.findOne({
            academic_year_no: academic_year_no,
            level_id: level_id,
          });
          if (existingAcademic_year_no) {
            throw new Error("ຂໍ້ມູນຂອງສົກຮຽນທີ້ນີ້ ຫຼື ຊັ້ນຮຽນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        school_fee.school_fee_id = school_fee_id || school_fee.school_fee_id;
        school_fee.academic_year_no =
          academic_year_no || school_fee.academic_year_no;
        school_fee.level_id = level_id || school_fee.level_id;
        school_fee.book_fee = book_fee || school_fee.book_fee;
        school_fee.uniform_fee = uniform_fee || school_fee.uniform_fee;
        school_fee.term_fee = term_fee || school_fee.term_fee;
        school_fee.total_fee = total_fee || school_fee.total_fee;

        await school_fee.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update student absence: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no school fee information found");
    }
  },

  deleteSchoolFee: async (
    schoolFeeModel: Model<SchoolFeeDocument>,
    _id: string
  ) => {
    try {
      const schoolFee = await schoolFeeModel.deleteOne({ _id: _id });
      if (schoolFee.deletedCount === 0) {
        throw new Error("School fee not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete School fee: " + error.message);
    }
  },
};

export default schoolFeeRepository;
