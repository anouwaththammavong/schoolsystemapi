import mongoose, { Model, Document, model } from "mongoose";
import { Student } from "../../domain/models/studentModels";
import { Score } from "../../domain/models/ScoreModels";

type ScoreDocument = Document & Score;
type StudentDocument = Document & Student;

const scoreRepository = {
  createScore: async (
    studentModel: Model<StudentDocument>,
    scoreModel: Model<ScoreDocument>,
    scoreData: Score
  ) => {
    try {
      const student = await studentModel.findOne({
        student_id: scoreData.student,
      });
      if (!student) {
        throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
      }

      const score_month = new Date(scoreData.month);
      const startOfMonth = new Date(
        score_month.getFullYear(),
        score_month.getMonth(),
        1
      );
      const endOfMonth = new Date(
        score_month.getFullYear(),
        score_month.getMonth() + 1,
        0,
        23,
        59,
        59,
        999
      );

      const existingStudentScoreMonth = await scoreModel.findOne({
        student: student._id,
        room_id: scoreData.room_id,
        level_id: scoreData.level_id,
        generation: scoreData.generation,
        month: {
          $gte: startOfMonth, // Greater than or equal to the start of the month
          $lt: endOfMonth, // Less than the start of the next month
        },
      });

      if (existingStudentScoreMonth) {
        throw new Error("ຂໍ້ມູນຄະແນນເສັງເດືອນນີ້ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
      }

      if (scoreData.term === "ພາກຮຽນ1") {
        const existingTerm1 = await scoreModel.findOne({
          student: student._id,
          room_id: scoreData.room_id,
          level_id: scoreData.level_id,
          generation: scoreData.generation,
          term: "ພາກຮຽນ1",
        });
        if (existingTerm1) {
          throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 1 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
        }
      }

      if (scoreData.term === "ພາກຮຽນ2") {
        const existingTerm1 = await scoreModel.findOne({
          student: student._id,
          room_id: scoreData.room_id,
          level_id: scoreData.level_id,
          generation: scoreData.generation,
          term: "ພາກຮຽນ2",
        });
        if (existingTerm1) {
          throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 2 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
        }
      }

      const newScore = await scoreModel.create({
        student: student._id,
        room_id: scoreData.room_id,
        level_id: scoreData.level_id,
        generation: scoreData.generation,
        month: scoreData.month,
        term: scoreData.term,
        subjects: scoreData.subjects,
        total_point: scoreData.subjects.reduce(
          (total, subject) => Number(total) + Number(subject.point),
          0
        ),
        rank: scoreData.rank,
      });

      if (!scoreData.rank) {
        // Rank students in the same room_id, level_id, generation, month, term
        const scoresInSameCriteria = await scoreModel.find({
          room_id: scoreData.room_id,
          level_id: scoreData.level_id,
          generation: scoreData.generation,
          month: {
            $gte: startOfMonth, // Greater than or equal to the start of the month
            $lt: endOfMonth, // Less than the start of the next month
          },
          term: scoreData.term,
        });

        // Sort the scores based on their total_point in descending order
        const sortedScores = scoresInSameCriteria.sort(
          (a, b) => b.total_point - a.total_point
        );

        // update rank for each score
        let rank = 1;
        let prevTotalPoint = sortedScores[0]?.total_point || 0;
        for (const score of sortedScores) {
          if (score.total_point < prevTotalPoint) {
            rank++;
          }
          score.rank = rank;
          await score.save();
          prevTotalPoint = score.total_point;
        }
      }

      return newScore;

      //   return scoreModel.create({
      //     student: student._id,
      //     room_id: scoreData.room_id,
      //     level_id: scoreData.level_id,
      //     generation: scoreData.generation,
      //     month: scoreData.month,
      //     term: scoreData.term,
      //     subjects: scoreData.subjects,
      //     total_point: scoreData.total_point,
      //     rank: scoreData.rank,
      //   });
    } catch (error: any) {
      throw new Error("Failed to create score: " + (error as Error).message);
    }
  },

  getScores: async (
    room_id: any,
    generation: any,
    month: any,
    term: any,
    limit: any,
    offset: any,
    scoreModel: Model<ScoreDocument>
  ) => {
    try {
      const convertScoreMonth = new Date(month);
      const scoreYear = convertScoreMonth.getFullYear();
      const scoreMonth = convertScoreMonth.getMonth() + 1;
      let filter: {
        room_id: any;
        generation: any;
        $expr?: any;
        term?: any;
      } = {
        room_id: room_id,
        generation: generation,
        // $expr: {
        //   $and: [
        //     { $eq: [{ $year: "$month" }, scoreYear] }, // Match year
        //     { $eq: [{ $month: "$month" }, scoreMonth] },
        //   ],
        // },
      };

      if (term) {
        // If term is provided, filter by term
        filter.term = term;
      } else {
        // If term is not provided, filter by year and month
        filter.term = "ບໍ່ແມ່ນ"
        filter.$expr = {
          $and: [
            { $eq: [{ $year: "$month" }, scoreYear] }, // Match year
            { $eq: [{ $month: "$month" }, scoreMonth] }, // Match month
          ],
        };
      }

      // Count total score matching the condition before applying limit and skip
      const totalScore = await scoreModel.countDocuments(filter);

      const scores = await scoreModel
        .find(filter)
        .populate("student")
        .populate("room_id")
        .populate("level_id")
        .populate({
          path: "subjects",
          populate: [
            { path: "title", model: "SubjectV2" },
            { path: "teacher", model: "Teachers" },
          ],
        })
        .sort({ rank: 1 })
        .limit(limit)
        .skip(offset)
        .exec();

      return {
        scores,
        totalScore,
      };
    } catch (error: any) {
      throw new Error("Failed to get score: " + (error as Error).message);
    }
  },

  updateScore: async (
    _id: string,
    reqBody: ScoreDocument,
    studentModel: Model<StudentDocument>,
    scoreModel: Model<ScoreDocument>
  ) => {
    const {
      student,
      room_id,
      level_id,
      generation,
      month,
      term,
      subjects,
      rank,
    } = reqBody;
    const score = await scoreModel.findById(_id);
    if (score) {
      try {
        // Find the student using the provided student
        const studentOne = await studentModel.findOne({ student_id: student });
        if (!studentOne) {
          throw new Error("ບໍ່ພົບຂໍ້ມູນນັກຮຽນ");
        }

        const score_month = new Date(month);
        const startOfMonth = new Date(
          score_month.getFullYear(),
          score_month.getMonth(),
          1
        );
        const endOfMonth = new Date(
          score_month.getFullYear(),
          score_month.getMonth() + 1,
          0,
          23,
          59,
          59,
          999
        );

        if (score.month.toISOString() !== new Date(month).toISOString()) {
          const existingStudentScoreMonth = await scoreModel.findOne({
            student: studentOne._id,
            room_id: room_id || score.room_id,
            level_id: level_id || score.level_id,
            generation: generation || score.generation,
            month: {
              $gte: startOfMonth, // Greater than or equal to the start of the month
              $lt: endOfMonth, // Less than the start of the next month
            },
          });

          if (existingStudentScoreMonth) {
            throw new Error("ຂໍ້ມູນຄະແນນເສັງເດືອນນີ້ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        if (score.term !== term) {
          if (term === "ພາກຮຽນ1") {
            const existingTerm1 = await scoreModel.findOne({
              student: studentOne._id,
              room_id: room_id,
              level_id: level_id,
              generation: generation,
              term: "ພາກຮຽນ1",
            });
            if (existingTerm1) {
              throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 1 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
          }

          if (term === "ພາກຮຽນ2") {
            const existingTerm1 = await scoreModel.findOne({
              student: studentOne._id,
              room_id: room_id,
              level_id: level_id,
              generation: generation,
              term: "ພາກຮຽນ2",
            });
            if (existingTerm1) {
              throw new Error("ຂໍ້ມູນຄະແນນເສັງພາກຣຽນ 2 ຂອງນັກຮຽນນີ້ແມ່ນມີແລ້ວ");
            }
          }
        }

        score.student = studentOne._id || score.student;
        score.room_id = room_id || score.room_id;
        score.level_id = level_id || score.level_id;
        score.generation = generation || score.generation;
        score.month = month || score.month;
        score.term = term || score.term;
        score.subjects = subjects || score.subjects;
        score.total_point = subjects
          ? subjects.reduce((total, subject) => total + subject.point, 0)
          : score.total_point;
        score.rank = rank || score.rank;

        await score.save();

        // Recalculate the rank if needed
        if (!rank) {
          // Build the common filter criteria
          const filter: {
            room_id: any;
            level_id: any;
            generation: any;
            term: any;
            month?: any;
          } = {
            room_id: room_id || score.room_id,
            level_id: level_id || score.level_id,
            generation: generation || score.generation,
            term: term || score.term,
          };

          // If term is "ບໍ່ແມ່ນ", add the month filter
          if (term === "ບໍ່ແມ່ນ") {
            filter.month = {
              $gte: startOfMonth, // Greater than or equal to the start of the month
              $lt: endOfMonth, // Less than the start of the next month
            };
          }
          // Find scores in the same criteria
          const scoresInSameCriteria = await scoreModel.find(filter);

          // Sort the scores based on their total_point in descending order
          const sortedScores = scoresInSameCriteria.sort(
            (a, b) => b.total_point - a.total_point
          );

          // Update rank for each score
          let rank = 1;
          let prevTotalPoint = sortedScores[0]?.total_point || 0;
          for (const scores of sortedScores) {
            if (scores.total_point < prevTotalPoint) {
              rank++;
            }
            scores.rank = rank;
            await scores.save();
            prevTotalPoint = scores.total_point;
          }
        }

        return score;
      } catch (error: any) {
        throw new Error("Failed to update score: " + (error as Error).message);
      }
    } else {
      throw new Error("ບໍ່ພົບຂໍ້ມູນຄະແນນເສັງນີ້");
    }
  },

  deleteScore: async (scoreModel: Model<ScoreDocument>, _id: string) => {
    try {
      const score = await scoreModel.deleteOne({ _id: _id });
      if (score.deletedCount === 0) {
        throw new Error("ບໍ່ພົບຂໍ້ມູນຄະແນນເສັງນີ້");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete score: " + error.message);
    }
  },
};

export default scoreRepository;
