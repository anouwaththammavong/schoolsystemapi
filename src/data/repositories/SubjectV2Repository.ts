import { Model, Document } from "mongoose";
import { SubjectV2 } from "../../domain/models/subjectV2Models";

type SubjectV2Document = Document & SubjectV2;

const subjectV2Repository = {
    createSubjectV2: async (
      subjectV2Model: Model<SubjectV2Document>,
      subjectV2Data: SubjectV2
    ) => {
      try {
        // Check if subject id if is unique
        const existingSubjectV2Id = await subjectV2Model.findOne({
          subject_id: subjectV2Data.subject_id,
        });
        if (existingSubjectV2Id) {
          throw new Error("ໄອດີຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
        }
  
        // Check if subject name if is unique
        const existingSubjectV2Name = await subjectV2Model.findOne({
          subject_name: subjectV2Data.subject_name,
        });
        if (existingSubjectV2Name) {
          throw new Error("ຊື່ຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
        }
  
        return subjectV2Model.create(subjectV2Data);
      } catch (error: any) {
        // Handle the error
        throw new Error(
          "Failed to create subject: " + (error as Error).message
        );
      }
    },
  
    getSubjectV2: async (
      subjectV2Model: Model<SubjectV2Document>,
      limit: any,
      offset: any
    ) => {
      try {
        const totalCount = await subjectV2Model.countDocuments();
  
        const subjectV2s = await subjectV2Model
          .find()
          .sort({ subject_name: 1 })
          .limit(Number(limit))
          .skip(Number(offset))
          .exec();
  
        return { subjectV2s, totalCount };
      } catch (error: any) {
        throw new Error("Failed to get subject: " + error.message);
      }
    },
  
    updateSubjectV2: async (
      subjectV2Model: Model<SubjectV2Document>,
      _id: string,
      reqBody: SubjectV2
    ) => {
      const { subject_id, subject_name } = reqBody;
      const subjectV2 = await subjectV2Model.findOne({ _id: _id });
      if (subjectV2) {
        try {
          if (subjectV2.subject_id !== subject_id) {
            const existingSubjectV2Id = await subjectV2Model.findOne({
              subject_id: subject_id,
            });
            if (existingSubjectV2Id) {
              throw new Error("ໄອດີຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
            }
          }
  
          if (subjectV2.subject_name !== subject_name) {
            // Check if subject name if is unique
            const existingSubjectV2Name = await subjectV2Model.findOne({
              subject_name: subject_name,
            });
            if (existingSubjectV2Name) {
              throw new Error("ຊື່ຂອງວິຊານີ້ແມ່ນມີແລ້ວ");
            }
          }
  
          subjectV2.subject_id = subject_id || subjectV2.subject_id;
          subjectV2.subject_name = subject_name || subjectV2.subject_name;
  
          await subjectV2.save();
        } catch (error: any) {
          throw new Error(
            "Failed to update subject: " + (error as Error).message
          );
        }
      } else {
        throw new Error("no subject information found");
      }
    },
  
    deleteSubjectV2: async (
      subjectV2Model: Model<SubjectV2Document>,
      _id: string
    ) => {
      try {
        const subjectV2 = await subjectV2Model.deleteOne({ _id: _id });
        if (subjectV2.deletedCount === 0) {
          throw new Error("Subject not found");
        }
      } catch (error: any) {
        // Handle any errors that occur during the find or delete operations
        throw new Error("Failed to delete subject: " + error.message);
      }
    },
  };
  
  export default subjectV2Repository;