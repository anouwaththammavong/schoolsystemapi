import mongoose, { Model, Document } from "mongoose";
import { Teacher } from "../../domain/models/teacherModels";
import { TeacherAssessment } from "../../domain/models/TeacherAssessmentModels";

type TeacherAssessmentDocument = Document & TeacherAssessment;
type TeacherDocument = Document & Teacher;

const teacherAssessmentRepository = {
  createTeacherAssessment: async (
    teacherAssessmentModel: Model<TeacherAssessmentDocument>,
    teacherModel: Model<TeacherDocument>,
    teacherAssessmentData: TeacherAssessment
  ) => {
    try {
      const teacher = await teacherModel.findOne({
        teacher_id: teacherAssessmentData.teacher,
      });
      if (!teacher) {
        throw new Error("ບໍ່ພົບຂໍ້ມູນອາຈານ");
      }

      const assessment_date = new Date(teacherAssessmentData.assessment_date);
      const startOfMonth = new Date(
        assessment_date.getFullYear(),
        assessment_date.getMonth(),
        1
      );
      const endOfMonth = new Date(
        assessment_date.getFullYear(),
        assessment_date.getMonth() + 1,
        0,
        23,
        59,
        59,
        999
      );

      const existingTeacherAssessment = await teacherAssessmentModel.findOne({
        teacher: teacher._id,
        assessment_date: {
          $gte: startOfMonth, // Greater than or equal to the start of the month
          $lt: endOfMonth, // Less than the start of the next month
        },
      });

      if (existingTeacherAssessment) {
        throw new Error("ການປະເມີນອາຈານຜູ້ນີ້ໃນເດືອນນີ້ແມ່ນມີແລ້ວ");
      }

      return teacherAssessmentModel.create({
        teacher: teacher._id,
        attendance_class: teacherAssessmentData.attendance_class,
        behavior: teacherAssessmentData.behavior,
        assess_teaching: teacherAssessmentData.assess_teaching,
        assessment_date: teacherAssessmentData.assessment_date,
        note: teacherAssessmentData.note,
      });
    } catch (error: any) {
      throw new Error(
        "ເກີດຂໍ້ຜິດພາດໃນການປະເມີນອາຈານ: " + (error as Error).message
      );
    }
  },
  getTeacherAssessment: async (
    teacher_id: any,
    assessment_date: any,
    limit: any,
    offset: any,
    teacherAssessmentModel: Model<TeacherAssessmentDocument>,
    teacherModel: Model<TeacherDocument>
  ) => {
    try {
      let condition: { teacher?: any; $expr?: any } = {};

      if (teacher_id) {
        const teacher = await teacherModel.findOne({
          teacher_id: teacher_id,
        });

        condition["teacher"] = teacher?._id;
      }

      if (assessment_date) {
        const convertAssessmentDate = new Date(assessment_date);
        const year = convertAssessmentDate.getFullYear();
        const month = convertAssessmentDate.getMonth() + 1;

        condition["$expr"] = {
          $and: [
            { $eq: [{ $year: "$assessment_date" }, year] }, // Match year
            { $eq: [{ $month: "$assessment_date" }, month] }, // Match month
          ],
        };
      }

      // Count total assessment matching the condition before applying limit and skip
      const totalAssessment = await teacherAssessmentModel.countDocuments(
        condition
      );

      const teacher_assessments = await teacherAssessmentModel
        .find(condition)
        .populate("teacher")
        .sort({ assessment_date: -1 }) // Sorting by assessment_date instead of name
        .limit(limit)
        .skip(offset)
        .exec();

      return {
        teacher_assessments,
        totalAssessment,
      };
    } catch (error: any) {
      throw new Error(
        "Failed to get teacher assessment: " + (error as Error).message
      );
    }
  },

  updateTeacherAssessment: async (
    _id: string,
    reqBody: TeacherAssessmentDocument,
    teacherAssessmentModel: Model<TeacherAssessmentDocument>,
    teacherModel: Model<TeacherDocument>
  ) => {
    const {
      teacher,
      attendance_class,
      behavior,
      assess_teaching,
      assessment_date,
      note,
    } = reqBody;
    const teacher_assessment = await teacherAssessmentModel.findById(_id);

    if (teacher_assessment) {
      try {
        // Find the teacher using the provided teacher
        const findTeacher = await teacherModel.findOne({ teacher_id: teacher });
        if (!findTeacher) {
          throw new Error("ບໍ່ພົບຂໍ້ມູນອາຈານ");
        }

        if (
          teacher_assessment.assessment_date.toISOString() !==
          new Date(assessment_date).toISOString()
        ) {
          const convertAssessment_data = new Date(assessment_date);
          const startOfMonth = new Date(
            convertAssessment_data.getFullYear(),
            convertAssessment_data.getMonth(),
            1
          );
          const endOfMonth = new Date(
            convertAssessment_data.getFullYear(),
            convertAssessment_data.getMonth() + 1,
            0,
            23,
            59,
            59,
            999
          );

          const existingTeacherAssessment =
            await teacherAssessmentModel.findOne({
              teacher: findTeacher._id,
              assessment_date: {
                $gte: startOfMonth, // Greater than or equal to the start of the month
                $lt: endOfMonth, // Less than the start of the next month
              },
            });

          if (existingTeacherAssessment) {
            throw new Error("ການປະເມີນອາຈານຜູ້ນີ້ໃນເດືອນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        teacher_assessment.teacher =
          findTeacher._id || teacher_assessment.teacher;
        teacher_assessment.attendance_class =
          attendance_class || teacher_assessment.attendance_class;
        teacher_assessment.behavior = behavior || teacher_assessment.behavior;
        teacher_assessment.assess_teaching =
          assess_teaching || teacher_assessment.assess_teaching;
        teacher_assessment.assessment_date =
          assessment_date || teacher_assessment.assessment_date;
        teacher_assessment.note = note || "";

        await teacher_assessment.save();
      } catch (error: any) {
        throw new Error(
          "ເກີດຂໍ້ຜິດພາກໃນການແກ້ໄຂຂໍ້ມູນການປະເມີນຜົນ: " +
            (error as Error).message
        );
      }
    } else {
      throw new Error("ບໍ່ພົບຂໍ້ມູນການປະເມີນນີ້");
    }
  },

  deleteTeacherAssessment: async (
    teacherAssessmentModel: Model<TeacherAssessmentDocument>,
    _id: string
  ) => {
    try {
      const teacher_assessment = await teacherAssessmentModel.deleteOne({
        _id: _id,
      });
      if (teacher_assessment.deletedCount === 0) {
        throw new Error("Teacher Assessment not found");
      }
    } catch (error: any) {
      throw new Error("Failed to delete Teacher Assessment: " + error.message);
    }
  },
};

export default teacherAssessmentRepository;
