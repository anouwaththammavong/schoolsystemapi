import { Model, Document } from "mongoose";
import { Student } from "../../domain/models/studentModels";
import { Assignment } from "../../domain/models/assignmentModels";

type StudentDocument = Document & Student;
type AssignmentDocument = Document & Assignment;

const assignmentRepository = {
  createAssignment: async (
    studentModel: Model<StudentDocument>,
    assignmentModel: Model<AssignmentDocument>,
    class_room: any,
    class_level: any,
    title: any
  ) => {
    try {
      const students = await studentModel.find({ class_level: class_level });
      if (students.length === 0) {
        throw new Error("There are no students in this class yet");
      }

      // Extract student IDs that already have subjects
      const studentIds = students.map((student) => student._id);

      // Create assignments for each student
      const assignments = studentIds.map(async (studentId) => {
        return await assignmentModel.create({
          student: studentId,
          class_room: class_room,
          class_level: class_level,
          title: title,
        });
      });

      return assignments;
    } catch (error: any) {
      throw new Error(
        "Failed to create assignment: " + (error as Error).message
      );
    }
  },
};

export default assignmentRepository;
