import { Model, Document } from "mongoose";
import userModel, { User } from "../../domain/models/userModel";
// import { Jwt } from "jsonwebtoken";
import jwt from "jsonwebtoken";

type UserDocument = Document & User;

const authRepository = {
  generateAccessToken: (user: User | any) => {
    const token = jwt.sign(
      { id: user._id, username: user.username },
      process.env.ACCESS_TOKEN_SECRET as string,
      {
        expiresIn: "1d",
      }
    );
    return token;
  },

  generateRefreshToken: (user: User | any) => {
    const token = jwt.sign(
      { id: user._id, username: user.username },
      process.env.REFRESH_TOKEN_SECRET as string,
      {
        expiresIn: "7d",
      }
    );
    return token;
  },

  verifyAccessToken: (token: string) => {
    try {
      const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as string);
      return decoded;
    } catch (error) {
      return;
    }
  },

  verifyRefreshToken: (token: string) => {
    try {
      const decoded = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET as string);
      return decoded;
    } catch (error) {
      return;
    }
  },
};

export default authRepository