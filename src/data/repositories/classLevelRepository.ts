import { Model, Document } from "mongoose";
import { ClassLevel } from "../../domain/models/classLevelModels";

type ClassLevelDocument = Document & ClassLevel;

const classLevelRepository = {
  createClassLevel: async (
    classLevelModel: Model<ClassLevelDocument>,
    classLevelData: ClassLevel
  ) => {
    try {
      // Check if class level id if is unique
      const existingClassLevelId = await classLevelModel.findOne({
        level_id: classLevelData.level_id,
      });
      if (existingClassLevelId) {
        throw new Error("ໄອດີຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
      }

      // Check if class level name if is unique
      const existingClassLevelName = await classLevelModel.findOne({
        level_name: classLevelData.level_name,
      });
      if (existingClassLevelName) {
        throw new Error("ຊື່ຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
      }

      return classLevelModel.create(classLevelData);
    } catch (error: any) {
      // Handle the error
      throw new Error(
        "Failed to create Class Level: " + (error as Error).message
      );
    }
  },

  getClassLevel: async (
    classLevelModel: Model<ClassLevelDocument>,
    limit: any,
    offset: any
  ) => {
    try {
      const totalCount = await classLevelModel.countDocuments();

      const classLevels = await classLevelModel
        .find()
        .sort({ level_name: 1 })
        .limit(Number(limit))
        .skip(Number(offset))
        .exec();

      return { classLevels, totalCount };
    } catch (error: any) {
      throw new Error("Failed to get class level: " + error.message);
    }
  },

  updateClassLevel: async (
    classLevelModel: Model<ClassLevelDocument>,
    _id: string,
    reqBody: ClassLevel
  ) => {
    const { level_id, level_name } = reqBody;
    const classLevel = await classLevelModel.findOne({ _id: _id });
    if (classLevel) {
      try {
        if (classLevel.level_id !== level_id) {
          const existingClassLevelId = await classLevelModel.findOne({
            level_id: level_id,
          });
          if (existingClassLevelId) {
            throw new Error("ໄອດີຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        if (classLevel.level_name !== level_name) {
          // Check if class level name if is unique
          const existingClassLevelName = await classLevelModel.findOne({
            level_name: level_name,
          });
          if (existingClassLevelName) {
            throw new Error("ຊື່ຂອງຊັ້ນຣຽນນີ້ແມ່ນມີແລ້ວ");
          }
        }

        classLevel.level_id = level_id || classLevel.level_id;
        classLevel.level_name = level_name || classLevel.level_name;

        await classLevel.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update class level: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no class level information found");
    }
  },

  deleteClassLevel: async (
    classLevelModel: Model<ClassLevelDocument>,
    _id: string
  ) => {
    try {
      const classLevel = await classLevelModel.deleteOne({ _id: _id });
      if (classLevel.deletedCount === 0) {
        throw new Error("Class level not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete class level: " + error.message);
    }
  },
};

export default classLevelRepository;
