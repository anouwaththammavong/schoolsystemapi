import { Model, Document } from "mongoose";
import { Student } from "../../domain/models/studentModels";
import { PayStudentTuition } from "../../domain/models/payStudentTuition";

type StudentDocument = Document & Student;
type PayStudentTuitionDocument = Document & PayStudentTuition;

const payStudentTuitionRepository = {
  // getPayStudentTuition: async (
  //   class_room: any,
  //   class_level: any,
  //   generation: any,
  //   limit: any,
  //   offset: any,
  //   studentModel: Model<StudentDocument>,
  //   payStudentTuitionModel: Model<PayStudentTuitionDocument>
  // ) => {
  //   try {
  //     const students = await studentModel.find({ class_level: class_level });

  //     // extract student IDs from the result
  //     const studentIds = students.map((student) => student._id);

  //     // Find pay student tuition for students matching the extracted IDs
  //     const existingPayStudentTuition = await payStudentTuitionModel
  //       .find({ student: { $in: studentIds } })
  //       .lean();

  //     // Extract student IDs that already have pay student tuition
  //     const studentIdsWithPayStudentTuition = existingPayStudentTuition.map(
  //       (pay_student_tuition) => pay_student_tuition.student.toString()
  //     );

  //     // Find student Ids without any pay student tuition
  //     const studentIdsWithoutPayStudentTuition = studentIds.filter(
  //       (id) => !studentIdsWithPayStudentTuition.includes(id.toString())
  //     );

  //     // Create new pay student tuition for students without existing entries
  //     if (studentIdsWithoutPayStudentTuition.length > 0) {
  //       const newPayStudentTuitionPromises =
  //         studentIdsWithoutPayStudentTuition.map((studentId) =>
  //           payStudentTuitionModel.create({
  //             student: studentId,
  //             class_room: class_room,
  //             class_level: class_level,
  //           })
  //         );
  //       await Promise.all(newPayStudentTuitionPromises);
  //     }

  //     // Count total matching students before pagination
  //     const totalStudents = await studentModel.countDocuments({
  //       class_level: class_level,
  //       generation: generation,
  //     });

  //     const payStudentTuition = await studentModel
  //       .aggregate([
  //         { $match: { class_level: class_level, generation: generation } },
  //         {
  //           $lookup: {
  //             from: "paystudenttuitions",
  //             let: { studentId: "$_id" },
  //             pipeline: [
  //               {
  //                 $match: {
  //                   $expr: { $eq: ["$student", "$$studentId"] },
  //                 },
  //               },
  //             ],
  //             as: "paystudenttuitions",
  //           },
  //         },
  //       ])
  //       .sort({ student_number: 1 })
  //       .limit(Number(limit))
  //       .skip(Number(offset));

  //     return {payStudentTuition,totalStudents};
  //   } catch (error: any) {
  //     throw new Error(
  //       "Failed to get pay student tuition: " + (error as Error).message
  //     );
  //   }
  // },

  getPayStudentTuition: async (
    class_room: any,
    class_level: any,
    generation: any,
    term_1: any,
    term_2: any,
    limit: any,
    offset: any,
    studentModel: Model<StudentDocument>,
    payStudentTuitionModel: Model<PayStudentTuitionDocument>
  ) => {
    try {
      // Convert string query parameters to booleans
      const term1Bool = term_1 === "true";
      const term2Bool = term_2 === "true";
  
      const students = await studentModel.find({ class_level: class_level });
  
      // Extract student IDs from the result
      const studentIds = students.map((student) => student._id);
  
      // Find pay student tuition for students matching the extracted IDs
      const existingPayStudentTuition = await payStudentTuitionModel
        .find({ student: { $in: studentIds } })
        .lean();
  
      // Extract student IDs that already have pay student tuition
      const studentIdsWithPayStudentTuition = existingPayStudentTuition.map(
        (pay_student_tuition) => pay_student_tuition.student.toString()
      );
  
      // Find student Ids without any pay student tuition
      const studentIdsWithoutPayStudentTuition = studentIds.filter(
        (id) => !studentIdsWithPayStudentTuition.includes(id.toString())
      );
  
      // Create new pay student tuition for students without existing entries
      if (studentIdsWithoutPayStudentTuition.length > 0) {
        const newPayStudentTuitionPromises =
          studentIdsWithoutPayStudentTuition.map((studentId) =>
            payStudentTuitionModel.create({
              student: studentId,
              class_room: class_room,
              class_level: class_level,
            })
          );
        await Promise.all(newPayStudentTuitionPromises);
      }
  
      // Build the aggregate pipeline
      const aggregatePipeline: any[] = [
        { $match: { class_level: class_level, generation: generation } },
        {
          $lookup: {
            from: "paystudenttuitions",
            let: { studentId: "$_id" },
            pipeline: [
              {
                $match: {
                  $expr: { $eq: ["$student", "$$studentId"] },
                },
              },
            ],
            as: "paystudenttuitions",
          },
        },
        {
          $unwind: {
            path: "$paystudenttuitions",
            preserveNullAndEmptyArrays: true,
          },
        },
      ];
  
      // Add filtering for term_1 and term_2 in the aggregation pipeline if provided
      const matchConditions: any = {};
      if (term_1 !== undefined) {
        matchConditions["paystudenttuitions.term_1"] = term1Bool;
      }
      if (term_2 !== undefined) {
        matchConditions["paystudenttuitions.term_2"] = term2Bool;
      }
      if (Object.keys(matchConditions).length > 0) {
        aggregatePipeline.push({ $match: matchConditions });
      }
  
      // Apply the same match conditions to the total count query
      const totalCountPipeline = [
        { $match: { class_level: class_level, generation: generation } },
        {
          $lookup: {
            from: "paystudenttuitions",
            let: { studentId: "$_id" },
            pipeline: [
              {
                $match: {
                  $expr: { $eq: ["$student", "$$studentId"] },
                },
              },
            ],
            as: "paystudenttuitions",
          },
        },
        {
          $unwind: {
            path: "$paystudenttuitions",
            preserveNullAndEmptyArrays: true,
          },
        },
      ];
  
      if (Object.keys(matchConditions).length > 0) {
        totalCountPipeline.push({ $match: matchConditions });
      }
  
      const totalStudentsResult = await studentModel.aggregate([
        ...totalCountPipeline,
        { $count: "totalStudents" },
      ]);
  
      const totalStudents = totalStudentsResult[0]?.totalStudents || 0;
  
      const payStudentTuition = await studentModel
        .aggregate(aggregatePipeline)
        .sort({ student_number: 1 })
        .limit(Number(limit))
        .skip(Number(offset));
  
      return { payStudentTuition, totalStudents };
    } catch (error: any) {
      throw new Error(
        "Failed to get pay student tuition: " + (error as Error).message
      );
    }
  },
  

  updatePayStudentTuition: async (
    payStudentTuitionModel: Model<PayStudentTuitionDocument>,
    _id: string,
    reqBody: PayStudentTuition
  ) => {
    const { term_1, term_2 } = reqBody;
    const payStudentTuition = await payStudentTuitionModel.findById(_id);
    if (payStudentTuition) {
      try {
        payStudentTuition.term_1 = term_1;
        payStudentTuition.term_2 = term_2;

        await payStudentTuition.save();
      } catch (error: any) {
        throw new Error("Internal server error" + (error as Error).message);
      }
    } else {
      throw new Error("no pay student tuition information found");
    }
  },
};

export default payStudentTuitionRepository;
