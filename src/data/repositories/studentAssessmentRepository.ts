import mongoose, { Model, Document } from "mongoose";
import { Student } from "../../domain/models/studentModels";
import { StudentAssessment } from "../../domain/models/studentAssessmentModels";

type StudentAssessmentDocument = Document & StudentAssessment;
type StudentDocument = Document & Student;

const studentAssessmentRepository = {
  createStudentAssessment: async (
    studentAssessmentModel: Model<StudentAssessmentDocument>,
    studentAssessmentData: StudentAssessment
  ) => {
    try {
      // Check if student_id and classroom already exist
      const existingStudentAssessment = await studentAssessmentModel.find({
        student: studentAssessmentData.student,
        room_id: studentAssessmentData.room_id,
        level_id: studentAssessmentData.level_id,
      });

      if (existingStudentAssessment.length > 0) {
        throw new Error("ຂໍ້ມູນນັກຮຽນນີ້ໃນຫ້ອງນີ້ມີການປະເມີນແລ້ວ");
      }

      return studentAssessmentModel.create(studentAssessmentData);
    } catch (error: any) {
      // Handle the error
      throw new Error(
        "Failed to create studentAssessment: " + (error as Error).message
      );
    }
  },

  getStudentAssessment: async (
    level_id: any,
    room_id: any,
    generation: any,
    limit: any,
    offset: any,
    studentModel: Model<StudentDocument>,
    studentAssessmentModel: Model<StudentAssessmentDocument>
  ) => {
    try {
      const students = await studentModel.find({
        level_id: level_id,
        room_id: room_id,
        generation: generation,
      });

      // extract student IDs from the result
      const studentIds = students.map((student) => student._id);

      // find student assessments for students matching the extracted Ids
      const existingStudentAssessment = await studentAssessmentModel
        .find({
          student: { $in: studentIds },
          level_id: level_id,
          room_id: room_id,
          generation: generation,
        })
        .lean();

      // Extract student Ids that already have student assessments
      const studentIdsWithStudentAssessments = existingStudentAssessment.map(
        (studentAssessment) => studentAssessment.student.toString()
      );

      // Find Student Ids without any Student Assessments
      const studentIdsWithoutStudentAssessments = studentIds.filter(
        (id) => !studentIdsWithStudentAssessments.includes(id.toString())
      );

      // Create new student Assessment for students without existing entries
      if (studentIdsWithoutStudentAssessments.length > 0) {
        const newStudentsPromises = studentIdsWithoutStudentAssessments.map(
          (studentId) =>
            studentAssessmentModel.create({
              student: studentId,
              level_id: level_id,
              room_id: room_id,
              generation: generation,
            })
        );
        await Promise.all(newStudentsPromises);
      }

      const totalStudents = await studentAssessmentModel.countDocuments({
        room_id: room_id,
        level_id: level_id,
        generation: generation,
      });

      // const studentAssessments = await studentModel
      //   .aggregate([
      //     {
      //       $match: {
      //         level_id: new mongoose.Types.ObjectId(level_id),
      //         room_id: new mongoose.Types.ObjectId(room_id),
      //         generation: generation,
      //       },
      //     },
      //     {
      //       $lookup: {
      //         from: "studentassessments",
      //         let: { studentId: "$_id" },
      //         pipeline: [
      //           {
      //             $match: {
      //               $expr: { $eq: ["$student", "$$studentId"] },
      //             },
      //           },
      //         ],
      //         localField: "student",
      //         foreignField: "_id",
      //         as: "studentassessments",
      //       },
      //     },
      //     {
      //       $lookup: {
      //         from: "classrooms",
      //         localField: "room_id",
      //         foreignField: "_id",
      //         as: "roomDetails",
      //       },
      //     },
      //     {
      //       $lookup: {
      //         from: "classlevels",
      //         localField: "roomDetails.level_id",
      //         foreignField: "_id",
      //         as: "level_id",
      //       },
      //     },
      //   ])
      //   .sort({ student_number: 1 })
      //   .limit(Number(limit))
      //   .skip(Number(offset));

      const studentAssessments = await studentAssessmentModel
        .aggregate([
          {
            $match: {
              level_id: new mongoose.Types.ObjectId(level_id),
              room_id: new mongoose.Types.ObjectId(room_id),
              generation: generation,
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "student",
              foreignField: "_id",
              as: "student",
            },
          },
          {
            $unwind: "$student",
          },
          {
            $lookup: {
              from: "classrooms",
              localField: "room_id",
              foreignField: "_id",
              as: "roomDetails",
            },
          },
          {
            $unwind: "$roomDetails",
          },
          {
            $lookup: {
              from: "classlevels",
              localField: "level_id",
              foreignField: "_id",
              as: "levelDetails",
            },
          },
          {
            $unwind: "$levelDetails",
          },
          {
            $project: {
              _id: 1,
              student: 1,
              roomDetails: 1,
              levelDetails: 1,
              attendance_class: 1,
              behavior: 1,
              assess_learning: 1,
              generation: 1,
              note: 1,
            },
          },
        ])
        .sort({ "student.student_number": 1 })
        .limit(Number(limit))
        .skip(Number(offset));

      return {
        studentAssessments,
        totalStudents,
      };
    } catch (error: any) {
      throw new Error("Failed to get subject: " + (error as Error).message);
    }
  },

  updateStudentAssessment: async (
    studentAssessmentModel: Model<StudentAssessmentDocument>,
    _id: string,
    reqBody: StudentAssessment
  ) => {
    const { attendance_class, behavior, assess_learning, note } = reqBody;
    const studentAssessment = await studentAssessmentModel.findById(_id);
    if (studentAssessment) {
      try {
        studentAssessment.attendance_class =
          attendance_class || studentAssessment.attendance_class;
        studentAssessment.behavior = behavior || studentAssessment.behavior;
        studentAssessment.assess_learning =
          assess_learning || studentAssessment.assess_learning;
        studentAssessment.note = note || studentAssessment.note;

        // Save the updated studentAssessment
        await studentAssessment.save();
      } catch (error: any) {
        throw new Error("Internal server error" + (error as Error).message);
      }
    } else {
      throw new Error("no studentAssessment information found");
    }
  },

  deleteStudentAssessment: async (
    studentAssessmentModel: Model<StudentAssessmentDocument>,
    _id: string
  ) => {
    try {
      const student_assessment = await studentAssessmentModel.deleteOne({
        _id: _id,
      });
      if (student_assessment.deletedCount === 0) {
        throw new Error("Student Assessment not found");
      }
    } catch (error: any) {
      throw new Error("Failed to delete Student Assessment: " + error.message);
    }
  },
};

export default studentAssessmentRepository;
