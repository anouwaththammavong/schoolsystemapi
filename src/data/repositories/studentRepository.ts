import { Model, Document } from "mongoose";
import { Student } from "../../domain/models/studentModels";

type StudentDocument = Document & Student;

const studentRepository = {
  createStudent: async (
    studentModel: Model<StudentDocument>,
    studentData: Student
  ) => {
    try {
      // Check if student_id is unique
      const existingStudentId = await studentModel.findOne({
        student_id: studentData.student_id,
      });
      if (existingStudentId) {
        // throw new Error("Student ID must be unique");
        throw new Error(
          "ລະຫັດນັກຮຽນນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ"
        );
      }

      // Check if student_number is unique within the same generation, class_room, and class_level
      const { generation, room_id, level_id, student_number, note } =
        studentData;

      // Construct the query object for finding existing students with the same number
      const query: any = {
        generation,
        room_id,
        level_id,
        student_number,
      };

      // If note is provided, include it in the query
      if (note !== undefined && note !== "") {
        query.note = note;
      } else {
        // If note is empty or not provided, include an empty note or check for documents where note is either empty or not present
        query.$or = [{ note: "" }, { note: { $exists: false } }];
      }

      // Check if there's already a student with the same student_number within the same combination of generation, class_room, class_level, and an empty or missing note
      const existingStudentWithSameNumber = await studentModel.findOne(query);

      // If the student_number already exists in the same combination of generation, class_room, and class_level
      if (existingStudentWithSameNumber) {
        // "Student number must be unique within the same generation, class room, and class level."
        throw new Error(
          "ລຳດັບນັກຮຽນນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລຳດັບອື່ນທີ່ບໍ່ຊ້ຳກັນ"
        );
      }

      // If no existing student with the same student_number within the same combination of generation, class_room, and class_level
      return studentModel.create(studentData);
    } catch (error: any) {
      // Handle the error
      throw new Error("Failed to create student: " + (error as Error).message);
    }
  },

  // getStudents: async (
  //   studentModel: Model<StudentDocument>,
  //   student_id: any,
  //   name: any,
  //   generation: any,
  //   limit: any,
  //   offset: any
  // ) => {
  //   try {
  //     let query = studentModel
  //       .find()
  //       .sort({ name: -1 })
  //       .limit(limit)
  //       .skip(offset);

  //     if (student_id || name || generation) {
  //       const conditions = [];

  //       if (student_id) {
  //         conditions.push({ student_id: student_id });
  //       }

  //       if (name) {
  //         conditions.push({ name: name });
  //       }

  //       if (generation) {
  //         conditions.push({ generation: generation });
  //       }

  //       query = query.where({ $and: conditions });
  //     }

  //     const results = await query.exec();
  //     return results;
  //   } catch (error: any) {
  //     throw new Error("Failed to get student: " + (error as Error).message);
  //   }
  // },

  getStudents: async (
    studentModel: Model<StudentDocument>,
    student_id: any,
    name: any,
    generation: any,
    level_id: any,
    new_student: any,
    limit: any,
    offset: any
  ) => {
    try {
      let conditions = {}; // Initialize the conditions as an empty object

      // Construct the query based on the provided parameters
      if (student_id || name || generation || level_id || new_student) {
        conditions = {
          ...(student_id && { student_id: student_id }),
          ...(name && { name: { $regex: name, $options: "i" } }), // case-insensitive search
          ...(generation && { generation: generation }),
          ...(level_id && { level_id: level_id }),
          ...(new_student && { new_student: new_student }),
        };
      }

      // Get total count of documents that match the criteria
      const totalCount = await studentModel.countDocuments(conditions);

      // Retrieve paginated results based on the conditions
      const students = await studentModel
        .find(conditions)
        .populate("room_id")
        .populate("level_id")
        .sort({ name: -1 })
        .limit(Number(limit))
        .skip(Number(offset))
        .exec();

      // uniqueGenerations will contain an array of unique generations without duplicates
      const uniqueGenerations = await studentModel
        .aggregate([
          {
            $group: {
              _id: "$generation", // Grouping by the generation field
            },
          },
          {
            $project: {
              _id: 0, // Exclude the _id field from the result
              generation: "$_id", // Rename the _id field to generation
            },
          },
        ])
        .sort({ generation: 1 });

      // Return both the count and the results
      return {
        students,
        totalCount,
        uniqueGenerations,
      };
    } catch (error: any) {
      throw new Error("Failed to get student: " + error.message);
    }
  },

  updateStudent: async (
    studentModel: Model<StudentDocument>,
    _id: string,
    reqBody: Student
  ) => {
    const {
      student_id,
      name,
      last_name,
      age,
      student_tel,
      date_of_birth,

      student_village,
      student_district,
      student_province,
      nationality,
      father_name,
      father_job,
      mother_name,
      mother_job,
      father_tel,
      mother_tel,
      student_past_school,

      student_number,
      room_id,
      level_id,
      status,
      note,
      school_entry_date,
      gender,
      generation,
    } = reqBody;
    const student = await studentModel.findById(_id);
    if (student) {
      try {
        if (student.student_id !== student_id) {
          const existingStudentId = await studentModel.findOne({
            student_id: student_id,
          });
          if (existingStudentId) {
            throw new Error(
              "ລະຫັດນັກຮຽນນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ"
            );
          }
        }

        if (
          student.generation !== generation ||
          String(student.room_id) !== String(room_id) ||
          String(student.level_id) !== String(level_id) ||
          student.student_number !== student_number
        ) {
          const query: any = {
            generation,
            room_id,
            level_id,
            student_number,
          };

          const existingStudentWithSameNumber = await studentModel.findOne(
            query
          );

          if (existingStudentWithSameNumber) {
            throw new Error(
              "ລຳດັບນັກຮຽນນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລຳດັບອື່ນທີ່ບໍ່ຊ້ຳກັນ"
              // "Student number must be unique within the same generation, class room, and class level."
            );
          }
        }

        student.student_id = student_id || student.student_id;
        student.name = name || student.name;
        student.last_name = last_name || student.last_name;
        student.student_tel = student_tel || student.student_tel;
        student.age = age || student.age;
        student.date_of_birth = date_of_birth || student.date_of_birth;

        student.student_village = student_village || student.student_village;
        student.student_district = student_district || student.student_district;
        student.student_province = student_province || student.student_province;
        student.nationality = nationality || student.nationality;
        student.father_name = father_name || student.father_name;
        student.father_job = father_job || student.father_job;
        student.mother_name = mother_name || student.mother_name;
        student.mother_job = mother_job || student.mother_job;
        student.father_tel = father_tel || student.father_tel;
        student.mother_tel = mother_tel || student.mother_tel;
        student.student_past_school = student_past_school || student.student_past_school;

        student.student_number = student_number || student.student_number;
        student.room_id = room_id || student.room_id;
        student.level_id = level_id || student.level_id;
        student.status = status || student.status;
        student.note = note || "";
        student.school_entry_date =
          school_entry_date || student.school_entry_date;
        student.gender = gender || student.gender;
        student.generation = generation || student.generation;

        // Save the updated student
        await student.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update student: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no student information found");
    }
  },

  deleteStudent: async (studentModels: Model<StudentDocument>, id: string) => {
    try {
      const student = await studentModels.deleteOne({ _id: id });
      if (student.deletedCount === 0) {
        throw new Error("Student not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete student: " + error.message);
    }
  },
};

export default studentRepository;
