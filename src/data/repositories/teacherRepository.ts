import { Model, Document } from "mongoose";
import teacherModels, { Teacher } from "../../domain/models/teacherModels";

type TeacherDocument = Document & Teacher;

const teacherRepository = {
  createTeacher: async (
    teacherModel: Model<TeacherDocument>,
    teacherData: Teacher
  ) => {
    try {
      // Check if teacher_id is unique
      const existingTeacherId = await teacherModel.findOne({
        teacher_id: teacherData.teacher_id,
      });
      if (existingTeacherId) {
        // throw new Error("Teacher ID must be unique");
        throw new Error(
          "ລະຫັດອາຈານນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ"
        );
      }

      // Check if the class already exists.
      const existingClass = await teacherModel.findOne({
        class_teacher: teacherData.class_teacher,
      });
      if (existingClass && Boolean(existingClass?.class_teacher)) {
        // throw new Error("Teacher in this class already exists");
        throw new Error("ຄູປະຈຳຫ້ອງນີ້ມີຢູ່ແລ້ວ");
      }

      return teacherModel.create(teacherData);
    } catch (error: any) {
      // Handle the error
      throw new Error("Failed to create teacher: " + (error as Error).message);
    }
  },

  getTeachers: async (
    teacherModel: Model<TeacherDocument>,
    teacher_id: any,
    limit: any,
    offset: any
  ) => {
    try {
      let filter = {};

      if (teacher_id) {
        filter = { teacher_id: teacher_id };
      }

      // Count total teachers matching the filter before applying limit and skip
      const totalTeachers = await teacherModel.countDocuments(filter);

      // Apply limit and skip for pagination
      // const teachers = await teacherModel
      //   .find(filter)
      //   .populate("subjects.title")
      //   .populate("class_teacher")
      //   .sort({ name: -1 })
      //   .limit(limit)
      //   .skip(offset)
      //   .exec();

      // Apply limit and skip for pagination
    const teachers = await teacherModel
    .find(filter)
    .populate({
      path: "subjects.title"
    })
    .populate({
      path: "class_teacher",
      populate: {
        path: "level_id",
        model: "ClassLevel"
      }
    })
    .sort({ name: -1 })
    .limit(Number(limit))
    .skip(Number(offset))
    .exec();

      // Return both teachers and the total count
      return {
        teachers,
        totalTeachers,
      };
    } catch (error: any) {
      throw new Error("Failed to get teachers: " + (error as Error).message);
    }
  },

  updateTeacher: async (
    teacherModel: Model<TeacherDocument>,
    _id: string,
    reqBody: Teacher
  ) => {
    const {
      teacher_id,
      name,
      last_name,
      age,
      teacher_tel,
      date_of_birth,

      teacher_village,
      teacher_district,
      teacher_province,
      nationality,
      graduated_from_institute,

      gender,
      degree,
      subjects,
      class_teacher,
    } = reqBody;
    const teacher = await teacherModel.findById(_id);
    if (teacher) {
      try {
        if (teacher.teacher_id != teacher_id) {
          const existingTeacherId = await teacherModel.findOne({
            teacher_id: teacher_id,
          });
          if (existingTeacherId) {
            // throw new Error("Teacher ID must be unique");
            throw new Error(
              "ລະຫັດອາຈານນີ້ມີຢູ່ແລ້ວ ກະລຸນາໃສ່ລະຫັດອື່ນທີ່ບໍ່ຊ້ຳກັນ"
            );
          }
        }

        if (class_teacher) {
        if (teacher.class_teacher != class_teacher) {
          const existingClass = await teacherModel.findOne({
            class_teacher: class_teacher,
          });
          if (existingClass) {
            // throw new Error("Teacher in this class already exists");
            throw new Error("ຄູປະຈຳຫ້ອງນີ້ມີຢູ່ແລ້ວ");
          }
        }
      }

        teacher.teacher_id = teacher_id || teacher.teacher_id;
        teacher.name = name || teacher.name;
        teacher.last_name = last_name || teacher.last_name;
        teacher.age = age || teacher.age;
        teacher.teacher_tel = teacher_tel || teacher.teacher_tel;
        teacher.date_of_birth = date_of_birth || teacher.date_of_birth;

        teacher.teacher_village = teacher_village || teacher.teacher_village;
        teacher.teacher_district = teacher_district || teacher.teacher_district;
        teacher.teacher_province = teacher_province || teacher.teacher_province;
        teacher.nationality = nationality || teacher.nationality;
        teacher.graduated_from_institute =
          graduated_from_institute || teacher.graduated_from_institute;

        teacher.gender = gender || teacher.gender;
        teacher.degree = degree || teacher.degree;
        teacher.subjects = subjects || teacher.subjects;
        if (typeof(class_teacher) !== undefined) {
          teacher.class_teacher = class_teacher
        }

        // Save the update teacher
        await teacher.save();
      } catch (error: any) {
        throw new Error(
          "Failed to update teacher: " + (error as Error).message
        );
      }
    } else {
      throw new Error("no teacher information found");
    }
  },

  deleteTeacher: async (teacherModels: Model<TeacherDocument>, id: string) => {
    try {
      const teacher = await teacherModels.deleteOne({ _id: id });
      if (teacher.deletedCount === 0) {
        throw new Error("Teacher not found");
      }
    } catch (error: any) {
      // Handle any errors that occur during the find or delete operations
      throw new Error("Failed to delete teacher: " + error.message);
    }
  },
};

export default teacherRepository;
