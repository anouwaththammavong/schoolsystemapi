import { Model, Document } from "mongoose";
import userModel, { User } from "../../domain/models/userModel";

type UserDocument = Document & User;

const userRepository = {
    getUserByUsername: async (reqBody:any) => {
        const username = [reqBody];
        try {
            const users = await userModel.findOne({username:username})
            return users;
        } catch (error) {
            throw new Error('Failed to get user');
        }
    }
}

export default userRepository