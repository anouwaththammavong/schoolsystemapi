import mongoose, { Schema, Document } from "mongoose";

export type AcademicYear = Document & {
    academic_year: string;
    academic_year_no: string;
}

const academicYearSchema = new Schema<AcademicYear>({
    academic_year: {type: String, required: true},
    academic_year_no: {type: String, required: true}
})

export default mongoose.model<AcademicYear>("AcademicYears", academicYearSchema)