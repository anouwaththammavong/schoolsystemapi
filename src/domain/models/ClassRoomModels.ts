import mongoose, { Schema, Document } from "mongoose";
import { ClassLevel } from "./classLevelModels";

export type ClassRoom = Document & {
  level_id: ClassLevel;
  room_name: string;
  room_id: string;
};

const classRoomSchema = new Schema<ClassRoom>({
  level_id: { type: Schema.Types.ObjectId, ref: "ClassLevel", required: true },
  room_id: { type: String, required: true, unique: true },
  room_name: { type: String, required: true },
});

export default mongoose.model<ClassRoom>("ClassRoom", classRoomSchema);