import mongoose, { Schema, Document } from "mongoose";

export type Registration = Document & {
  registration_id: string;
  student_id: Schema.Types.ObjectId; // Reference to Student
  room_id: Schema.Types.ObjectId; // Reference to ClassRoom
  level_id: Schema.Types.ObjectId; // Reference to Class Level
  academic_year_no: Schema.Types.ObjectId; // Reference to Academic Year
  school_fee_id: Schema.Types.ObjectId;
  registration_date: Date;
  isPaid: string;
};

const registrationSchema = new Schema<Registration>({
  registration_id: { type: String, required: true, unique: true },
  student_id: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  room_id: {
    type: Schema.Types.ObjectId,
    ref: "ClassRoom",
    required: true,
  },
  level_id: {
    type: Schema.Types.ObjectId,
    ref: "ClassLevel",
    required: true,
  },
  academic_year_no: {
    type: Schema.Types.ObjectId,
    ref: "AcademicYears",
    required: true,
  },
  school_fee_id: {
    type: Schema.Types.ObjectId,
    ref: "SchoolFees",
    required: true,
  },
  registration_date: { type: Date, required: true },
  isPaid: { type: String, required: true, default: "notPaid" },
});

export default mongoose.model<Registration>(
  "Registrations",
  registrationSchema
);
