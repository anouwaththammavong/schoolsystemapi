import mongoose, { Schema, Document } from "mongoose";
import { AcademicYear } from "./AcademicYearModels";
import { ClassLevel } from "./classLevelModels";

export type SchoolFee = Document & {
    school_fee_id: string;
    academic_year_no: AcademicYear;
    level_id: ClassLevel;
    book_fee: number;
    uniform_fee: number;
    term_fee: number;
    total_fee: number;
}

const schoolFeeSchema = new Schema<SchoolFee>({
    school_fee_id: {type: String, required: true, unique: true},
    academic_year_no: { type: Schema.Types.ObjectId, ref: "AcademicYears", required: true },
    level_id: { type: Schema.Types.ObjectId, ref: "ClassLevel", required: true },
    book_fee: {type: Number, required: true},
    uniform_fee: {type: Number, required: true},
    term_fee: {type: Number, required: true},
    total_fee: {type: Number, required: true}
})

export default mongoose.model<SchoolFee>("SchoolFees", schoolFeeSchema)