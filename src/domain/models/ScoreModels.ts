import mongoose, { Schema, Document } from "mongoose";

export type Score = Document & {
  student: Schema.Types.ObjectId;
  room_id: Schema.Types.ObjectId;
  level_id: Schema.Types.ObjectId;
  generation: string;
  month: Date;
  term: string;
  subjects: {
    title: Schema.Types.ObjectId;
    point: number;
    teacher: Schema.Types.ObjectId;
  }[];
  total_point: number;
  rank: number;
};

const scoreSchema = new Schema<Score>({
  student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  room_id: {
    type: Schema.Types.ObjectId,
    ref: "ClassRoom",
    required: true,
  },
  level_id: { type: Schema.Types.ObjectId, ref: "ClassLevel", required: true },
  generation: { type: String, required: true },
  month: { type: Date, required: true },
  term: { type: String },
  subjects: [
    {
      title: { type: Schema.Types.ObjectId, ref: "SubjectV2", required: true },
      point: { type: Number, default: 0 },
      teacher: { type: Schema.Types.ObjectId, ref: "Teachers" },
    },
  ],
  total_point:{ type: Number },
  rank: { type: Number},
});

export default mongoose.model<Score>("Scores", scoreSchema);