import mongoose, { Schema, Document } from "mongoose";

export type TeacherAssessment = Document & {
  teacher: Schema.Types.ObjectId;
  attendance_class: string;
  behavior: string;
  assess_teaching: string;
  assessment_date: Date;
  note: string;
};

const teacherAssessmentSchema = new Schema<TeacherAssessment>({
  teacher: { type: Schema.Types.ObjectId, ref: "Teachers", required: true },
  attendance_class: { type: String, required: true, default: "ປານກາງ" },
  behavior: { type: String, required: true, default: "ປານກາງ" },
  assess_teaching: { type: String, required: true, default: "ປານກາງ" },
  assessment_date: { type: Date, required: true },
  note: { type: String },
});

export default mongoose.model<TeacherAssessment>(
    "TeacherAssessments",
    teacherAssessmentSchema
  );
  