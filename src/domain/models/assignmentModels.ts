import mongoose, { Schema, Document } from "mongoose";
import { Student } from "./studentModels";

export type Assignment = Document & {
    student: Student;
    class_room: string;
    class_level: string;
    title: string;
    submit_assignment: boolean;
}

const assignmentSchema = new Schema<Assignment>({
    student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
    class_room: { type: String, required: true },
    class_level: { type: String, required: true },
    title: { type: String, required: true },
    submit_assignment: {type: Boolean, required: true, default: false}
});

export default mongoose.model<Assignment>("Assignment", assignmentSchema)