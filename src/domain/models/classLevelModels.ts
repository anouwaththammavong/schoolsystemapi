import mongoose, { Schema, Document } from "mongoose";

export type ClassLevel = Document & {
  level_id: string;
  level_name: string;
};

const classLevelSchema = new Schema<ClassLevel>({
  level_id: { type: String, required: true, unique: true },
  level_name: { type: String, required: true, unique: true },
});

export default mongoose.model<ClassLevel>("ClassLevel", classLevelSchema);
