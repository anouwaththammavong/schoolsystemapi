import mongoose, { Schema, Document } from "mongoose";
import { Student } from "./studentModels";

export type PayStudentTuition = Document & {
  student: Student;
  class_room: string;
  class_level: string;
  term_1: boolean;
  term_2: boolean;
};

const payStudentTuitionSchema = new Schema<PayStudentTuition>({
  student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  class_room: { type: String, required: true },
  class_level: { type: String, required: true },
  term_1: { type: Boolean, default: false },
  term_2: { type: Boolean, default: false },
});

export default mongoose.model<PayStudentTuition>(
  "PayStudentTuition",
  payStudentTuitionSchema
);
