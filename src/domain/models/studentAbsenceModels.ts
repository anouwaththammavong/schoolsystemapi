import mongoose, { Schema, Document } from "mongoose";
import { Student } from "./studentModels";

export type StudentAbsence = Document & {
  student: Student;
  room_id: Schema.Types.ObjectId;
  be_reasonable: string;
  absence_allday: string;
  absence_date: Date;
  term: string;
  generation: string;
  note: string;
};

const studentAbsenceSchema = new Schema<StudentAbsence>({
  student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  room_id: {
    type: Schema.Types.ObjectId,
    ref: "ClassRoom",
    required: true,
  },
  be_reasonable: { type: String, required: true },
  absence_allday: { type: String, required: true },
  absence_date: { type: Date, required: true },
  term: { type: String, required: true },
  generation: { type: String, required: true },
  note: { type: String },
});

export default mongoose.model<StudentAbsence>(
  "StudentAbsence",
  studentAbsenceSchema
);
