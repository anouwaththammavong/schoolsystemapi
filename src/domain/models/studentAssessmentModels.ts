import mongoose, { Schema, Document } from "mongoose";
import { Student } from "./studentModels";

export type StudentAssessment = Document & {
  student: Student;
  room_id: Schema.Types.ObjectId;
  level_id: Schema.Types.ObjectId;
  attendance_class: string;
  behavior: string;
  assess_learning: string;
  generation: string;
  note: string;
};

const studentAssessmentSchema = new Schema<StudentAssessment>({
  student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  room_id: {
    type: Schema.Types.ObjectId,
    ref: "ClassRoom",
    required: true,
  },
  level_id: { type: Schema.Types.ObjectId, ref: "ClassLevel", required: true },
  attendance_class: { type: String, required: true, default: "ປານກາງ" },
  behavior: { type: String, required: true, default: "ປານກາງ" },
  assess_learning: { type: String, required: true, default: "ປານກາງ" },
  generation: { type: String, required: true},
  note: { type: String },
});

studentAssessmentSchema.index({ student: 1, level_id: 1, room_id: 1, generation: 1 }, { unique: true });

export default mongoose.model<StudentAssessment>(
  "StudentAssessment",
  studentAssessmentSchema
);
