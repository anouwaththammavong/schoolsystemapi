import mongoose, { Schema, Document } from "mongoose";

export type Student = Document & {
  student_id: string;
  name: string;
  last_name: string;
  age: string;
  student_tel: string;
  date_of_birth: Date;
  student_village: string;
  student_district: string;
  student_province: string;
  nationality: string;
  father_name: string;
  father_job: string;
  mother_name: string;
  mother_job: string;
  father_tel: string;
  mother_tel: string;
  student_past_school?: string;
  student_number: string;
  room_id: Schema.Types.ObjectId;
  level_id: Schema.Types.ObjectId;
  status: string;
  note?: string;
  school_entry_date: Date;
  gender: string;
  generation: string;
  new_student: boolean;
};

const studentSchema = new Schema<Student>({
  student_id: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  last_name: { type: String, required: true },
  age: { type: String, required: true },
  student_tel: { type: String, required: true },
  date_of_birth: { type: Date, required: true },
  student_village: { type: String, required: true },
  student_district: { type: String, required: true },
  student_province: { type: String, required: true },
  nationality: { type: String, required: true },
  father_name: { type: String, required: true },
  father_job: { type: String, required: true },
  mother_name: { type: String, required: true },
  mother_job: { type: String, required: true },
  father_tel: { type: String, required: true },
  mother_tel: { type: String, required: true },
  student_past_school: { type: String },
  student_number: { type: String, required: true },
  room_id: { type: Schema.Types.ObjectId, ref: "ClassRoom", required: true },
  level_id: { type: Schema.Types.ObjectId, ref: "ClassLevel", required: true },
  status: { type: String, required: true },
  note: { type: String },
  school_entry_date: { type: Date, required: true },
  gender: { type: String, required: true },
  generation: { type: String, required: true },
  new_student: { type: Boolean, required: true, default: true },
});

export default mongoose.model<Student>("Students", studentSchema);
