import mongoose, { Schema, Document } from "mongoose";
import { Student } from "./studentModels";

export type Subject = Document & {
  student: Student;
  class_room: string;
  class_level: string;
  term: string;
  laos: number;
  english: number;
  geography: number;
  social: number;
  physical_education: number;
  history: number;
  math: number;
  chemical: number;
  physics: number;
  biology: number;
  total_score: number;
  academic_results: string;
  rank: number;
};

const subjectSchema = new Schema<Subject>({
  student: { type: Schema.Types.ObjectId, ref: "Students", required: true },
  class_room: { type: String, required: true },
  class_level: { type: String, required: true },
  term: { type: String, required: true },
  laos: { type: Number, default: 0 },
  english: { type: Number, default: 0 },
  geography: { type: Number, default: 0 },
  social: { type: Number, default: 0 },
  physical_education: { type: Number, default: 0 },
  history: { type: Number, default: 0 },
  math: { type: Number, default: 0 },
  chemical: { type: Number, default: 0 },
  physics: { type: Number, default: 0 },
  biology: { type: Number, default: 0 },
  total_score: { type: Number, default: 0 },
  academic_results: { type: String, default:"ອ່ອນຫຼາຍ" },
  rank: { type: Number }
});

export default mongoose.model<Subject>("Subject", subjectSchema);