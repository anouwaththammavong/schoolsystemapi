import mongoose, { Schema, Document } from "mongoose";

export type SubjectV2 = Document & {
    subject_id: string;
    subject_name: string;
  };
  
  const subjectV2Schema = new Schema<SubjectV2>({
    subject_id: { type: String, required: true, unique: true },
    subject_name: { type: String, required: true, unique: true },
  });
  
  export default mongoose.model<SubjectV2>("SubjectV2", subjectV2Schema);