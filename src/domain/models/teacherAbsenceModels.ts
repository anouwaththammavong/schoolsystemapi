import mongoose, { Schema, Document } from "mongoose";
import { Teacher } from "./teacherModels";

export type TeacherAbsence = Document & {
    teacher: Teacher;
    be_reasonable : string;
    absence_allday: string;
    absence_date: Date;
    note: string;
}

const teacherAbsenceSchema = new Schema<TeacherAbsence>({
    teacher: { type: Schema.Types.ObjectId, ref: "Teachers", required: true },
    be_reasonable: {type: String, required: true},
    absence_allday: {type: String, required: true},
    absence_date: { type: Date, required: true },
    note: {type: String}
})

export default mongoose.model<TeacherAbsence>("TeacherAbsence", teacherAbsenceSchema)