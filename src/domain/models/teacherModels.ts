import mongoose, { Schema, Document } from "mongoose";
import { SubjectV2 } from "./subjectV2Models";
import { ClassRoom } from "./ClassRoomModels";

export type Teacher = Document & {
  teacher_id: string;
  name: string;
  last_name: string;
  age: string;
  teacher_tel: string;
  date_of_birth: Date;
  teacher_village: string;
  teacher_district: string;
  teacher_province: string;
  nationality: string;
  graduated_from_institute: string;
  gender: string;
  degree: string;
  subjects: { title: Schema.Types.ObjectId }[]; // Reference to SubjectV2
  class_teacher?: Schema.Types.ObjectId; // Reference to ClassRoom
};

const teacherSchema = new Schema<Teacher>({
  teacher_id: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  last_name: { type: String, required: true },
  age: { type: String, required: true },
  teacher_tel: { type: String, required: true },
  date_of_birth: { type: Date, required: true },

  teacher_village: { type: String, required: true },
  teacher_district: { type: String, required: true },
  teacher_province: { type: String, required: true },
  nationality: { type: String, required: true },
  graduated_from_institute: { type: String, required: true },

  gender: { type: String, required: true },
  degree: { type: String, required: true },
  subjects: [
    {
      title: { type: Schema.Types.ObjectId, ref: "SubjectV2", required: true },
    },
  ],
  class_teacher: {
    type: Schema.Types.ObjectId,
    ref: "ClassRoom",
  },
});

export default mongoose.model<Teacher>("Teachers", teacherSchema);
