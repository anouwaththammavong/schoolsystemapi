import mongoose, { Schema, Document } from "mongoose";

export type User = Document & {
  _id: string;
  username: string;
  password: string;
  isAdmin: boolean;
  role: string;
};

const userSchema = new Schema<User>({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, required: true, default: true },
  role: { type: String, required: true, default: "ADMIN" }
});

export default mongoose.model<User>("Users", userSchema);
