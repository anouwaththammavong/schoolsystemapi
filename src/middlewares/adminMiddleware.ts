import { Request, Response, NextFunction } from "express";
import userModel from "../domain/models/userModel";

const adminMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    const auThenUser = await userModel.findOne({username:(req as any).decoded.username});
    if (!auThenUser) {
        res.status(401).json({ message: 'Unauthorized' });
    } else {
        if (auThenUser.isAdmin !== true) {
            res.status(403).json({ message: "Forbidden" })
        } else {
            next()
        }
    }
}

export default adminMiddleware