import { Request, Response, NextFunction } from "express";
import authRepository from "../data/repositories/authRepository";

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  // decode token
  const authHeader = req.headers.authorization;
  if (authHeader && authHeader.startsWith("Bearer ")) {
    const accessToken = authHeader.split(" ")[1];
    // verifies secret and check exp
    const decoded = authRepository.verifyAccessToken(accessToken);
    if (!decoded) {
      res.status(401).json({ message: "Unauthorized" });
    } else {
      // if everything is good, save to request for use in other routes
      (req as any).decoded = decoded;
      next();
    }
  } else {
    // User is not authenticated, send unauthorized response
    res.status(401).json({ message: "Unauthorized" });
  }
};

export default authMiddleware