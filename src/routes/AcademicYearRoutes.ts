import express from "express";
import {
  createAcademicYear,
  deleteAcademicYear,
  getAcademicYear,
  updateAcademicYear,
} from "../controllers/AcademicYearControllers";

const router = express.Router();

router.post("/createAcademicYear", createAcademicYear);
router.get("/getAcademicYear", getAcademicYear);
router.put("/updateAcademicYear/:id", updateAcademicYear);
router.delete("/deleteAcademicYear/:id", deleteAcademicYear);

export default router;
