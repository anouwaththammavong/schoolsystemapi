import express from "express";
import {
  createClassLevel,
  deleteClassLevel,
  getClassLevel,
  updateClassLevel,
} from "../controllers/ClassLevelControllers";

const router = express.Router();

router.post("/createClassLevel", createClassLevel);
router.get("/getClassLevel", getClassLevel);
router.put("/updateClassLevel/:id", updateClassLevel);
router.delete("/deleteClassLevel/:id", deleteClassLevel);

export default router;