import express from "express";
import { createClassRoom, deleteClassroom, getClassRooms, updateClassroom } from "../controllers/ClassRoomControllers";

const router = express.Router();

router.post("/createClassroom", createClassRoom);
router.get("/getClassrooms", getClassRooms);
router.put("/updateClassroom/:id", updateClassroom);
router.delete("/deleteClassroom/:id", deleteClassroom);

export default router