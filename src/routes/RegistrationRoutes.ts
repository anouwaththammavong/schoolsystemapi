import express from "express";
import {
    createNewStudentRegistration,
  createOldStudentRegistration,
  deleteRegistration,
  getRegistrations,
  updateNewRegistration,
  updateRegistration,
} from "../controllers/RegistrationControllers";

const router = express.Router();

router.post("/createOldStudentRegistration", createOldStudentRegistration);
router.post("/createNewStudentRegistration", createNewStudentRegistration);
router.get("/getRegistrations", getRegistrations);
router.put("/updateRegistration/:id", updateRegistration);
router.put("/updateNewRegistration/:id", updateNewRegistration);
router.delete("/deleteRegistration/:id", deleteRegistration);

export default router;
