import express from "express";
import { createSchoolFee, deleteSchoolFee, getSchoolFees, updateSchoolFee } from "../controllers/SchoolFeeControllers";

const router = express.Router();

router.post("/createSchoolFee", createSchoolFee);
router.get("/getSchoolFees", getSchoolFees);
router.put("/updateSchoolFee/:id", updateSchoolFee);
router.delete("/deleteSchoolFee/:id", deleteSchoolFee);

export default router