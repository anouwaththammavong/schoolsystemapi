import express from "express";
import { createScore, deleteScore, getScores, updateScore } from "../controllers/ScoreControllers";

const router = express.Router();

router.post("/createScore", createScore);
router.get("/getScores", getScores);
router.put("/updateScore/:id", updateScore);
router.delete("/deleteScore/:id", deleteScore);

export default router;