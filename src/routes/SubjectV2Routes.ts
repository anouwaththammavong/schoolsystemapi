import express from "express";
import { createSubjectV2, deleteSubjectV2, getSubjectV2, updateSubjectV2 } from "../controllers/SubjectV2Controller";

const router = express.Router();

router.post("/createSubjectV2", createSubjectV2);
router.get("/getSubjectV2", getSubjectV2);
router.put("/updateSubjectV2/:id", updateSubjectV2);
router.delete("/deleteSubjectV2/:id", deleteSubjectV2);

export default router;