import express from "express";
import { createTeacherAssessment, deleteTeacherAssessment, getTeacherAssessment, updateTeacherAssessment } from "../controllers/teacherAssessmentControllers";

const router = express.Router();

router.post("/createTeacherAssessment", createTeacherAssessment);
router.get("/getTeacherAssessment", getTeacherAssessment);
router.put("/updateTeacherAssessment/:id", updateTeacherAssessment);
router.delete("/deleteTeacherAssessment/:id", deleteTeacherAssessment);

export default router;
