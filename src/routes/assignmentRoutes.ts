import express from "express";
import { createAssignment } from "../controllers/assignmentControllers";

const router = express.Router();

router.post("/createAssignment", createAssignment);

export default router