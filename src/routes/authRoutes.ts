import express from "express"
import { updateUser, getUser, login, logout, refreshToken, register } from "../controllers/authControllers";

const router = express.Router();

router.post("/auth/register", register)
router.post("/auth/login", login)
router.put("/auth/updateUser/:id", updateUser)
router.get("/auth/getUser", getUser)
// Route for refreshing access token
router.post('/auth/refresh-token', refreshToken);
router.post('/auth/logout', logout);

export default router