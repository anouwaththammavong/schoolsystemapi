import express from "express";
import {
  getPayStudentTuition,
  updatePayStudentTuition,
} from "../controllers/payStudentTuitionControllers";

const router = express.Router();

router.get("/getPayStudentTuition", getPayStudentTuition);
router.put("/updatePayStudentTuition/:id", updatePayStudentTuition);

export default router;
