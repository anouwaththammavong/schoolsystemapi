import express from "express";
import {
  createStudentAbsence,
  deleteStudentAbsence,
  getAllStudentAbsence,
  getStudentAbsence,
  updateStudentAbsence,
} from "../controllers/studentAbsenceControllers";

const router = express.Router();

router.post("/createStudentAbsence", createStudentAbsence);
router.get("/getStudentAbsence", getStudentAbsence);
router.get("/getAllStudentAbsence", getAllStudentAbsence);
router.put("/updateStudentAbsence/:id", updateStudentAbsence);
router.delete("/deleteStudentAbsence/:id", deleteStudentAbsence);

export default router;
