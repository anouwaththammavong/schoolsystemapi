import express from "express";
import {
  createStudentAssessment,
  deleteStudentAssessment,
  getStudentAssessment,
  updateStudentAssessment,
} from "../controllers/studentAssessmentControllers";

const router = express.Router();

router.post("/createStudentAssessment", createStudentAssessment);
router.get("/getStudentAssessment", getStudentAssessment);
router.put("/updateStudentAssessment/:id", updateStudentAssessment);
router.delete("/deleteStudentAssessment/:id", deleteStudentAssessment);

export default router;
