import express from "express";
import { createStudent, deleteStudent, getStudent, updateStudent } from "../controllers/studentControllers";

const router = express.Router();

router.post("/createStudent", createStudent);
router.get("/getStudent", getStudent);
router.put("/updateStudent/:id", updateStudent);
router.delete("/deleteStudent/:id", deleteStudent);

export default router