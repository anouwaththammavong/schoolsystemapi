import express from "express";
import { deleteSubject, getSubject, updateSubject } from "../controllers/subjectControllers";

const router = express.Router();

router.get("/getSubject", getSubject);
router.put("/updateSubject/:id", updateSubject)
router.delete("/deleteSubject/:id", deleteSubject);

export default router;