import express from "express";
import {
  createTeacherAbsence,
  deleteTeacherAbsence,
  getTeacherAbsence,
  updateTeacherAbsence,
} from "../controllers/teacherAbsenceControllers";

const router = express.Router();

router.post("/createTeacherAbsence", createTeacherAbsence);
router.get("/getTeacherAbsence", getTeacherAbsence);
router.put("/updateTeacherAbsence/:id", updateTeacherAbsence);
router.delete("/deleteTeacherAbsence/:id", deleteTeacherAbsence);

export default router;
