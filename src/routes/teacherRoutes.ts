import express from "express";
import { createTeacher, deleteTeacher, getTeachers, updateTeacher } from "../controllers/teacherControllers";

const router = express.Router();

router.post("/createTeacher", createTeacher);
router.get("/getTeacher", getTeachers)
router.put("/updateTeacher/:id", updateTeacher);
router.delete("/deleteTeacher/:id", deleteTeacher);

export default router;